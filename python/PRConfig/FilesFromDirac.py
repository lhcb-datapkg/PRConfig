###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
import os
import subprocess
from collections import defaultdict


def get_access_urls_mc(bkkpath,
                       evttype,
                       filetypes,
                       sites_to_remove=[],
                       max_files=500):
    customEnv = {}

    # set custom grid proxy path if exists
    try:
        customEnv["X509_USER_PROXY"] = os.environ["X509_USER_PROXY"]
        print("Found X509_USER_PROXY set to {}".format(
            customEnv["X509_USER_PROXY"]))
    except:
        print("No X509_USER_PROXY found, continuing with defaults...")

    print("getting list of files from Dirac")
    stdout = subprocess.check_output(
        ". /cvmfs/lhcb.cern.ch/lib/LbEnv --quiet;"
        "lb-dirac dirac-dms-list-directory -B {} --EventType={} --FileType={}".
        format(bkkpath, evttype, ",".join(x.upper() for x in filetypes)),
        shell=True,
        env=customEnv,
        text=True,
    )

    # remove general directory name and number of files from list
    # e.g. "/lhcb/MC/Upgrade/XDIGI/00128250/0000/: 14 files, 0 sub-directories"
    file_list = [x for x in stdout.splitlines() if ":" not in x]

    # ensure files are always in same order
    file_list.sort()

    try:
        stdout = subprocess.check_output(
            ". /cvmfs/lhcb.cern.ch/lib/LbEnv --quiet;"
            "lb-dirac dirac-dms-lfn-accessURL --Terminal",
            shell=True,
            env=customEnv,
            text=True,
            input="\n".join(file_list[:max_files]))
    except subprocess.CalledProcessError as e:
        stdout = e.output

    urls = defaultdict(list)
    successful = False
    for line in stdout.splitlines():
        if line[0] != ' ':
            successful = line.startswith('Successful')
        else:
            if successful:
                m = re.match(r' +([^: ]*) *: *(.*)', line)
                if m:
                    urls[m.group(1)].append(m.group(2))

    # Get the first URL (if more than one) for each LFN, while skipping
    # LFNs for which we couldn't find an URL (e.g. a site was down).
    lfns_tmp = [urls[lfn][0] for lfn in file_list if lfn in urls]

    # Filter out some failing grid sites/files from the list
    excluded = ['stfc.ac.uk'] + sites_to_remove
    lfns = [
        lfn for lfn in lfns_tmp if not any(site in lfn for site in excluded)
    ]
    lfns = sorted(lfns, key=lambda str: not "eoslhcb.cern.ch" in str)

    return lfns

    # TODO warn if some of the first N files was not resolved to a URL
    # since then one would get numerically different results.


def get_access_urls_data(bkkpath, sites_to_remove=[], max_files=500):
    customEnv = {}

    # set custom grid proxy path if exists
    try:
        customEnv["X509_USER_PROXY"] = os.environ["X509_USER_PROXY"]
        print("Found X509_USER_PROXY set to {}".format(
            customEnv["X509_USER_PROXY"]))
    except:
        print("No X509_USER_PROXY found, continuing with defaults...")

    print("getting list of files from Dirac")
    stdout = subprocess.check_output(
        ". /cvmfs/lhcb.cern.ch/lib/LbEnv --quiet;"
        "lb-dirac dirac-dms-list-directory -B {}".format(bkkpath),
        shell=True,
        env=customEnv,
        text=True,
    )

    # remove general directory name and number of files from list
    # e.g. "/lhcb/MC/Upgrade/XDIGI/00128250/0000/: 14 files, 0 sub-directories"
    file_list = [x for x in stdout.splitlines() if ":" not in x]

    # ensure files are always in same order
    file_list.sort()

    print(
        "#### Checking output of . /cvmfs/lhcb.cern.ch/lib/LbEnv --quiet; lb-dirac dirac-dms-lfn-accessURL --Terminal"
    )
    try:
        stdout = subprocess.check_output(
            ". /cvmfs/lhcb.cern.ch/lib/LbEnv --quiet;"
            "lb-dirac dirac-dms-lfn-accessURL --Terminal",
            shell=True,
            env=customEnv,
            text=True,
            input="\n".join(file_list[:max_files]))
    except subprocess.CalledProcessError as e:
        stdout = e.output

    print("#### Storing URLs")
    urls = defaultdict(list)
    successful = False
    for line in stdout.splitlines():
        if line[0] != ' ':
            successful = line.startswith('Successful')
        else:
            if successful:
                m = re.match(r' +([^: ]*) *: *(.*)', line)
                if m:
                    urls[m.group(1)].append(m.group(2))

    # Get the first URL (if more than one) for each LFN, while skipping
    # LFNs for which we couldn't find an URL (e.g. a site was down).
    lfns_tmp = [urls[lfn][0] for lfn in file_list if lfn in urls]

    # Filter out some failing grid sites/files from the list
    excluded = ['stfc.ac.uk'] + sites_to_remove
    lfns = [
        lfn for lfn in lfns_tmp if not any(site in lfn for site in excluded)
    ]
    lfns = sorted(lfns, key=lambda str: not "eoslhcb.cern.ch" in str)
    if any(file.endswith("raw") for file in file_list):
        lfns = [lfn for lfn in lfns if "eoslhcb.cern.ch" in lfn]

    return lfns

    # TODO warn if some of the first N files was not resolved to a URL
    # since then one would get numerically different results.


if __name__ == "__main__":
    # bkk_path = ("/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8"
    # "/Sim10-Up08/Digi15-Up04")
    # evt_type = "30000000"
    bkk_path_for_data = (
        "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real\ Data/Reco18/Stripping34/90000000/BHADRON.MDST"
    )
    # for url in get_access_urls_mc(bkk_path, evt_type, ['XDIGI']):
    for url in get_access_urls_data(bkk_path_for_data, max_files=10):
        print(url)
