###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
TestFileDB: the database of files used for testing in the nightlies and elsewhere.

Usage: Add into this module a testfiles() object which maps a logical name for a dataset, like '2012_raw_default' to a list of files.

This module creates the dictionary {logicalname : <testfiles>)

Testfile object, minimum information:
   Minimally you must supply->
     1) a logical name
     2) a list of files
     3) a dictionary with { 'Date':<> , 'Format':<>, 'DataType' : <>}
     4) a meaningful comment as to why the file was added to the test DB
     (the database to add to, test_file_db)

So, minimally something like:

testfiles(
    'Dummy!!',
    ['/dev/null'],
    {
    'Author': 'Rob Lambert',
    'Format': 'DIGI',
    'DataType': '2011',
    'Date': '2013.05.31',
    },
    'Dummy test file, for testing the testfiles mechanism...',
    test_file_db
    )

The testfiles object has the concept of a qualifier, for which a small number are additionally known, such as the DDDB tag, CondDB tag, Production, Stripping, Reconstruction, etc.

For the list of possibilites, see PRConfig.TestFileObjects.testfiles.__known_qualifiers__

The testfiles object is smart enough to know how to add it into the EventSelector, e.g.:

from PRConfig.TestFileDB import test_file_db

test_file_db['2012_raw_default'].run() #pass and options to LHCbApp

or

test_file_db['2012_raw_default'].run(configurable=Moore())

---------------------------------

There is now an automated mechanism to append to this database, see:
-> $PRCONFIGROOT/scripts/AddToTestFilesDB.py

To check files in the database can be read, see:
-> $PRCONFIGROOT/scripts/TestReadable.py

To migrate files in this database to EOS/CERN-SWTEST, see:
-> $PRCONFIGROOT/scripts/MigrateToSWTEST.py

"""
#get the testfile object definition
from PRConfig.TestFileObjects import testfiles
from PRConfig.TestFileDBObject import TestFileDBDict

#the database to fill
test_file_db = TestFileDBDict()

###############  TEST FILES START HERE ###################
testfiles(
    "mc_2024_sim10d_HLT2-2024.Q1.2_Bu2JpsiK_MagDown", [
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2024/DIGI/00226612/0000/00226612_00000010_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2024/DIGI/00226612/0000/00226612_00000011_1.digi',
    ], {
        'Author': 'Ziyu Bai',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2024.12.2',
        'DDDB': 'dddb-20240427',
        'CondDB': 'sim10-2024.Q1.2-v1.1-md100',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'sim10/run3-ideal',
    },
    'MC sample used for flavour tagging tests in Moore, Bu2JpsiK, MagDown',
    test_file_db=test_file_db)

testfiles(
    "mc_2024_sim10d_HLT2-2024.W31.34_Bd2KstMuMu_MagUp", [
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2024/DST/00252443/0000/00252443_00000003_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2024/DST/00252443/0000/00252443_00000022_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2024/DST/00252443/0000/00252443_00000007_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2024/DST/00252443/0000/00252443_00000019_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2024/DST/00252443/0000/00252443_00000005_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2024/DST/00252443/0000/00252443_00000016_1.dst',
    ], {
        'Author': 'Mark Smith',
        'Format': 'DST',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2024.11.18',
        'DDDB': 'dddb-20240427',
        'CondDB': 'sim10-2024.Q3.4-v1.3-mu100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    '2024 centrally produced with HLT2 MC for use in the DaVinciExamples tests',
    test_file_db=test_file_db)

testfiles(
    "collision_24_magup_spruce_24c4_data_rd_stream", [
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00002996_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00002952_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00002329_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00000274_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00000498_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00007050_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00005595_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00005544_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00005081_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00003112_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00005523_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00005771_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00003460_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00005609_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00004349_1.rd.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2024/RD_DST/00235370/0000/00235370_00005730_1.rd.dst',
    ], {
        'Author': 'Mark Smith',
        'Format': 'DST',
        'DataType': '2024',
        'Simulation': False,
        'Date': '2025.02.05',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    '2024 MagUp collision data from sprucing 24c4, RD stream, for the DaVinci PRTest of memory usage',
    test_file_db=test_file_db)

testfiles(
    "upgrade_sim10_Up08_Digi15_D2KsPi_MU", [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000030_1.xdigi',
        'root://xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000097_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000042_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000104_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000088_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000062_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000086_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00128247/0000/00128247_00000066_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000041_1.xdigi',
        'root://xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000114_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00128247/0000/00128247_00000055_1.xdigi'
    ], {
        'Author': 'Peilian Li',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2021.06.08',
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-mu100',
    },
    'MC Validation test for upgrade studies, D2KsPi, MagUp',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Bs2JPsiPhi_MD', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000021_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000042_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000054_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000066_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000072_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000075_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000079_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000001_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000015_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000026_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000036_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000049_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000061_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000080_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000081_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000003_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000005_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000027_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000052_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000074_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000004_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000031_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000034_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000044_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000047_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000067_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000070_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000078_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000006_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000020_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000043_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000059_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000076_1.xdigi',
        'root://hake12.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000008_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000018_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000029_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000041_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000058_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091825/0000/00091825_00000077_1.xdigi',
        'root://door02.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000030_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000053_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000082_1.xdigi',
        'root://shark10.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091825/0000/00091825_00000063_1.xdigi'
    ], {
        'Author': 'Ross Hunter',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.12.02',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
    },
    'Data Challenges test for upgrade studies, Bs2JpsiPhi (Jpsi -> mu+ mu-, Phi -> K+ K-), MagDown',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_Bs2JpsiPhiMU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000043_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000058_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000063_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000070_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000074_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000076_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000077_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000078_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000079_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000081_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000083_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000001_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000008_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000037_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000039_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000061_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000073_1.xdigi',
        'root://shark6.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000002_1.xdigi',
        'root://shark6.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000066_1.xdigi'
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000003_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000005_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000006_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000020_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000041_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000049_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000067_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000080_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000004_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000007_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000021_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000030_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000042_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000059_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000072_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091139/0000/00091139_00000082_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000016_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000026_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000045_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000075_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000084_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000019_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000035_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000044_1.xdigi',
        'root://door01.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000068_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000031_1.xdigi',
        'root://lhcbsdrm.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091139/0000/00091139_00000054_1.xdigi',
    ], {
        'Author': 'Ross Hunter',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.12.02',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
    },
    'Data Challenges test for upgrade studies, Bs2JpsiPhi (Jpsi -> mu+ mu-, Phi -> K+ K-), MagUp',
    test_file_db=test_file_db)

testfiles(
    'upgrade_B2JpsiK_ee_MU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00099854/0000/00099854_000000{0}_1.xdigi'
        .format(i) for i in [12, 16, 20, 24, 29, 32, 36, 38, 41, 43, 46]
    ], {
        'Author': 'Carla Marin',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2021.03.16',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
    },
    'Test sample of B2JpsiK with Jpsi2ee for upgrade studies, MagUp, Sim10-Up03-OldP8Tuning',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_MinBiasMU', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000008_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000048_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000050_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000061_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000063_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000073_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092855/0000/00092855_00000006_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092855/0000/00092855_00000012_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092855/0000/00092855_00000042_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000010_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000018_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000023_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000040_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000062_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000070_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000001_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000004_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000025_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000034_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000043_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000074_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000014_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000026_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000031_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000039_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000046_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000057_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000058_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000067_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000075_1.xdigi',
        'root://lhcb-sdpd7.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000027_1.xdigi',
        'root://lhcb-sdpd15.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000037_1.xdigi',
        'root://lhcb-sdpd16.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000054_1.xdigi',
        'root://lhcb-sdpd16.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000056_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000072_1.xdigi',
        'root://hake1.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000002_1.xdigi',
        'root://hake1.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000003_1.xdigi',
        'root://guppy14.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000005_1.xdigi',
        'root://hake1.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000007_1.xdigi',
        'root://guppy7.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000017_1.xdigi',
        'root://guppy14.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000021_1.xdigi',
        'root://guppy7.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000052_1.xdigi',
        'root://guppy7.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092855/0000/00092855_00000066_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        "ConditionsVersion": "master",
    },
    'Data Challenges test for upgrade studies, MinBias, MagUp',
    test_file_db=test_file_db)

testfiles(
    'upgrade_DC19_01_MinBiasMD', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000014_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000031_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000054_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000071_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000072_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000074_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000005_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000010_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000021_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000040_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000055_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000066_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00092857/0000/00092857_00000073_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000008_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000018_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000028_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000033_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000048_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000051_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000057_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000070_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000003_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000017_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000029_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000035_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000043_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000060_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000064_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000002_1.xdigi',
        'root://door04.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000024_1.xdigi',
        'root://door07.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000038_1.xdigi',
        'root://door05.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000050_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000006_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000013_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000020_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000026_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000042_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000046_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000049_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000059_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000063_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000065_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000067_1.xdigi',
        'root://lhcb-sdpd2.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000004_1.xdigi',
        'root://lhcb-sdpd11.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000007_1.xdigi',
        'root://lhcb-sdpd2.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000032_1.xdigi',
        'root://lhcb-sdpd1.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000047_1.xdigi',
        'root://lhcb-sdpd5.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000058_1.xdigi',
        'root://hake4.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000045_1.xdigi',
        'root://guppy5.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00092857/0000/00092857_00000061_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
    },
    'Data Challenges test for upgrade studies, MinBias, MagDown',
    test_file_db=test_file_db)

testfiles(
    "upgrade_minbias_2019", [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP4/test_files/minbias/00091069_00000001_1.digi"
    ], {
        "Author": "Biljana Mitreska",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2019.07.23",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20190223",
        "ConditionsVersion": "master",
    }, "Upgrade minbias sample for testing the VP alignment", test_file_db)

testfiles(
    "upgrade_alignment_Bs2JpsiPhi", [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP4/test_files/bs2jpsiphi/00091825_00000016_1.xdigi"
    ], {
        "Author": "Peter Griffith",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.04.05",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20190223",
    }, "Upgrade Bs2JpsiPhi sample for testing the Muon system alignment",
    test_file_db)

testfiles(
    'upgrade_DC19_01_Bs2PhiPhiMD', [
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000003_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000008_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000019_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000021_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000034_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000038_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000042_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000051_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000053_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000055_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000056_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000057_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000058_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000059_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000060_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000062_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000064_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000065_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000066_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000068_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000069_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000075_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000079_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000080_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000014_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000025_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000041_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000054_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000061_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000072_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000074_1.xdigi',
        'root://xrootd-lhcb.cr.cnaf.infn.it//storage/gpfs_lhcb/lhcb/disk/MC/Upgrade/XDIGI/00091831/0000/00091831_00000081_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000001_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000007_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000023_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000031_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000048_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000067_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000077_1.xdigi',
        'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000082_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000030_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000050_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000063_1.xdigi',
        'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000083_1.xdigi',
        'root://door06.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000017_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000036_1.xdigi',
        'root://door03.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000071_1.xdigi',
        'root://door04.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000085_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000028_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000047_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000070_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000078_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000086_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000002_1.xdigi',
        'root://lhcb-sdpd8.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000043_1.xdigi',
        'root://lhcb-sdpd11.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000076_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000084_1.xdigi',
        'root://lhcb-sdpd4.t1.grid.kiae.ru:1094/t1.grid.kiae.ru/data/lhcb/lhcbdisk/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000087_1.xdigi',
        'root://lobster16.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000004_1.xdigi',
        'root://mouse7.grid.surfsara.nl:1094/pnfs/grid.sara.nl/data/lhcb/MC/Upgrade/XDIGI/00091831/0000/00091831_00000033_1.xdigi'
    ], {
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2019.07.02',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
    },
    'Data Challenges test for tracking, Bs2phiphi, MagDown',
    test_file_db=test_file_db)

testfiles(
    "TestData_stripping_PbPbcollision18_reco18", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00017884_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00017873_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00017858_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Lead18/RDST/00083197/0001/00083197_00015240_1.rdst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "RDST",
        "DataType": "2018",
        "Date": "2019.01.09",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 218177 PbPb data from 2018 collisions", test_file_db)

testfiles(
    "TestData_restripping_collision18_reco18", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119592_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119590_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119588_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119597_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119595_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision18/RDST/00077427/0011/00077427_00119593_1.rdst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "RDST",
        "DataType": "2018",
        "Date": "2018.11.06",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 214741 pp data from 2018 for end of LHCb restrippings",
    test_file_db)

testfiles(
    "TestData_restripping_collision17_reco17", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00064779/0000/00064779_00000242_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00064779/0000/00064779_00000245_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00064779/0000/00064779_00000500_1.rdst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "RDST",
        "DataType": "2017",
        "Date": "2018.11.06",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 195969 pp data from 2017 for end of LHCb restrippings",
    test_file_db)

testfiles(
    "TestData_stripping20r2_collision13_reco14", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000538_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000431_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000928_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000289_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000134_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision13/FULL.DST/00023836/0000/00023836_00000465_1.full.dst"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "DST",
        "DataType": "2013",
        "Date": "2025-02.18",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["test_stripping20r2_collision13_reco14"],
    }, "Run 137246 pp data from 2013 for stripping20r2 stripping qmtests",
    test_file_db)

testfiles(
    "TestData_stripping20r3_protonion13_reco14r1", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Protonion13/FULL.DST/00032860/0000/00032860_00005453_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Protonion13/FULL.DST/00032860/0000/00032860_00005634_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Protonion13/FULL.DST/00032860/0000/00032860_00005549_1.full.dst"
    ], {
        "Author":
        "Marco Cattaneo",
        "Format":
        "DST",
        "DataType":
        "2013",
        "Date":
        "2025-02.18",
        "Application":
        "DaVinci",
        "Simulation":
        False,
        "QMTests": [
            "test_stripping20r3_protonion13_reco14r1",
            "test_stripping20r3p1_protonion13_reco14r1"
        ],
    },
    "Run 135710 ProtonIon data from 2013 for stripping20r3 stripping qmtests",
    test_file_db)

testfiles(
    "TestData_restripping_collision12_reco14", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000672_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000320_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000315_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000327_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000316_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000323_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00021315/0000/00021315_00000317_1.full.dst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2018.11.06",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 133399 pp data from 2012 for end of LHCb restrippings",
    test_file_db)

testfiles(
    "TestData_restripping_collision11_reco14", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision11/FULL.DST/00022754/0000/00022754_00000029_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision11/FULL.DST/00022754/0000/00022754_00000010_1.full.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision11/FULL.DST/00022754/0000/00022754_00000047_1.full.dst"
    ], {
        "Author": "Nicole Skidmore",
        "Format": "DST",
        "DataType": "2011",
        "Date": "2018.11.06",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [],
    }, "Run 89335 pp data from 2011 for end of LHCb restrippings",
    test_file_db)

testfiles(
    "2017_DaVinciTests.stripping32_collision17_reco17", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000015_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000016_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000017_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000018_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000019_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000020_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000021_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00069260/0000/00069260_00000022_1.rdst",
    ], {
        "Author": "Carlos Vazquez Sierra",
        "Format": "RDST",
        "DataType": "2017",
        "Date": "2018.07.19",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.stripping32_collision17_reco17"],
    }, "Run 202214 5 TeV pp data from 2017 for Stripping32", test_file_db)

# Moore tests
testfiles(
    "S20r0p2_stripped_test", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/B2CC.S20r0p2.Dimuon.dst"
    ], {
        "Author": "Stefano Perazzini",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2013.09.30",
        "Processing": "Stripping20r0p2",
        "Reconstruction": 14,
        "Stripping": "20r0p2",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.rerun_s20p2", "moore.swim_s20p2"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "Test stripping v20r0p2 2013 DST including packed flavour tag classes. DDDB/Conddb used in production was dddb-20120831, cond-20130114",
    test_file_db)

testfiles(
    "S20_stripped_test", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/DIMUON.DST/00020567/0000/00020567_00006473_1.dimuon.dst"
    ], {
        "Author":
        "Vava Gligorov",
        "Format":
        "DST",
        "DataType":
        "2012",
        "Date":
        "2013.09.30",
        "Processing":
        "Stripping20",
        "Reconstruction":
        14,
        "Stripping":
        20,
        "Application":
        "DaVinci",
        "Simulation":
        False,
        "QMTests": [
            "moore.rerun_s20", "moore.swim_s20",
            "ioexample.copyreco14stripping20dsttoroot"
        ],
        "DDDB":
        "dddb-20120831",
        "CondDB":
        "cond-20120831"
    },
    "Test stripping v20r0 2013 DST *not* including packed flavour tag classes, for Moore v14r8 (nominal). ",
    test_file_db)

testfiles(
    "S21_bhadron_mdst", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/BHADRON.MDST/00041836/0000/00041836_00000001_1.bhadron.mdst"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDST",
        "DataType": "2012",
        "Date": "2012",
        "Processing": "Stripping21",
        "Reconstruction": 14,
        "Stripping": "21",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["ioexample.copyreco14stripping21mdsttoroot"],
        "DDDB": "dddb-20130929-1",
        "CondDB": "cond-20141107"
    }, "Test with Reco14-Stripping21 BHADRON.MDST from 2012", test_file_db)

testfiles(
    "2018_raw_full", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2018/RAW/FULL/LHCb/COLLISION18/209291/209291_0000000745.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2018",
        "Date": "2018.05.26",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["brunel.2018magdown"],
    },
    "Proton-Proton collision raw data FULL stream, run 209291 from 26th May 2018. Same run as used for Stripping tests",
    test_file_db)

testfiles(
    "2017_raw_full", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2017/RAW/FULL/LHCb/COLLISION17/198660/198660_0000000211.raw"
    ], {
        "Author":
        "Marco Cattaneo",
        "Format":
        "MDF",
        "DataType":
        "2017",
        "Date":
        "2017.09.06",
        "Application":
        "Moore",
        "Simulation":
        False,
        "QMTests": [
            "brunel.2017magup", "l0app.data_xxx",
            "DaVinciTests.davinci.gaudipython_algs"
        ],
    },
    "Proton-Proton collision raw data FULL stream, run 198660 from 6th September 2017. Same run as used for Stripping tests",
    test_file_db)

testfiles(
    "2016_raw_full", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2016/RAW/FULL/LHCb/COLLISION16/179277/179277_0000000601.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2016",
        "Date": "2016.07.05",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["brunel.2016magup"],
    },
    "Proton-Proton collision raw data FULL stream, run 179277 from 7th July 2016. Same run as used for Stripping tests",
    test_file_db)

testfiles(
    "2015_raw_full", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2015/RAW/FULL/LHCb/COLLISION15/164668/164668_0000000744.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2015",
        "Date": "2015.10.02",
        "Application": "Moore",
        "Simulation": False,
        "QMTests":
        ["brunel.2015magdown, brunel.compatibility.2015-latest-*db"],
    },
    "Proton-Proton collision raw data FULL stream, run 164668 from 2nd October 2015. Same run as used for Stripping tests",
    test_file_db)

testfiles(
    "2009_raw", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2009/RAW/FULL/LHCb/COLLISION09/63497/063497_0000000001.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2009",
        "Date": "2009.12.06",
        "Simulation": False,
        "QMTests": ["brunel.fixedfile-brunel2009-*"],
    },
    "Proton-Proton collision raw data FULL stream, run 32474 from 12th December 2009. Magnet on, all detectors in readout, Velo open",
    test_file_db)

testfiles(
    "2008_cosmics", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2008/RAW/LHCb/COSMICS/35537/035537_0000088110.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2008",
        "Date": "2008",
        "Simulation": False,
        "QMTests": ["brunel.brunel-cosmics"],
    }, "Cosmics run, with CALO, OT and  RICH2 (35569 events)", test_file_db)

testfiles(
    "2008_TED", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2008/RAW/LHCb/BEAM/32474/032474_0000081642.raw"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "MDF",
        "DataType": "2008",
        "Date": "2008.09",
        "Simulation": False,
        "QMTests": ["brunel.brunel-2008"],
    }, "TED data from September 2008 with velo - prev2", test_file_db)

testfiles(
    "2012_raw_default", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2012_raw_default/1318"
        + str(n) + "_0x0046_NB_L0Phys_00.raw" for n in range(83, 100)
    ], {
        "Author": "Eric Van Herwijnen",
        "Format": "MDF",
        "DataType": "2012",
        "Date": "2012.11.09",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["moore.physics.2012", "moore.database.2012", "l0app."],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "L0 Phys, L046, from 2012 data, mu~1.7 originally found from http://twiki.cern.ch/bin/view/LHCb/DataSets",
    test_file_db)

testfiles(
    "2012_raw_full", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2012/RAW/FULL/LHCb/COLLISION12/114753/114753_0000000"
        + n + ".raw" for n in ["015", "016", "017", "298"]
    ], {
        "Author": "Jaap Panman",
        "Format": "MDF",
        "DataType": "2012",
        "Date": "2012.11.09",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["lumialgs"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    },
    "Direct raw data from the pit taken in 2012. Used for Rec/LumiAlgs tests so that there are lumi events, etc. copied out of the freezer",
    test_file_db)

testfiles(
    "2011_raw_default", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_raw_default/81349_0x002a_MBNB_L0Phys.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_raw_default/80881_0x002a_MBNB_L0Phys.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_raw_default/79647_0x002a_MBNB_L0Phys.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_raw_default/79646_0x002a_MBNB_L0Phys.raw"
    ], {
        "Author": "Eric Van Herwijnen",
        "Format": "MDF",
        "DataType": "2011",
        "Date": "2011.01.07",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["moore.database.2011"],
        "DDDB": "head-20110302",
        "CondDB": "head-20110622"
    },
    "L0 Phys, L002, from 2011 data, used previously for run_more script in Moore tests. Now no longer used there.",
    test_file_db)

testfiles(
    "Sim09_2016_digi", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/debug/2016/DIGI/00073919/0000/00073919_00000001_4.digi"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "DIGI",
        "DataType": "2016",
        "Date": "2018.05.29",
        "Application": "Moore",
        "Simulation": True,
        "QMTests": ["brunel.sim09-2016"],
        "DDDB": "dddb-20170721-3",
        "CondDB": "sim-20170721-2-vc-md100"
    },
    "2016 Sim09 Digi, event type 49000041, /MC/2016/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09c/Trig0x6138160F/ (Boole step 133533, Moore steps 130089, 130090)",
    test_file_db)

testfiles(
    "Charm_Strip20_Test", [
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Charm_Strip20_Test/00022726_00111043_1.CharmToBeSwum.dst'
    ], {
        "Author": "Nick Torr",
        "Format": "DST",
        "DataType": "2012",
        "Processing": "Stripping20",
        "Reconstruction": 14,
        "Stripping": 20,
        "Date": "2013.02.15",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["moore.swim_strip20"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20121025"
    },
    "DST of offline-selected charm physics events for testing the swimming. See task #40021",
    test_file_db)

testfiles(
    "Sim08_2012_ForMoore", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08_2012_ForMoore/HepMCYes-10000000-100ev-20130225.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08_2012_ForMoore/HepMCNo-10000000-100ev-20130304.digi"
    ], {
        "Author": "Gloria Corti",
        "Format": "DIGI",
        "DataType": "2012",
        "Processing": "Sim08",
        "Date": "2013.02.26",
        "Application": "Boole",
        "Simulation": True,
        "QMTests": ["moore.mc2012", "l0app"],
        "DDDB": "dddb-20120831",
        "CondDB": "sim-20121025-vc-md100"
    },
    "Gauss/Boole output for testing Moore v14r8p6 in preparation for large scale 2012 simulation, Sim08, with Reco14 Stripping20. see task #39714",
    test_file_db)

testfiles(
    "Sim08_2012_L044", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08_2012_L044/HepMCYes-10000000-100ev-20130225-L044.digi"
    ], {
        "Author": "Gloria Corti, Rob Lambert",
        "Format": "DIGI",
        "DataType": "2012",
        "Processing": "Sim08",
        "Date": "2013.02.26",
        "Application": "Boole",
        "Simulation": True,
        "QMTests": ["moore.mc2012"],
        "DDDB": "dddb-20120831",
        "CondDB": "sim-20121025-vc-md100"
    },
    "Generated from Sim08_2012_ForMoore with L0 0x0044 added by L0App, for testing of new running of Moore MC after L0App.",
    test_file_db)

testfiles(
    "Sim08_2011_ForMoore", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim08_2011_ForMoore/Sim08-2011-Boole-10000000-20130508-100ev.digi"
    ], {
        "Author": "Gloria Corti",
        "Format": "DIGI",
        "DataType": "2011",
        "Processing": "Sim08",
        "Date": "2013.05.08",
        "Application": "Boole",
        "Simulation": True,
        "QMTests": ["moore.run_moore_mc2011"],
        "DDDB": "dddb-20130503",
        "CondDB": "sim-20130503-vc-md100"
    },
    "Gauss/Boole output for testing Moore v12r8g3 - OBSOLETE - replaced by Sim10_2011_ForMoore",
    test_file_db)

testfiles(
    "Sim10_2011_ForMoore", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Sim10d_2011_ForMoore/Sim10d-2011-L0App-20000000-20250121-10ev.digi"
    ], {
        "Author": "Marco Cattaneo",
        "Format": "DIGI",
        "DataType": "2011",
        "Processing": "Sim10",
        "Date": "2025.01.21",
        "Application": "L0App",
        "Simulation": True,
        "QMTests": ["moore.run_moore_mc2011"],
        "DDDB": "dddb-20220927-2011",
        "CondDB": "sim-20201113-1-vc-mu100-Sim10"
    },
    "Gauss/Boole/L0App output for testing Moore v12r8g6 in preparation 2011 simulation with, Sim10, Reco14, Stripping20. see issue #52",
    test_file_db)

testfiles(
    "2016_Hlt1_0x11361609", [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016_Hlt1_0x11361609/Run_0179276_20160705-%s.hlta0101_%s.mdf'
        % (f[0], f[1])
        for f in [['162951', '000'], ['163211', '001'], ['163432', '002'],
                  ['163656', '003'], ['163917', '004'], ['164139', '005'],
                  ['164401', '006'], ['164622', '007'], ['164843', '008'],
                  ['165107', '009'], ['165330', '010'], ['165556', '011']]
    ], {
        "Author": "Jessica Prisciandaro",
        "Format": "MDF",
        "DataType": "2016",
        "Processing": "Moore",
        "Date": "2016.07.05",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20160522",
        "InitialTime": "2016-07-05 15:29:00 UTC",
    }, "2016 data accepted by Hlt1 TCK 0x11361609", test_file_db)

testfiles(
    myname="2016NB_25ns_L0Filt0x1609",
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609/2016NB_0x1609_%s.mdf'
        % f for f in [
            '174822_01', '174823_01', '174824_01', '175266_01', '175266_02',
            '175269_02', '175363_01', '175364_01', '175364_02', '175431_02',
            '175492_02', '175585_01', '175585_02', '175589_01'
        ]
    ],
    qualifiers={
        'Author': 'Conor Fitzpatrick',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20160517",
        "InitialTime": "2016-05-31 08:40:00 UTC",  # during run 175589
        'Date': '2016-06-18',
        'Application': 'Moore'
    },
    comment='2015NB data that has been filtered through 0x1609 with L0App',
    test_file_db=test_file_db)

testfiles(
    myname="2016NB_25ns_L0Filt0x160B",
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x160B/2016NB_0x160B_%s.mdf'
        % i for i in [
            '174819_01', '174822_01', '174823_01', '174824_01', '175266_01',
            '175266_02', '175269_01', '175269_02', '175359_01', '175363_01',
            '175364_01', '175364_02', '175385_01', '175431_01', '175431_02',
            '175434_01', '175492_01', '175492_02', '175497_01', '175499_01',
            '175580_01', '175584_01', '175585_01', '175585_02', '175589_01'
        ]
    ],
    qualifiers={
        'Author': 'Conor Fitzpatrick, Mika Vesterinen',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20160517",
        "InitialTime": "2016-05-31 08:40:00 UTC",  # during run 175589
        'Date': '2016-07-25',
        'Application': 'Moore'
    },
    comment='2016NB data that has been filtered through 0x160B with L0App',
    test_file_db=test_file_db)

testfiles(
    "2015_pbpb_raw_full", [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2015/RAW/FULL/LHCb/LEAD15/169329/169329_0000000062.raw'
    ], {
        "Author": "Rosen Matev",
        "Format": "MDF",
        "DataType": "2015",
        "Date": "2016.02.01",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": ["brunel.2015magdown"],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150828",
    },
    "Lead-Lead collision raw data FULL stream, run 169329 from 8th December 2015",
    test_file_db)

testfiles(
    "pNe2015NB_L0Filt0x1608_small",
    [
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SMOG/pNe2015NB_L0Filt0x1608_BB_small.mdf',  # from 500 nobias events, run 161119
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SMOG/pNe2015NB_L0Filt0x1608_BE_small.mdf',  # from 2500 nobias events
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SMOG/pNe2015NB_L0Filt0x1608_EB_small.mdf',  # from 2500 nobias events
    ],
    {
        "Author": "Rosen Matev",
        "Format": "MDF",
        "DataType": "2015",
        "Processing": "Moore",
        "Date": "2016.06.01",
        "Application": "Moore",
        "Simulation": False,
        "QMTests": [],
        "DDDB": "dddb-20150724",
        "CondDB": "cond-20150805",
        "InitialTime": "2015-08-25 03:00:00 UTC",  # during run 161119
    },
    "2015 pNe nobias data that has been filtered through 0x1608 (few hundred events)",
    test_file_db)

testfiles(
    myname='2017HLTCommissioning_B2CC_MC2015_Bs2JpsiPhi_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bs2JpsiPhi_MagDown_L0Processed_0x160F/L0Processed_8.dst',
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'egovorko',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15Dev',
        'Date': '2017-02-13',
        'Application': 'Moore'
    },
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db)

testfiles(
    myname=
    '2017HLTCommissioning_B2CC_MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_8.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_14.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_15.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_19.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_21.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagDown_L0Processed_0x160F/L0Processed_23.dst',
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'egovorko',
        'DataType': '2015',
        'Processing': 'Sim08g',
        'Simulation': True,
        'CondDB': 'sim-20150119-3-vc-md100',
        'DDDB': 'dddb-20150119-3',
        'Reconstruction': 'Reco15Dev',
        'Date': '2017-02-13',
        'Application': 'Moore'
    },
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db)

testfiles(
    myname=
    '2017HLTCommissioning_B2CC_MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_11.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_12.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_15.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_16.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_18.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_19.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/B2CC/2017HltCommissioning_forTestFileDB/MC2015_Bu2JpsiK_Jpsi2ee_MagUp_L0Processed_0x160F/L0Processed_22.dst',
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'egovorko',
        'DataType': '2015',
        'Processing': 'Sim08g',
        'Simulation': True,
        'CondDB': 'sim-20150119-3-vc-mu100',
        'DDDB': 'dddb-20150119-3',
        'Reconstruction': 'Reco15Dev',
        'Date': '2017-02-13',
        'Application': 'Moore'
    },
    comment='For Moore efficiency, has been filtered through 0x160F with L0App',
    test_file_db=test_file_db)

###############  Auto-additions are appended to this file ###################
#
# There is now an automated mechanism to append to this database.
# See: $PRCONFIGROOT/scripts/AddToTestFilesDB.py

### auto created ###
testfiles(
    myname='2011_25ns_raw_default',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2011/RAW/FULL/LHCb/COLLISION11_25/103053/103053_0000000035.raw'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2011',
        'Format': 'MDF',
        'Simulation': False,
        'Date': '2013-11-26 15:03:01.559482',
        'DDDB': 'dddb-20130503',
        'CondDB': 'cond-20130522',
        'QMTests': 'brunel.brunel_25ns'
    },
    comment=
    'RAW data file with events from run 103053, fill 2186 on 2011-10-07, 25ns bunch spacing. DB tags are default 2011 tags in LHCb v36r3',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2010_MagUp_raw_default',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2010/RAW/FULL/LHCb/COLLISION10/69623/069623_0000000003.raw'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2010',
        'Format': 'MDF',
        'Simulation': False,
        'Date': '2013-11-26 15:56:50.465614',
        'DDDB': 'head-20110721',
        'CondDB': 'head-20110614',
        'QMTests': 'brunel.brunel2010magup'
    },
    comment=
    'Default RAW file for 2010 MagUp tests, 3500 GeV data. DB tags are default 2010 tags in LHCb v36r3',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2010_MagOff_raw_default',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2010/RAW/FULL/LHCb/COLLISION10/69947/069947_0000000004.raw'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2010',
        'Format': 'MDF',
        'Simulation': False,
        'Date': '2013-11-26 16:01:05.210397',
        'QMTests': 'brunel.brunel2010magoff'
    },
    comment=
    'Default RAW file for 2010 MagOff tests, 3500 GeV data. DB tags are default 2010 tags in LHCb v36r3',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2009_dst_read_test',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2009/DST/00006290/0000/00006290_00000001_1.dst'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DST',
        'Simulation': False,
        'CondDB': 'head-20110614',
        'DDDB': 'head-20101206',
        'Date': '2014-01-21 10:37:23.196590',
        'QMTests': 'ioexample.copy2009dsttoroot'
    },
    comment='Official POOL DST from 2009',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Reco10-sdst-10events',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Reco10-sdst-10events/00011652_00000001_1-evt-18641to18650.sdst'
    ],
    qualifiers={
        'Format': 'SDST',
        'Author': 'cattanem',
        'DataType': '2011',
        'Processing': 'Reco10',
        'Simulation': False,
        'CondDB': 'head-20110622',
        'DDDB': 'head-20110302',
        'Reconstruction': 10,
        'Date': '2014-01-21 15:14:33.219187',
        'Application': 'Brunel',
        'QMTests': 'ioexample.testunpacktrack'
    },
    comment=
    '10 events from Reco10 SDST, including one testing UnpackTrack bug fix in EventPacker v2r10',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC09-pool-dst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MC09-pool-dst/00001820_00000001.dst'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20090402-vc-mu100',
        'DDDB': 'head-20090330',
        'Date': '2014-01-21 17:20:23.041861',
        'Application': 'Brunel',
        'QMTests': 'ioexample.copypooldsttoroot'
    },
    comment=
    '100 minimum bias events produced with MC09 default settings, Gauss v37r0, Boole v18r0, Brunel v34r5',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC09-pool-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MC09-pool-digi/30000000-100ev-20090407-MC09.digi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20090508-vc-mu100',
        'DDDB': 'head-20090508',
        'Date': '2014-01-21 17:47:29.732855',
        'Application': 'Boole',
        'QMTests': 'ioexample.copypooldigitoroot'
    },
    comment=
    '100 minimum bias events produced with MC09 default settings, Gauss v37r0, Boole v19r2',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC09-pool-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MC09-pool-sim/30000000-100ev-20090407-MC09.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20090402-vc-mu100',
        'DDDB': 'head-20090330',
        'Date': '2014-01-21 17:51:25.261926',
        'Application': 'Gauss',
        'QMTests': 'ioexample.copypoolsimtoroot'
    },
    comment=
    '100 minimum bias events produced with MC09 default settings, Gauss v37r0',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2011_smallfiles_EW',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000049_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010654_00000049_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000040_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000168_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000163_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010352_00000014_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010352_00000002_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00010197_00000003_1.radiative.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00011760_00000130_1.dimuon.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2011_smallfiles_EW/00008385_00000552_1.bhadron.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'rlambert',
        'DataType': '2011',
        'Processing': 'Stripping13b',
        'Simulation': False,
        'CondDB': 'head-20111102',
        'DDDB': 'head-20110302',
        'Stripping': 'Stripping12',
        'Reconstruction': 'Reco08',
        'Date': '2014-02-12 11:43:35.705405',
        'Application': 'DaVinci',
        'QMTests': 'lhcbalgs.fsr-small-files-root'
    },
    comment=
    'Very small files generated in S13b, which have a few events only, but a lot of FSRs potentially. Used to test if FSRs really work properly. Copied from the grid by rlambert in 2011.',
    test_file_db=test_file_db)

testfiles(
    myname='2010_justFSR_EW',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2010_justFSR_EW/JustFSR.2010.EW.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'rlambert',
        'DataType': '2010',
        'Processing': 'Stripping12',
        'Simulation': False,
        'CondDB': 'head-20101112',
        'DDDB': 'head-20101026',
        'Stripping': 'Stripping12',
        'Reconstruction': 'Reco08',
        'Date': '2014-02-12 11:43:35.705405',
        'Application': 'DaVinci',
        'QMTests':
        ['lhcbalgs.fsr-small-files-root', 'lhcbalgs.fsr-only-file.root']
    },
    comment=
    'A file which contains nothing but FSRs. Early on in FSR development, files with only FSRs were not readable. This file is kept to test against future re-occurance of that.',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='boole.boole-mc11',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole.boole-mc11/Gauss-10000000-100ev-20111014.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2011',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20110723-vc-md100',
        'DDDB': 'head-20110914',
        'Date': '2014-02-13 09:02:22.212231',
        'Application': 'Gauss',
        'QMTests': 'boole.boole-mc11'
    },
    comment=
    'Generic b events with spillover from Gauss v40r4, settings as in $APPCONFIGOPTS/Gauss/beam35000GeV-md100-MC11-nu2-50ns.py',
    test_file_db=test_file_db)

testfiles(
    myname='MC2012.dst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2012/ALLSTREAMS.DST/00024784/0000/00024784_00000001_1.allstreams.dst'
    ],
    qualifiers={
        'Author': 'jonrob',
        'DataType': '2012',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'Sim08-20130326-1-vc-md100',
        'DDDB': 'Sim08-20130326-1',
        'Date': '2014-07-23 09:02:22.212231',
        'Application': 'Brunel',
        'QMTests': ['mc2012-testread']
    },
    comment='Example MC2012 DST (JpsiK)',
    test_file_db=test_file_db)

testfiles(
    myname='mc11a-xdst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2011/XDST/00012935/0000/00012935_00000827_3.xdst'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2011',
        'Format': 'XDST',
        'Simulation': True,
        'CondDB': 'sim-20111020-vc-md100',
        'DDDB': 'head-20110914',
        'Date': '2014-02-13 09:02:22.212231',
        'Application': 'Brunel',
        'QMTests': ['boole.boole-mc11a-xdst', 'brunel.brunel-mc11a-xdst']
    },
    comment='Inclusive b events Beam3500GeV-2011-MagDown-Nu2-50ns-EmNoCuts',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='boole.fixedfile-gauss-v39r4-mc10-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole.fixedfile-gauss-v39r4-mc10-sim/Gauss-v39r4-30000000-100ev-20110902-MC10.sim'
    ],
    qualifiers={
        'Author':
        'cattanem',
        'DataType':
        '2010',
        'Format':
        'SIM',
        'Simulation':
        True,
        'CondDB':
        'sim-20101210-vc-md100',
        'DDDB':
        'head-20101206',
        'Date':
        '2014-02-13 10:04:53.289232',
        'Application':
        'Gauss',
        'QMTests': [
            'boole.fixedfile-gauss-v39r4-mc10-sim', 'boole.boole-mc10',
            'boole.boole-mc10-notruth', 'boole.boole-fest',
            'boole.boole-2010-latestdb'
        ]
    },
    comment='MC10 file produced with Gauss v39r4',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel-mc10',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-mc10/Boole-v21r8p1-MC2010Tuning.digi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2010',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20101026-vc-mu100',
        'DDDB': 'head-20101026',
        'Date': '2014-02-13 16:13:31.976938',
        'Application': 'Boole',
        'QMTests': 'brunel.brunel-mc10-withtruth'
    },
    comment='Boole v21r8p1 digi file for 2010 MC tuning',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009/Boole-v19r8-30000000-100ev-20091112-vc-md100.digi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-13 17:21:20.645501',
        'Application': 'Boole',
        'QMTests': 'brunel.fixedfile-boole-v19r8-2009-digi'
    },
    comment='100 minimum bias events, 2009 geometry, with Boole v19r8',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel.fixedfile-sim2010',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel.fixedfile-sim2010/Boole-v21r9-00007107_00000001_2.digi'
    ],
    qualifiers={
        'Author':
        'cattanem',
        'DataType':
        '2010',
        'Format':
        'DIGI',
        'Simulation':
        True,
        'CondDB':
        'sim-20100624-vc-md100',
        'DDDB':
        'head-20100624',
        'Date':
        '2014-02-13 17:43:36.579037',
        'Application':
        'Boole',
        'QMTests': [
            'brunel.fixedfile-sim2010', 'brunel.fixedfile-sim2010-globaldb',
            'brunel.fixedfile-sim2010-localdb'
        ]
    },
    comment='Simulation of 2010 geometry with Boole v21r9',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009-mdf/Boole-v19r8-30000000-100ev-20091112-vc-md100.mdf'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-13 17:56:16.931913',
        'Application': 'Boole',
        'QMTests': 'brunel.fixedfile-boole-v19r8-2009-mdf'
    },
    comment=
    '100 minimum bias events, 2009 geometry, with Boole v19r8, mdf format - no MC truth',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009-extended',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009-extended/Boole-v19r8-30000000-100ev-20091112-vc-md100-extended.digi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-13 17:59:26.133915',
        'Application': 'Boole',
        'QMTests': 'brunel.fixedfile-boole-v19r8-2009-digi-extended'
    },
    comment=
    '100 minimum bias events, 2009 geometry, with Boole v19r8, xdigi format',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='brunel.fixedfile-boole-v19r8-2009-extended-spillover',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel.fixedfile-boole-v19r8-2009-extended-spillover/Boole-v19r8-30000000-100ev-20091112-vc-md100-extended-spillover.digi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2009',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-13 18:01:51.972516',
        'Application': 'Boole',
        'QMTests': 'brunel.fixedfile-boole-v19r8-2009-digi-extended-spillover'
    },
    comment=
    '100 minimum bias events, 2009 geometry, with Boole v19r8, with spillover, xdigi',
    test_file_db=test_file_db)

testfiles(
    myname='R08S14_smallfiles',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/R08S14_smallfiles/R08S14EW_small_1_5.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'rlambert',
        'DataType': '2010',
        'Processing': 'R08S14',
        'Simulation': False,
        'CondDB': 'head-20100531',
        'DDDB': 'head-20100531',
        'Stripping': 14,
        'Reconstruction': 8,
        'Date': '2014-02-14 11:08:29.713073',
        'Application': 'DaVinci',
        'QMTests': 'lhcbalgs.merge-small-files'
    },
    comment='Small files extracted from the grid for running FSR merging tests',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v35r8-2009',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v35r8-2009/Brunel-v35r8-30000000-100ev-20091112-vc-md100.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'rlambert',
        'DataType': 'MC09',
        'Processing': 'MC09',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-17 14:29:37.350134',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fixedfile'
    },
    comment=
    'Brunel output in 2009 checking that DaVinci is still able to read it',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v35r8-2009-wtruth',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v35r8-2009-wtruth/Brunel-v35r8-30000000-100ev-20091112-vc-md100-withTruth.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'rlambert',
        'DataType': 'MC09',
        'Processing': 'MC09',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-17 14:24:01.596343',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fixedfile'
    },
    comment=
    'Tests that DaVinci can still read very old file formats, fixedfile dst from 2009 with MCTruth',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v35r8-2009-xdst-wtruth',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v35r8-2009-xdst-wtruth/Brunel-v35r8-30000000-100ev-20091112-vc-md100-withTruth.xdst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'rlambert',
        'DataType': 'MC09',
        'Processing': 'MC09',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-17 14:24:01.596343',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fixedfile'
    },
    comment=
    'Tests that DaVinci can still read very old file formats, fixedfile xdst from 2009 with MCTruth',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v35r8-2009-xdst-wtruth-wspill',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v35r8-2009-xdst-wtruth-wspill/Brunel-v35r8-30000000-100ev-20091112-vc-md100-withTruth-spillover.xdst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'rlambert',
        'DataType': 'MC09',
        'Processing': 'MC09',
        'Simulation': True,
        'CondDB': 'sim-20091112-vc-md100',
        'DDDB': 'head-20091112',
        'Date': '2014-02-17 14:24:01.596343',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fixedfile'
    },
    comment=
    'Tests that DaVinci can still read very old file formats, fixedfile xdst from 2009 with MCTruth, with spillover',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v37r3-fsrs-md',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v37r3-fsrs-md/Brunel-v37r3-20ev-FSR1.dst'
    ],
    qualifiers={
        'Author': 'rlambert',
        'DataType': '2010',
        'Format': 'DST',
        'Simulation': False,
        'CondDB': 'head-20110614',
        'DDDB': 'head-20110721',
        'Date': '2014-02-17 14:46:53.777561',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fsrs'
    },
    comment=
    'Input test file with FSRs for checking the streaming procedires of the stripping in DaVinci. Magnet Down.',
    test_file_db=test_file_db)

testfiles(
    myname='brunel-v37r3-fsrs-mu',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/brunel-v37r3-fsrs-mu/Brunel-v37r3-20ev-FSR2.dst'
    ],
    qualifiers={
        'Author': 'rlambert',
        'DataType': '2010',
        'Format': 'DST',
        'Simulation': False,
        'CondDB': 'head-20110614',
        'DDDB': 'head-20110721',
        'Date': '2014-02-17 14:46:53.777561',
        'Application': 'DaVinci',
        'QMTests': 'davinci.fsrs'
    },
    comment=
    'Input test file with FSRs for checking the streaming procedires of the stripping in DaVinci. Magnet Up.',
    test_file_db=test_file_db)

testfiles(
    myname='MC2010_BdJPsiKs',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MC2010_BdJPsiKs/MC2010_BdJPsiKs_00008414_00000106_1.dst'
    ],
    qualifiers={
        'Author': 'jonrob',
        'DataType': '2010',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20100715-vc-md100',
        'DDDB': 'head-20100624',
        'Date': '2014-03-07 17:16:03.678324',
        'Application': 'Brunel'
    },
    comment='DaVinci MC2010 decaytreeTuple test',
    test_file_db=test_file_db)

testfiles(
    myname='Brunel-SIM08-MinBias-Beam6500GeV-md100-nu4.8.xdst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Brunel-SIM08-MinBias-Beam6500GeV-md100-nu4.8.xdst/Brunel_Beam6500GeV-md100-nu4.8.xdst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'dhcroft',
        'DataType': 'XDST',
        'Processing': 'SIM08',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Stripping': None,
        'Reconstruction': 'Brunelv44r7',
        'Date': '2014-03-21 16:28:57.998963',
        'Application': 'Brunel',
        'QMTests': 'boole-sim08-xdst.qmt'
    },
    comment=
    '1000 MinBias XDST events used to test reprocessing in Boole of XDSTs',
    test_file_db=test_file_db)

testfiles(
    myname='MC10-MinBias',
    filenames=[
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/MC10/ALLSTREAMS.DST/00008898/0000/00008898_00000002_1.allstreams.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'cattanem',
        'DataType': '2010',
        'Processing': 'MC10',
        'Simulation': True,
        'CondDB': 'sim-20101210-vc-md100',
        'DDDB': 'head-20101206',
        'Reconstruction': '8',
        'Date': '2014-04-11 10:04:18.947716',
        'QMTests': 'ioexample.copymc10dsttoroot'
    },
    comment='MC10 minimum bias DST',
    test_file_db=test_file_db)

testfiles(
    myname='Reco08-charm.mdst',
    filenames=[
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2010/CHARM.MDST/00008397/0000/00008397_00000939_1.charm.mdst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'cattanem',
        'DataType': '2010',
        'Processing': 'Reco08',
        'Simulation': False,
        'CondDB': 'head-20101112',
        'DDDB': 'head-20101206',
        'Reconstruction': '8',
        'Stripping': '12',
        'Date': '2014-04-11 10:04:18.947716',
        'QMTests': 'ioexample.copyreco08mdsttoroot'
    },
    comment='2010 Reco08 CHARM.MDST',
    test_file_db=test_file_db)

testfiles(
    myname='Reco08-bhadron.dst',
    filenames=[
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2010/BHADRON.DST/00008399/0000/00008399_00001052_1.bhadron.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'cattanem',
        'DataType': '2010',
        'Processing': 'Reco08',
        'Simulation': False,
        'CondDB': 'head-20101112',
        'DDDB': 'head-20101206',
        'Reconstruction': '8',
        'Stripping': '12',
        'Date': '2014-04-11 10:04:18.947716',
        'QMTests': 'ioexample.copyreco08dsttoroot'
    },
    comment='2010 Reco08 BHADRON.DST',
    test_file_db=test_file_db)

testfiles(
    myname='R12S17-bhadron.dst',
    filenames=[
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision11/BHADRON.DST/00012545/0000/00012545_00000003_1.bhadron.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'cattanem',
        'DataType': '2011',
        'Processing': 'R21S17',
        'Simulation': False,
        'CondDB': 'head-20110914',
        'DDDB': 'head-20110914',
        'Reconstruction': '12',
        'Stripping': '17',
        'Date': '2014-04-11 10:04:18.947716',
        'QMTests': 'ioexample.copyreco12stripping17dsttoroot'
    },
    comment='2011 R12S17 BHADRON.DST',
    test_file_db=test_file_db)

testfiles(
    myname='R12S17-charm.mdst',
    filenames=[
        'PFN:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision11/CHARM.MDST/00012547/0000/00012547_00000013_1.charm.mdst'
    ],
    qualifiers={
        'Format': 'MDST',
        'Author': 'cattanem',
        'DataType': '2011',
        'Processing': 'R21S17',
        'Simulation': False,
        'CondDB': 'head-20110914',
        'DDDB': 'head-20110914',
        'Reconstruction': '12',
        'Stripping': '17',
        'Date': '2014-04-11 10:04:18.947716',
        'QMTests': 'ioexample.copyreco12stripping17mdsttoroot'
    },
    comment='2011 R12S17 CHARM.MDST',
    test_file_db=test_file_db)

testfiles(
    "R14S20-bhadron.mdst", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/BHADRON.MDST/00020198/0000/00020198_00000758_1.bhadron.mdst"
    ], {
        "Author": "cattanem",
        "Format": "MDST",
        "DataType": "2012",
        "Date": "2014.04.11",
        "Processing": "R14S20",
        "Reconstruction": 14,
        "Stripping": 20,
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["ioexample.copyreco14stripping20mdsttoroot"],
        "DDDB": "dddb-20120831",
        "CondDB": "cond-20120831"
    }, "2012 R14S20 BHADRON.MDST", test_file_db)

testfiles(
    myname='upgrade-reprocessing-xdst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDST/00032467/0000/00032467_00000002_1.xdst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'yamhis',
        'DataType': 'Upgrade',
        'Processing': 'Step-125740',
        'Simulation': True,
        'CondDB': 'sim-20130830-vc-md100',
        'DDDB': 'dddb-20131025',
        'Reconstruction': '14',
        'Date': '2014-06-18 17:56:23.229933',
        'Application': 'Boole',
        'QMTests': 'boole-reprocess-xdst.qmt'
    },
    comment='xdst for reprocessing of upgrade MC sample',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-minimal-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-minimal-sim/Minimal.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'boole-upgrade-minimal.qmt'
    },
    comment='Simulation for studies of minimal upgrade detector',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-minimal-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-minimal-xdigi/Minimal.xdigi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Application': 'Brunel',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'brunel-upgrade-minimal.qmt'
    },
    comment='Digitisation for studies of minimal upgrade detector',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-calo_nospdprs-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-calo_nospdprs-sim/Calo_NoSpdPrs.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'boole-upgrade-calo_nospdprs.qmt'
    },
    comment=
    'Simulation for studies of minimal upgrade detector without spd and prs',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-calo_nospdprs-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-calo_nospdprs-xdigi/Calo_NoSpdPrs.xdigi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'brunel-upgrade-calo_nospdprs.qmt'
    },
    comment=
    'Digitisation for studies of minimal upgrade detector without spd and prs',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-muon_nom1-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-muon_nom1-sim/Muon_NoM1.sim'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'boole-upgrade-muon_nom1.qmt'
    },
    comment='Simulation for studies of minimal upgrade detector without M1',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-muon_nom1-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-muon_nom1-xdigi/Muon_NoM1.xdigi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130808',
        'Date': '2014-06-23 12:47:20.529593',
        'QMTests': 'brunel-upgrade-muon_nom1.qmt'
    },
    comment='Digitisation for studies of minimal upgrade detector without M1',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-reprocessing-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00027904/0000/00027904_00000001_2.xdigi'
    ],
    qualifiers={
        'Author': 'yamhis',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130806',
        'Date': '2014-07-02 10:43:43.592234',
        'Application': 'Boole',
        'QMTests': 'boole-reprocess-xdigi.qmt'
    },
    comment='files to test reprocessing of xdigi files in Boole',
    test_file_db=test_file_db)

testfiles(
    myname='Sim09-2016-xdigi-LHCBGAUSS-1008',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2016/XDIGI/00064502/0000/00064502_00000001_1.xdigi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2015',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20161124-2-vc-md100',
        'DDDB': 'dddb-20150724',
        'Date': '2018-04-30 12:18:45',
        'Application': 'Boole',
        'QMTests': 'boole-reprocess-xdigi.qmt'
    },
    comment=
    'file to test reprocessing of xdigi file in Boole/digi14-patches, see LHCBGAUSS-1008',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='MC2015_digi_nu2.6_B2Kstmumu_L0',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/digi/MC2015_digi_nu2.6_B2Kstmumu_L0.dst'
    ],
    qualifiers={
        'Author': 'sneubert',
        'DataType': '2012',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2014-08-28 15:36:15.365588'
    },
    comment='L0 processed digi file for 2015 tracking studies',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='upgrade-rotnm_foil_gbt-sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319117/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319127/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319139/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319157/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319176/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319198/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319211/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319224/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319234/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319236/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319237/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319254/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319263/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319275/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319284/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319293/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319303/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319314/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319320/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319335/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319341/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319359/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319382/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319392/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319409/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319413/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319419/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319422/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319440/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319444/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319447/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319450/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319495/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319502/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319506/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319516/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319527/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319534/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319557/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319569/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319585/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319592/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319597/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319603/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319622/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319632/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319639/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319651/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319664/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319675/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319684/OctTest-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/user/t/tbird/2014_07/82319/82319692/OctTest-Extended.digi'
    ],
    qualifiers={
        'Author': 'lben',
        'DataType': 'Upgrade',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20130722-vc-md100',
        'DDDB': 'dddb-20130806',
        'Date': '2014-08-21 10:04:11.651755',
        'Application': 'Brunel'
    },
    comment='Test files for the Velo Pixel created by Tim Head',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2012_CaloFemtoDST',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000001_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000002_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000003_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000004_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000005_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000006_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000007_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000008_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000009_1.fmdst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FMDST/00021391/0000/00021391_00000010_1.fmdst'
    ],
    qualifiers={
        'Author': 'jonesc',
        'DataType': '2012',
        'Format': 'FMDST',
        'Simulation': False,
        'CondDB': 'cond-20140604',
        'DDDB': 'dddb-20130929-1',
        'Date': '2014-09-29 12:59:33.800587'
    },
    comment='CALO FemtoDST tests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2017HLTCommissioning_BnoC_MC2015_Bs2PhiPhi_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_11.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_12.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_8.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagUp/MC2015_Bs2PhiPhi_MagUp_L0Processed_9.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_0.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_10.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_11.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_12.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_13.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_14.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_15.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_16.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_17.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_18.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_2.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_3.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_4.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_5.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_6.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_7.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_8.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/BnoC/Bs2PhiPhi/MagDown/MC2015_Bs2PhiPhi_MagDown_L0Processed_9.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'jmccarth',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Reconstruction': 'Reco15DEV',
        'Date': '2014-11-06 15:02:16.232429',
        'Application': 'Moore'
    },
    comment=
    'MC2015 samples of Bs2PhiPhi, processed for Run II trigger studies, MagUp and MagDown, for the BnoC group',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    'RDWG_MC2015_Bu2JpsiK_MagUp_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/BuJpsiK/MagUp/L0processed_5.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-mu100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 10:32:30.677116'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    'RDWG_MC2015_Bs2PhiGamma_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_4.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_5.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_6.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_7.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_8.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_9.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_10.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_11.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_12.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_13.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_14.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_15.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_16.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_17.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_18.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bs2PhiGamma/MagDown/L0processed_19.dst'
    ],
    qualifiers={
        'Format': 'XDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 11:46:56.208205'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    'RDWG_MC2015_Bd2Ksmumu_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_0.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_1.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_2.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_3.dst,root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015_L0Processed/filtered/RDWG/Bd2Ksmumu/MagDown/L0processed_4.dst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'apuignav',
        'DataType': '2015',
        'Processing': 'Sim08f-r2',
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Date': '2014-11-11 12:11:11.895297'
    },
    comment='Dataset for 2015 trigger optimization',
    test_file_db=test_file_db)

testfiles(
    '2017HLTCommissioning_BnoC_MC2016_B2pppp_MagDown',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_10.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_11.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_12.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_13.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_14.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_15.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_16.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_17.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_18.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_19.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_20.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2016/BnoC/B2pppp/MagDown/BnoC_MC2016_B2pppp_MagDown_9.digi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'amerli',
        'DataType': '2016',
        'Processing': 'Sim09b',
        'Simulation': True,
        'CondDB': 'sim-20161124-2-vc-md100',
        'DDDB': 'dddb-20150724',
        'Reconstruction': '',
        'Date': '2017-02-17 12:50',
        'Application': 'Moore'
    },
    comment='MC2016 sample of B2ppbarppbar MagDown digitalized no L0 processed',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Tesla_Bsphiphi_MC12wTurbo',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/RemadeReports_HEAD_14-1-15_Bsphiphi_1k.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'sbenson',
        'DataType': '2012',
        'Processing': 'Sim08plusTurbo',
        'Simulation': True,
        'CondDB': 'sim-20130503-vc-md100',
        'DDDB': 'dddb-20130503',
        'Stripping': 20,
        'Reconstruction': 14,
        'Date': '2015-03-27 23:28:13.682936',
        'Application': 'DaVinci',
        'QMTests': 'tesla.containers,tesla.default'
    },
    comment='check resurrection of decay from raw bank',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='RDWG_MC2015_Bs2gammagamma_MagDown_L0Processed',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/MC2015/BsGammaGamma_Stripped_Truth_0xFF66/MC15Dev_0_Truth.SeqBs2GG_none.dst'
    ],
    qualifiers={
        'Format': 'DST',
        'Author': 'sbenson',
        'DataType': '2015',
        'Processing': 65382,
        'Simulation': True,
        'CondDB': 'sim-20140730-vc-md100',
        'DDDB': 'dddb-20140729',
        'Stripping': 'Custom',
        'Reconstruction': 'Reco15Dev',
        'Date': '2015-05-24 12:41:17.316214',
        'Application': 'Moore',
        'QMTests': 'Moore_Neutral'
    },
    comment='LHCbIntegrationTests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='2015HLTValidationData_L0filtered_0x0050',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2015ValidationData/1406_L0filtered_25ns_0x0050/2015NB_L0filtered_0x0050.mdf'
    ],
    qualifiers={
        'Author': 'raaij',
        'DataType': '2015',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20150617',
        'DDDB': 'dddb-20150526',
        'Date': '2015-07-22 18:06:43.143827',
        'Application': 'Moore'
    },
    comment='Validation of Moore for 25 ns running in August of 2015',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='TeslaTest_TCK_0x022600a2',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/TeslaTest_TCK_0x022600a2.dst'
    ],
    qualifiers={
        'Author': 'sbenson',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': False,
        'CondDB': 'cond-20150828',
        'DDDB': 'dddb-20150724',
        'Date': '2016-03-29 18:05:33.296452',
        'Application': 'Moore'
    },
    comment='For Tesla output container checks',
    test_file_db=test_file_db)

testfiles(
    myname='TeslaTest_2015raw_0xFF66',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/TurboRaw_L0_0xFF66.mdf'
    ],
    qualifiers={
        'Author': 'sbenson',
        'DataType': '2015',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20150828',
        'DDDB': 'dddb-20150724',
        'Date': '2016-04-07 23:41:33.296452',
        'Application': 'Moore'
    },
    comment='For integration persist reco. tests',
    test_file_db=test_file_db)

testfiles(
    myname='TeslaTest_2016raw_0x11361609_0x21361609',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/2016raw_0x11361609_0x21361609/179348_0000000227.raw'
    ],
    qualifiers={
        'Author': 'mvesteri',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20160522',
        'DDDB': 'dddb-20150724',
        'InitialTime': '2016-07-07 03:54:16 UTC',
        'Date': '2016-07-21',
        'Application': 'Moore'
    },
    comment='A turbo raw file for tesla testing',
    test_file_db=test_file_db)

testfiles(
    myname="Juggled_MC_2015_27163003_DstToD0pip_D0ToKmPip",
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SignalMC/Juggled_MC_2015_27163003_Beam6500GeVJun2015MagDownNu1.6Pythia8_Sim08h/juggled.dst'
    ],
    qualifiers={
        'Author': 'mvesteri',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2016-05-18',
        'Application': 'Moore'
    },
    comment=
    'Signal MC sample for D*->D0pi, D0->Kpi. To test efficiency version of Moore_RateTest.py',
    test_file_db=test_file_db)

testfiles(
    myname="MC_2015_27163003_DstToD0pip_D0ToKmPip",
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/SignalMC/MC_2015_27163003_Beam6500GeVJun2015MagDownNu1.6Pythia8_Sim08h/00046271_00000135_2.dst'
    ],
    qualifiers={
        'Author': 'mvesteri',
        'DataType': '2015',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20131023-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2016-05-18',
        'Application': 'Moore'
    },
    comment=
    'Signal MC sample for D*->D0pi, D0->Kpi. To test efficiency version of Moore_RateTest.py',
    test_file_db=test_file_db)

testfiles(
    myname="2016NB_25ns_L0Filt0x1609_large",
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174819_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174822_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174823_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_174824_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175266_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175266_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175269_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175269_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175359_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175363_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175364_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175364_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175385_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175431_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175431_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175434_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175492_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175492_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175497_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175499_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175580_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175585_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175585_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1609_large/2016NB_0x1609_175589_01.mdf'
    ],
    qualifiers={
        'Author': 'Conor Fitzpatrick',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        "InitialTime": "2016-05-31 08:40:00 UTC",  # during run 175589
        'Date': '2016-06-21',
        'Application': 'Moore'
    },
    comment=
    'Larger sample of 2016NB data that has been filtered through 0x1609 with L0App',
    test_file_db=test_file_db)

testfiles(
    myname="HltDAQ-routingbits_nobias",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltDAQ-routingbits_nobias/174909_0000000095_nobias.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        'InitialTime': '2016-05-21 01:42:00 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test file for test HltDAQ.routingbits_nobias',
    test_file_db=test_file_db)

testfiles(
    myname="HltDAQ-routingbits_full",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltDAQ-routingbits_full/174375_0000000619_full.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        'InitialTime': '2016-05-14 08:30:00 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test file for test HltDAQ.routingbits_full',
    test_file_db=test_file_db)

testfiles(
    myname="HltDAQ-routingbits_beamgas",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltDAQ-routingbits_beamgas/174909_0000000705_beamgas.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        'InitialTime': '2016-05-21 01:42:00 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test file for test HltDAQ.routingbits_beamgas',
    test_file_db=test_file_db)

testfiles(
    myname="HltDAQ-routingbits_turcal",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltDAQ-routingbits_turcal/174892_0000000258_turcal.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        'InitialTime': '2016-05-20 20:33:00 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test file for test HltDAQ.routingbits_turcal',
    test_file_db=test_file_db)

testfiles(
    myname="HltDAQ-routingbits_turbo",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltDAQ-routingbits_turbo/174909_0000000760_turbo.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160517',
        'InitialTime': '2016-05-21 01:42:00 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test file for test HltDAQ.routingbits_turbo',
    test_file_db=test_file_db)

testfiles(
    myname="HltServices-close_cdb_file",
    filenames=[
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltServices-close_cdb_file/StripHLT_0x11291600.raw',
        'mdf:root://eoslhcb.cern.ch///eos/lhcb/cern-swtest/lhcb/swtest/HltServices-close_cdb_file/StripHLT_0x11291603.raw'
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'DDDB': 'dddb-20150724',
        'CondDB': 'cond-20160420',
        'InitialTime': '2016-05-17 05:26:16 UTC',
        'Date': '2016-06-28',
        'Application': 'LHCb'
    },
    comment='Test files for test HltServices.close_cdb_file',
    test_file_db=test_file_db)

testfiles(
    myname="upgrade-Brunel-Baseline-DIGI",
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Custom_Geoms_Upgrade/samples/UFT_5x/Bs2PhiPhi_MagDown/UFT5x_13104012_MagDown_Noise_LightSharing_NewDeposit_StepSinglePowerAttMap_AllLHCb-10ev-Extended.digi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'tnikodem',
        'DataType': 'Upgrade',
        'Simulation': True,
        'CondDB': 'sim-20150716-vc-md100',
        'DDDB': 'dddb-20160304',
        'Date': '2016-06-30',
        'Application': 'Brunel',
        'QMTests': 'Brunel'
    },
    comment='Test file for upgrade Brunel qmtests',
    test_file_db=test_file_db)

testfiles(
    myname='2016NB_25ns_L0Filt0x1715',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2016CommissioningDatasets/2016NB_25ns_L0Filt0x1715/RedoL0.mdf'
    ],
    qualifiers={
        'Author': 'sely',
        'DataType': '2016',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20160517',
        'DDDB': 'dddb-20150724',
        "InitialTime": "2016-05-31 10:40:00 UTC",
        'Date': '2016-08-03',
        'Application': 'L0App'
    },
    comment=
    'Sample of 2016 No Bias data filtered with 0x1715 for Nightly test of Calibration microbias configuration',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='pPb2013_pPb_NoBias_L0Filter0x161B',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/LHCBPS-1592/pPb_L0TCK_0x161D_MagDown.raw'
    ],
    qualifiers={
        'Author': 'zhangy',
        'DataType': '2012',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20120831',
        'DDDB': 'dddb-20120831',
        'Date': '2016-08-18 10:33:38.493052'
    },
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='pPb2013_Pbp_NoBias_L0Filter0x161B',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/LHCBPS-1592/Pbp_L0TCK_0x161D_MagDown.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/LHCBPS-1592/Pbp_L0TCK_0x161D_MagUp.raw'
    ],
    qualifiers={
        'Author': 'zhangy',
        'DataType': '2012',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20120831',
        'DDDB': 'dddb-20120831',
        'Date': '2016-08-18 10:35:19.150018'
    },
    comment='For development of 2016 pPb trigger',
    test_file_db=test_file_db)

testfiles(
    myname='genFSR_2012_digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Boole-13114005-test1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Boole-13114005-test2.digi'
    ],
    qualifiers={
        'Author': 'dfazzini',
        'DataType': '2012',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20130522-1-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2016-10-01'
    },
    comment='For test of generation FSR propagation',
    test_file_db=test_file_db)

testfiles(
    myname='genFSR_2012_digi_unpacked',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Boole-13114005-unpacked.digi'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2012',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20130522-1-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2016-10-01'
    },
    comment=
    'Same data as in genFSR_2012_digi, but with unpacked pSim containers',
    test_file_db=test_file_db)

testfiles(
    myname='genFSR_2012_dst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Brunel-13114005-test1.dst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Brunel-13114005-test2.dst'
    ],
    qualifiers={
        'Author': 'dfazzini',
        'DataType': '2012',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20130522-1-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2016-10-01'
    },
    comment='For test of generation FSR propagation',
    test_file_db=test_file_db)

testfiles(
    myname='genFSR_2012_sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Gauss-13114005-test1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Gauss-13114005-test2.sim'
    ],
    qualifiers={
        'Author': 'dfazzini',
        'DataType': '2012',
        'Format': 'SIM',
        'CondDB': 'sim-20130522-1-vc-md100',
        'DDDB': 'dddb-20130929-1',
        'Date': '2016-10-25'
    },
    comment='For test of generation FSR propagation',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-FT61-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SciFi-v61/Boole-13104012-100ev-20170301.digi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'Jeroen van Tilburg',
        'DataType': 'Upgrade',
        'Simulation': True,
        'CondDB': 'sim-20170301-vc-md100',
        'DDDB': 'dddb-20170301',
        'Date': '2017-03-01',
        'QMTests': 'brunel-upgrade-baseline.qmt'
    },
    comment='digi for testing upgrade Brunel',
    test_file_db=test_file_db)

testfiles(
    myname='2017NB_L0Filt0x1706',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_01.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_02.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_03.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_04.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_05.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_06.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_07.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_08.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_09.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_100.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_101.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_102.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_103.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_104.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_105.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_106.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_107.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_108.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_109.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_10.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_110.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_111.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_112.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_113.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_114.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_115.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_116.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_117.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_118.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_119.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_11.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_120.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_12.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_13.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_14.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_15.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_16.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_17.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_18.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_19.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_20.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_21.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_22.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_23.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_24.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_25.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_26.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_27.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_28.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_29.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_30.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_31.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_32.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_33.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_34.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_35.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_36.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_37.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_38.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_39.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_40.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_41.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_42.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_43.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_44.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_45.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_46.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_47.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_48.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_49.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_50.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_51.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_52.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_53.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_54.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_55.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_56.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_57.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_58.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_59.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_60.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_61.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_62.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_63.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_64.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_65.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_66.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_67.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_68.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_69.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_70.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_71.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_72.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_73.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_74.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_75.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_76.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_77.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_78.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_79.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_80.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_81.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_82.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_83.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_84.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_85.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_86.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_87.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_88.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_89.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_90.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_91.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_92.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_93.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_94.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_95.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_96.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_97.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_98.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017NB_0x1706/2017NB_0x1706_192305_99.mdf'
    ],
    qualifiers={
        'Author': 'cofitzpa',
        'DataType': '2017',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20170510',
        'DDDB': 'dddb-20150724',
        'Date': '2017-06-20',
        'Application': 'L0App',
    },
    comment=
    'Sample of 2017 No Bias data filtered with 0x1706 for rate tests and 2017 commissioning',
    test_file_db=test_file_db)

testfiles(
    myname='2017_Hlt1_0x11611709',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2017CommissioningDatasets/2017HLT1/Run_0201852_20171106-{0}.hltf0111.mdf'
        .format(i) for i in [
            222031, 222252, 222426, 222513, 222644, 222905, 223039, 223259,
            223431, 223517, 223649, 223906, 224040, 224125, 224256, 224339,
            224511, 224729, 224901, 225119, 225251, 225337, 225507, 225725,
            225857, 225942, 230116, 230203, 230335, 230554, 230724, 230811,
            230942, 231114, 231158, 231331, 231549, 231807, 231938
        ]
    ],
    qualifiers={
        'Author': 'rmatev',
        'Date': '2018-02-27',
        'DataType': '2017',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20170724',
        'DDDB': 'dddb-20150724',
        'InitialTime': '2017-11-06 21:20:00 UTC',  # run 201852
    },
    comment='Sample of 2017 HLT1-accepted data with 0x11611709',
    test_file_db=test_file_db)

testfiles(
    myname='2017NB_L0Filt0x18A1',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/2018CommissioningDatasets/2017NB_0x18A1/2017NB_0x18A1_197458_{0:02}.mdf'
        .format(i) for i in range(1, 22)
    ],
    qualifiers={
        'Author': 'rmatev',
        'Date': '2018-04-05',
        'DataType': '2017',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20170724',
        'DDDB': 'dddb-20150724',
        'InitialTime': '2017-08-18 13:03:00 UTC',  # run 197458
    },
    comment=
    'Sample of 2017 No Bias data filtered with 0x18A1 for rate tests and 2018 commissioning',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='upgrade_integration_test',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/LHCbIntegrationTest/Gauss-30000000-10ev-20171212.sim'
    ],
    qualifiers={
        'Author': 'sstahl',
        'DataType': 'SIM',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20171126',
        'Date': '2018-01-11 13:46:49.273953'
    },
    comment=
    'A sample of 10 events, privately produced, to test the integration of applications from Boole to DaVinci in Upgrade conditions',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-throughput-minbias-nogec-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade/MINBIASTESTSAMPLE_big.mdf'
    ],
    qualifiers={
        'Author': 'gligorov',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20171126',
        'Date': '2018-02-06'
    },
    comment=
    'A sample of upgrade events in MDF format with no GEC applied for throughput tests',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-throughput-minbias-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade/MINBIASTESTSAMPLE_big_GEC.mdf'
    ],
    qualifiers={
        'Author': 'gligorov',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20171126',
        'Date': '2018-02-06'
    },
    comment=
    'A sample of upgrade events in MDF format with a GEC at 11000 FT hits applied for throughput tests',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-prchecker-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade/BBARTESTSAMPLE_1.digi'
    ],
    qualifiers={
        'Author': 'gligorov',
        'DataType': 'Upgrade',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20171126',
        'Date': '2018-02-02'
    },
    comment=
    'A sample of upgrade events in DIGI format applied for PrChecker tests',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim09c-up02-minbias-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeMinbiasDigi/00070647_{0:08}_1.digi'
        .format(i) for i in range(1, 31)
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'mramospe',
        'DataType': 'Upgrade',
        'Processing': 'Sim09c-Up02',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20171126',
        'Date': '2018-11-06'
    },
    comment=
    'Upgrade minbias MC in DIGI format, used to test reconstruction algorithms',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim09c-up02-minbias-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeMinbiasXDigi/00067189_{0:08}_1.xdigi'
        .format(i) for i in range(1, 31)
    ],
    qualifiers={
        'Format': 'XDIGI',
        'Author': 'mramospe',
        'DataType': 'Upgrade',
        'Processing': 'Sim09c-Up02',
        'Simulation': True,
        'CondDB': 'sim-20170301-vc-md100',
        'DDDB': 'dddb-20171010',
        'Date': '2019-04-01'
    },
    comment=
    'Upgrade minbias MC in XDIGI format, used to test reconstruction and muon identification algorithms',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim09c-up02-reco-up01-minbias-ldst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeMinbias/00069155_00000021_2.ldst'
    ],
    qualifiers={
        'Format': 'LDST',
        'Author': 'olupton',
        'DataType': 'Upgrade',
        'Processing': 'Sim09c-Up02',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20171126',
        'Reconstruction': 'Reco-Up01',
        'Date': '2018-04-25'
    },
    comment=
    'Upgrade minbias MC in LDST format, used for TCK creation for upgrade MC filtering for selection studies',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim09c-up02-34112100-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeSignalDigi/K0S2mu2/K0S2mu2_MCUpgrade_34112100_MagDown_{0:02}.digi'
        .format(i) for i in range(0, 37)
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'mramospe',
        'DataType': 'Upgrade',
        'Processing': 'Sim09c-Up02',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20171126',
        'Date': '2018-11-28'
    },
    comment=
    'Upgrade KS0 -> mu+ mu- MC in DIGI format, used to test reconstruction algorithms',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up01-34112106-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeSignalXDigi/34112106/K0S2mu2_MCUpgrade_34112106_MagUp_{0:02}.xdigi'
        .format(i) for i in range(0, 45)
    ],
    qualifiers={
        'Format': 'XDIGI',
        'Author': 'mramospe',
        'DataType': 'Upgrade',
        'Processing': 'Sim10-Up01',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-mu100',
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20190223',
        'Date': '2019-04-03'
    },
    comment=
    'Upgrade KS0 -> mu+ mu- MC in XDIGI format, used to test reconstruction and muon identification algorithms',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-sim10-up01-24142001-xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/UpgradeSignalXDigi/24142001/Jpsi2mu2_MCUpgrade_24142001_MagDown_{0:02}.xdigi'
        .format(i) for i in range(0, 91)
    ],
    qualifiers={
        'Format': 'XDIGI',
        'Author': 'mramospe',
        'DataType': 'Upgrade',
        'Processing': 'Sim10-Up01',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20190223',
        'Date': '2019-08-01'
    },
    comment=
    'Upgrade inclusive Jpsi -> mu+ mu- MC in XDIGI format, used to test reconstruction and muon identification algorithms',
    test_file_db=test_file_db)

testfiles(
    "2015_DaVinciTests.davinci.gaudipython_algs", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision15/RDST/00048237/0000/00048237_00008700_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision15/RDST/00048237/0000/00048237_00008701_1.rdst"
    ], {
        "Author": "Eduardo Rodrigues",
        "Format": "DST",
        "DataType": "2015",
        "Date": "2018.05.30",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.davinci.gaudipython_algs"],
    }, "Proton-Proton collision data, Reco15a, run 164668", test_file_db)

testfiles(
    "2016_DaVinciTests.stripping28X_collision16_reco16", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision16/RDST/00053196/0010/00053196_00100516_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision16/RDST/00053196/0009/00053196_00099798_1.rdst"
    ], {
        "Author": "Eduardo Rodrigues",
        "Format": "DST",
        "DataType": "2016",
        "Date": "2018.06.01",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": [""],
    }, "Proton-Proton collision data from 2016", test_file_db)

testfiles(
    "2017_DaVinciTests.stripping29r2_collision17_reco17", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00064899/0006/00064899_00065694_1.rdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision17/RDST/00064899/0006/00064899_00065751_1.rdst"
    ], {
        "Author": "Eduardo Rodrigues",
        "Format": "DST",
        "DataType": "2017",
        "Date": "2018.06.04",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.stripping29r2_collision17_reco17"],
    }, "Proton-Proton collision data from 2017", test_file_db)

testfiles(
    "2012_DaVinciTests.stripping.test_express_appconfig", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00020330/0004/00020330_00047243_1.full.dst"
    ], {
        "Author": "Eduardo Rodrigues",
        "Format": "DST",
        "DataType": "2012",
        "Date": "2018.06.01",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.stripping.test_express_appconfig"],
    }, "Proton-Proton collision data from 2012", test_file_db)

testfiles(
    "2015_DaVinciTests.trackrefitting.read_2015_mdst", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision15/LEPTONIC.MDST/00048279/0000/00048279_00001457_1.leptonic.mdst"
    ], {
        "Author": "Eduardo Rodrigues",
        "Format": "MDST",
        "DataType": "2015",
        "Date": "2018.06.01",
        "Application": "DaVinci",
        "Simulation": False,
        "QMTests": ["DaVinciTests.trackrefitting.read_2015_mdst"],
    }, "Stripping output from 2015 pp collision data", test_file_db)

testfiles(
    myname='TeslaTest_2018raw_0x11751801_0x21761801_TURBO',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/2018raw_0x11751801_0x21761801/210224_0000000252.raw'
    ],
    qualifiers={
        'Author': 'rmatev',
        'DataType': '2018',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20180202',
        'DDDB': 'dddb-20171030-3',
        'InitialTime': '2018-06-12 04:47:00 UTC',
        'Date': '2018-06-18',
        'Application': 'Moore'
    },
    comment='A 2018 turbo raw file for tesla testing',
    test_file_db=test_file_db)

testfiles(
    myname='TeslaTest_2018raw_0x11751801_0x21761801_TURCAL',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/HLT/Tesla/TestFiles/2018raw_0x11751801_0x21761801/210224_0000000247.raw'
    ],
    qualifiers={
        'Author': 'rmatev',
        'DataType': '2018',
        'Format': 'MDF',
        'Simulation': False,
        'CondDB': 'cond-20180202',
        'DDDB': 'dddb-20171030-3',
        'InitialTime': '2018-06-12 04:47:00 UTC',
        'Date': '2018-06-18',
        'Application': 'Moore'
    },
    comment='A 2018 turbo raw file for tesla testing',
    test_file_db=test_file_db)

testfiles(
    "MiniBrunel_2018_MinBias_FTv4_DIGI", [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_1.digi.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_2.digi.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_3.digi.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_4.digi.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_5.digi.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Simulation/MinBiasRawBankv5/00067189_6.digi.digi"
    ], {
        "Author": "Sebastien Ponce",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2018.08.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20180815",
    },
    "Upgrade minimum biais data from simulation. 100k events using FT v4/5 rawbank format",
    test_file_db)

testfiles(
    "MiniBrunel_2018_MinBias_FTv4_MDF", [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MiniBrunel/00067189.mdf"
    ] * 10, {
        "Author": "Sebastien Ponce",
        "Format": "MDF",
        "DataType": "Upgrade",
        "Date": "2018.08.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20180815",
    },
    "Upgrade minimum biais data from simulation. 100k events using FT v4/5 rawbank format",
    test_file_db)

testfiles(
    "2017_raw_full_lowmu",
    [
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2017/RAW/FULL/LHCb/CALIBRATION17/202694/202694_0000000810.raw"
    ],
    {
        "Author": "Rosen Matev",
        "Format": "MDF",
        "DataType": "2017",
        "Date": "2018.08.31",
        "Application": "",  # passthrough data
        "Simulation": False,
        "QMTests": ["brunel.2017-passthrough"],
    },
    "Low-mu passthrough Proton-Proton collision raw data FULL stream, run 202694 from 21th November 2017.",
    test_file_db)

### auto created ###
testfiles(
    myname='upgrade-big-events',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00067189/0000/00067189_00000098_1.xdigi'
    ],
    qualifiers={
        'Author': 'sstahl',
        'DataType': 'XDIGI',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20170301-vc-md100',
        'DDDB': 'dddb-20171010',
        'Date': '2018-09-08 10:05:37.051919',
        'QMTests': 'Brunel.brunel-upgrade-bigevents'
    },
    comment=
    'This file is intended to gather big events to test the upgrade reconstruction',
    test_file_db=test_file_db)

testfiles(
    "Upgrade_BdKstgamma_busy_event_XDIGI", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000026_1.xdigi"
    ], {
        "Author": "Andre Guenther",
        "Format": "XDIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.14",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20190223",
        "QMTests": 'brunel-upgrade-bigevents2.qmt'
    },
    "This file contains events with particularly many hits in the SciFi stations, same idea as 'upgrade-big-events'.",
    test_file_db)

testfiles(
    myname='2016-lb2l0gamma.strip.dst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2016/LB2L0GAMMA.STRIP.DST/00057531/0000/00057531_00000001_1.lb2l0gamma.strip.dst'
    ],
    qualifiers={
        'Author': 'cattanem',
        'DataType': '2016',
        'Format': 'DST',
        'Simulation': True,
        'CondDB': 'sim-20161124-2-vc-md100',
        'DDDB': 'dddb-20150724',
        'Date': '2018-09-13',
        'QMTests': 'L0Calo.testL0CaloFix2016'
    },
    comment='MC/2016 lb2l0gamma stripped DST',
    test_file_db=test_file_db)

testfiles(
    "upgrade_2018_BsPhiPhi_FTv2", [
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00067195/0000/00067195_00000032_1.xdigi"
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "XDIGI",
        "DataType": "Upgrade",
        "Date": "2018.10.25",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20170301-vc-md100",
        "DDDB": "dddb-20171010",
    }, "Upgrade BsPhiPhi simulation, FT v2 rawbank format, 1k events",
    test_file_db)

testfiles(
    "upgrade_2018_BdKstee_LDST", [
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000019_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000045_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000066_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000075_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000078_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000084_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000090_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000093_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000101_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070697/0000/00070697_00000106_2.ldst"
    ], {
        "Author": "Carla Marin Benito",
        "Format": "LDST",
        "DataType": "Upgrade",
        "Date": "2019.04.02",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20171127-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20171126",
    }, "Upgrade BdKstee simulation, LDST format, 10k events", test_file_db)

testfiles(
    myname='UpgradeHLT1FilteredMinbias',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP2/Hlt2Throughput/minbias_filtered_{0}.mdf'
        .format(i) for i in [
            1, 12, 13, 14, 17, 2, 20, 21, 22, 23, 24, 25, 27, 29, 3, 32, 36,
            37, 39, 4, 40, 42, 43, 46, 47, 49, 52, 53, 54, 55, 58, 60, 61, 62,
            66, 68, 70, 73, 74, 76, 78, 79, 80, 82, 83, 86, 87, 9, 91, 94, 96,
            97, 98
        ]
    ],
    qualifiers={
        'Author': 'Marian Stahl',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20171010',
        'CondDB': 'sim-20170301-vc-md100',
        'Date': '2019-05-28'
    },
    comment=
    'Min bias mdfs for testing upgrade Hlt2 reconstruction. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/3000000 . There was an additional filtering on (|Two)TrackTight | DiMuon(Low|HighMass)Tight.',
    test_file_db=test_file_db)

testfiles(
    myname='UpgradeHLT1FilteredWithGEC',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP2/Hlt2Throughput/minbias_filtered_gec11000_{0}.mdf'
        .format(i) for i in [1, 2, 3, 4, 5]
    ],
    qualifiers={
        'Author': 'Sascha Stahl',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20171010',
        'CondDB': 'sim-20170301-vc-md100',
        'Date': '2019-05-28'
    },
    comment=
    'Min bias mdfs for testing upgrade Hlt2 reconstruction. From /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/3000000 . There was an additional filtering on (|Two)TrackTight | DiMuon(Low|HighMass)Tight. A GEC of 11000 on the sum of FT and UT clusters is applied.',
    test_file_db=test_file_db)

testfiles(
    "Upgrade_MinBias_LDST", [
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00001005_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00000988_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00000940_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00001010_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00000789_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00001379_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00001442_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00001435_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00000531_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00071353/0000/00071353_00000538_2.ldst"
    ], {
        "Author": "Alex Pearce",
        "Format": "LDST",
        "DataType": "Upgrade",
        "Date": "2019.09.20",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20171127-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20171126",
    }, "Upgrade MinBias simulation, LDST format, 10k events", test_file_db)

testfiles(
    "Upgrade_BsPhiPhi_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MD_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.01.23",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20180815",
    },
    "Upgrade BsPhiPhi data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_BsPhiPhi_MU_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_BsPhiPhi_MU_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.01.23",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
        "ConditionsVersion": "master",
    },
    "Upgrade BsPhiPhi data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_Ds2KKPi_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p0-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p1-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p2-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p3-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p4-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p5-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p6-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p7-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p8-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MD_FTv4_DIGI/23263020-FTv64-md-p9-Extended.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20180815",
    },
    "Upgrade Ds2KKPi data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_Ds2KKPi_MU_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p0-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p1-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p2-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p3-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p4-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p5-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p6-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p7-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p8-Extended.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Ds2KKPi_MU_FTv4_DIGI/23263020-FTv64-mu-p9-Extended.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
        "ConditionsVersion": "master",
    },
    "Upgrade Ds2KKPi data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_JPsiMuMu_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-10.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-11.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-12.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-13.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-14.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-15.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-16.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-17.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-18.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-19.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-20.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-21.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-22.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-23.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-24.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-25.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-26.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-27.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-28.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-29.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-30.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-31.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-32.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-33.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-35.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-36.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-37.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-38.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-39.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-40.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-41.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-42.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-43.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-44.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-45.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-46.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-47.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-48.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-49.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-50.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-51.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-52.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-53.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-54.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-55.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-56.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-57.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-58.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-59.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-60.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-61.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-62.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-63.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-64.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-65.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-66.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-67.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-68.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-69.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-70.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-71.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-72.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-73.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-74.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-75.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-76.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-77.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-78.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-79.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-80.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-81.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-82.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-83.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-84.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-85.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-86.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-87.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-88.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-89.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-90.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-91.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-92.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-93.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-94.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-95.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-96.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-97.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-98.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-99.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_JPsiMuMu_MD_FTv4_DIGI/Boole-6x2-WithSpillover-24142000-50000-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20180815",
    },
    "Upgrade JPsiMuMu data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_KstEE_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MD_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20180815",
    },
    "Upgrade KstEE data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_KstEE_MU_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstEE_MU_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
        "ConditionsVersion": "master",
    },
    "Upgrade KstEE data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_KstMuMu_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MD_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20180815",
    },
    "Upgrade KstMuMu data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_KstMuMu_MU_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_KstMuMu_MU_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
        "ConditionsVersion": "master",
    },
    "Upgrade KstMuMu data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_ZMuMu_MD_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MD_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20180815",
    },
    "Upgrade ZMuMu data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

testfiles(
    "Upgrade_ZMuMu_MU_FTv4_DIGI", [
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-0.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-5.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-6.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-7.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-8.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_ZMuMu_MU_FTv4_DIGI/Boole-Extended-9.digi'
    ], {
        "Author": "Dorothea vom Bruch",
        "Format": "DIGI",
        "DataType": "Upgrade",
        "Date": "2020.02.15",
        "Application": "MiniBrunel",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
        "ConditionsVersion": "master",
    },
    "Upgrade ZMuMu data from simulation. 10k events using FT v4/5 rawbank format, private production",
    test_file_db)

### auto created ###
testfiles(
    myname='Upgrade_Bd_pi+pi-pi0_LDST',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000153_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000078_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000048_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000026_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000171_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000084_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000019_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000012_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000219_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000102_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000077_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000096_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000133_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000197_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000072_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000005_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000151_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000158_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000027_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000148_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000212_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000069_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000145_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000042_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000017_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000099_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000170_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000098_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000215_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000192_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000136_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000041_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000103_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000144_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000002_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000167_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000021_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000120_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000176_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000180_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000007_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000061_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000022_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000201_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000082_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000143_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000075_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000168_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000051_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000193_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000175_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000113_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000163_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000214_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000196_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000149_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000008_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000125_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000052_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000074_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000128_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000183_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000152_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000169_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000010_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000165_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000203_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000119_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000020_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000177_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000009_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000080_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000160_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000079_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000091_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000058_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000184_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000071_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000166_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000094_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000023_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000053_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000181_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000086_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000206_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000116_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000015_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000129_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000001_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000130_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000115_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000062_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000059_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000146_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000055_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000198_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000135_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000047_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000117_2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00073042/0000/00073042_00000093_2.ldst',
    ],
    qualifiers={
        'Author': 'lzenaiev',
        'DataType': 'Upgrade',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20171126',
        'Date': '2020-03-04 15:56:08.815188',
        'QMTests': 'RecoConf.hlt2_reco_calo_resolution_pi0'
    },
    comment=
    'MC Upgrade Bd_pi+pi-pi0_LDST sample (about 100K events), event type 11102404, needed for calo resolution studies in Moore tests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='Upgrade_BdKstgamma_XDIGI',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000004_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000015_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000027_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000009_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000049_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000045_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000042_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000022_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000001_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000047_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000006_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000024_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000008_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000023_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000032_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000041_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000043_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000002_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000007_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/XDIGI/00099828/0000/00099828_00000012_1.xdigi',
    ],
    qualifiers={
        'Author': 'lzenaiev',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20190223',
        'Date': '2020-03-04 16:57:09.341749',
        'QMTests': 'RecoConf.hlt2_reco_calo_resolution_gamma'
    },
    comment=
    'MC Upgrade BdKstgamma_XDIGI sample (about 40K events), event type 11102201, needed for calo resolution studies in Moore tests',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2011',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2011.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2011',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20160614-1-vc-md100',
        'DDDB': 'dddb-20170721-1',
        'Date': '2020-04-17 19:24:42.879832',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2012',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2012.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2012',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20160321-2-vc-md100',
        'DDDB': 'dddb-20170721-2',
        'Date': '2020-04-17 19:26:35.055644',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2015',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2015.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2015',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20161124-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 19:27:47.632598',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2016',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2016.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2016',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20170721-2-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 19:29:35.877782',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2017',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2017.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2017',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20190430-1-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 19:47:11.052450',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Sim-2018',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Gauss-11874424-100ev-LHCBGAUSS-1985-2018.sim'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2018',
        'Format': 'SIM',
        'Simulation': True,
        'CondDB': 'sim-20190430-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 19:48:15.875389',
        'Application': 'Gauss'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Digi-2012',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Boole-LHCBGAUSS-1985-2012.digi'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2012',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20160321-2-vc-md100',
        'DDDB': 'dddb-20170721-2',
        'Date': '2020-04-17 19:55:52.573428',
        'Application': 'Boole'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Digi-2015',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Boole-LHCBGAUSS-1985-2015.digi'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2015',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20161124-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 19:57:05.767479',
        'Application': 'Boole'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Digi-2016',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Boole-LHCBGAUSS-1985-2016.digi'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2016',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20170721-2-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 20:02:04.821129',
        'Application': 'Boole'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Digi-2017',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Boole-LHCBGAUSS-1985-2017.digi'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2017',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20190430-1-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 20:03:08.176329',
        'Application': 'Boole'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='LHCBGAUSS-1985-Digi-2018',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/SimTests/PGunTests/Boole-LHCBGAUSS-1985-2018.digi'
    ],
    qualifiers={
        'Author': 'adavis',
        'DataType': '2018',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20190430-vc-md100',
        'DDDB': 'dddb-20170721-3',
        'Date': '2020-04-17 20:04:06.217418',
        'Application': 'Boole'
    },
    comment='Test Propagation of PV from PGPrimaryVertex from Gauss',
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='genFSR_2012_mergesmallfiles_digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/genfsrVM_test1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/genfsrVM_test2.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/genfsrVM_test3.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/genfsrVM_test4.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/genfsrVM_test5.digi',
    ],
    qualifiers={
        'Author': 'dfazzini',
        'DataType': '2012',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20160321-2-vc-mu100',
        'DDDB': 'dddb-20170721-2',
        'Date': '2020-04-20 20:20:15.45723',
        'QMTests': 'mergesmallfiles-genFSR.qmt'
    },
    comment=
    'Files .digi for testing the virtual memory and the memory usage during the genFSR merging',
    test_file_db=test_file_db)

testfiles(
    "Upgrade_Bs2phiphi_MU_FTv4_SIM", [
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/0/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/1/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/2/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/3/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/4/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/5/Gauss-13104012-100ev-20190506.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/6/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/7/Gauss-13104012-100ev-20190505.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/8/Gauss-13104012-100ev-20190507.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/user/g/gligorov/UpgradeStudies/FTv4Sim/147/9/Gauss-13104012-100ev-20190505.sim'
    ], {
        "Author": "Vladimir Gligorov",
        "Format": "SIM",
        "DataType": "Upgrade",
        "Date": "2019.05.05",
        "Application": "Boole",
        "Simulation": True,
        "CondDB": "sim-20180530-vc-mu100",
        "DDDB": "dddb-20180815",
        "ConditionsVersion": "master",
    },
    "Upgrade Bs2PhiPhi data from simulation. #10k events with FT version 6.4 , private production",
    test_file_db)

testfiles(
    myname="upgrade_minbias_hlt1_filtered",
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00004602_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00004625_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00005065_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00005143_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00005582_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00005592_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006124_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006137_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006187_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006191_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006211_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006220_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00006746_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007043_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007187_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007259_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007332_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007348_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007357_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007389_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007409_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007441_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007457_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007519_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007578_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007641_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007658_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007932_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00007952_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00008351_1.ldst",
    ],
    qualifiers={
        "Author": "Alex Pearce",
        "Format": "LDST",
        "DataType": "Upgrade",
        "Date": "2020-04-23 12:12:12.12000",
        "Simulation": True,
        "CondDB": "sim-20171127-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20171126",
    },
    comment=
    "Upgrade minimum bias sample filtered by Moore v30r0 HLT1; DIRAC transformation ID 75219.",
    test_file_db=test_file_db)

testfiles(
    myname='CHARMSPECPARKED_for_memory_test',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Collision16/CHARMSPECPARKED.MDST/00076439/0000/00076439_00000685_1.charmspecparked.mdst'
    ],
    qualifiers={
        'Author': 'Marian Stahl',
        'Format': 'ROOT',
        'DataType': '2016',
        'Simulation': False,
        'Turbo': True,
        'RootInTES': '/Event/Charmspec/Turbo',
        'Date': '2020-04-29'
    },
    comment=
    'Randomly chosen CHARMSPECPARKED mDST which causes large memory consumption in an empty event loop from DaVinci v45r1 (Gaudi v32r1) onwards. Issue described in https://gitlab.cern.ch/lhcb-dpa/project/issues/2',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-202004-PbPb-EPOS-b-6_14fm',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/'
        'upgrade-202004-PbPb-EPOS-b-6_14fm/{0}_Gausstest_MB.xdigi'.format(i)
        for i in [
            361080073, 361080076, 361080079, 361080084, 361080088, 361080109,
            361080120, 361080132, 361080148, 361080176, 361080192, 361080204,
            361080218, 361080231, 361080239, 361080251, 361080262, 361080274,
            361080288, 361080289, 361080290, 361080291, 361080292, 361080293,
            361080294, 361080297, 361080298, 361080300, 361080301, 361080302,
            361080303, 361080304, 361080305, 361080306, 361080307, 361080308,
            361080310, 361080311, 361080312, 361080313, 361080314, 361080316,
            361080317, 361080318, 361080319, 361080320, 361080321, 361080322,
            361080323, 361080324, 361080325, 361080326, 361080327, 361080328,
            361080329, 361080330, 361080331, 361080332, 361080334, 361080335,
            361080336, 361080337, 361080338, 361080339, 361080340, 361080341,
            361080343, 361080344, 361080345, 361080346, 361080347, 361080348,
            361080349, 361080350, 361080351, 361080352, 361080353, 361080354,
            361080355, 361080357, 361080358, 361080359, 361080360, 361080361,
            361080362, 361080363, 361080364, 361080365, 361080366, 361080368,
            361080369, 361080370, 361080371, 361080372
        ]
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20190223',
        'Date': '2020-04-20 00:00:00'
    },
    comment=(
        'Private sample using EPOS: merged J/psi-> mu mu (5k) and B->J/psi '
        '(5k) embedded in MB EPOS events with the impact parameter b ranging '
        'between 6-14 fm (15%-80% centrality).'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-202004-PbPb-EPOS-b-6_14fm-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/'
        'upgrade-202004-PbPb-EPOS-b-6_14fm/all_Gausstest_MB.mdf',
    ],
    qualifiers={
        'Author': 'Carla Marin',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20180530-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20190223',
        'Date': '2021-03-03 08:30:00'
    },
    comment=
    ('Private MDF sample created from upgrade-202004-PbPb-EPOS-b-6_14fm for throughput tests.'
     'Around 10k events available.'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-202305-PbPb-EPOS-b-8_22-VELO-10mm-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/2023_PbPb_MB/'
        'model{0}/Gausstest_MB.xdigi'.format(i) for i in range(1, 100)
    ],
    qualifiers={
        'Author': 'Chenxi Gu',
        'DataType': 'Upgrade',
        'Format': 'ROOT',
        'Simulation': True,
        'CondDB': 'sim-20230322-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20230313',
        'Date': '2023-06-01 00:00:00'
    },
    comment=('Private sample using EPOS MB with 10 mm VELO apperture'
             'with the impact parameter b ranging '
             'between 8-22 fm (30%-100% centrality).'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-202305-PbPb-EPOS-b-8_22-VELO-10mm-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/2023_PbPb_MB/dump/mdf_sim-20230322-vc-md100/dumped_file.mdf'
    ],
    qualifiers={
        'Author': 'Chenxi Gu',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20230322-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20230313',
        'Date': '2023-06-01 00:00:00'
    },
    comment=('Private sample using EPOS MB with 10 mm VELO apperture'
             'with the impact parameter b ranging '
             'between 8-22 fm (30%-100% centrality).'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-official-PbPb-EPOS-b-8_22-VELO-23.5mm-digi',
    filenames=[
        "root://x509up_u74587@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000012_1.xdigi",
        "root://x509up_u74587@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000016_1.xdigi",
        "root://x509up_u74587@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000020_1.xdigi",
        "root://x509up_u74587@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000027_1.xdigi",
        "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000001_1.xdigi",
        "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000002_1.xdigi",
        "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000010_1.xdigi",
        "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000022_1.xdigi",
        "root://x509up_u74587@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000009_1.xdigi",
        "root://x509up_u74587@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000024_1.xdigi",
        "root://x509up_u74587@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000025_1.xdigi",
        "root://x509up_u74587@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000003_1.xdigi",
        "root://x509up_u74587@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000007_1.xdigi",
        "root://x509up_u74587@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000014_1.xdigi",
        "root://x509up_u74587@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000028_1.xdigi",
        "root://x509up_u74587@xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000019_1.xdigi",
        "root://x509up_u74587@xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000030_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000004_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000005_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000006_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000008_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000011_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000015_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000017_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000018_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000021_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000023_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000029_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000032_1.xdigi",
        "root://x509up_u74587@xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000013_1.xdigi",
        "root://x509up_u74587@xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000026_1.xdigi",
        "root://x509up_u74587@xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197804/0000/00197804_00000031_1.xdigi",
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'ROOT',
        'Simulation': True,
        'CondDB': 'sim-20230626-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20230313',
        'Date': '2023-06-01 00:00:00'
    },
    comment=('Official sample using EPOS MB with 23.5 mm VELO apperture'
             'with the impact parameter b ranging '
             'between 8-22 fm (30%-100% centrality).'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-official-PbPb-EPOS-b-8_22-VELO-23.5mm-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/upgrade-official-PbPb-EPOS-b-8_22-VELO-23.5mm-mdf/dump/mdf_sim-20230626-vc-md100/dumped_file.mdf'
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20230626-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20230313',
        'Date': '2023-06-01 00:00:00'
    },
    comment=('Official sample using EPOS MB with 23.5 mm VELO apperture'
             'with the impact parameter b ranging '
             'between 8-22 fm (30%-100% centrality).'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-official-PbPb-EPOS-b-8_22-VELO-23.5mm-d0-digi',
    filenames=[
        "root://x509up_u74587@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00197808/0000/00197808_00000003_1.xdigi",
        "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/Dev/XDIGI/00197808/0000/00197808_00000001_1.xdigi",
        "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/Dev/XDIGI/00197808/0000/00197808_00000002_1.xdigi",
        "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/Dev/XDIGI/00197808/0000/00197808_00000004_1.xdigi",
        "root://x509up_u74587@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197808/0000/00197808_00000005_1.xdigi",
        "root://x509up_u74587@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197808/0000/00197808_00000006_1.xdigi",
        "root://x509up_u74587@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197808/0000/00197808_00000009_1.xdigi",
        "root://x509up_u74587@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197808/0000/00197808_00000007_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197808/0000/00197808_00000008_1.xdigi"
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'ROOT',
        'Simulation': True,
        'CondDB': 'sim-20230626-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20230313',
        'Date': '2023-06-01 00:00:00'
    },
    comment=('Official sample using EPOS MB with 23.5 mm VELO apperture'
             'with the impact parameter b ranging '
             'between 8-22 fm (30%-100% centrality).'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-official-PbPb-EPOS-b-8_22-VELO-23.5mm-d0-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/upgrade-official-PbPb-EPOS-b-8_22-VELO-23.5mm-d0-mdf/dump/mdf_sim-20230626-vc-md100/dumped_file.mdf'
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20230626-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20230313',
        'Date': '2023-06-01 00:00:00'
    },
    comment=('Official sample using EPOS MB with 23.5 mm VELO apperture'
             'with the impact parameter b ranging '
             'between 8-22 fm (30%-100% centrality).'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-official-PbPb-EPOS-b-8_22-VELO-23.5mm-jpsi-digi',
    filenames=[
        "root://x509up_u74587@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00197810/0000/00197810_00000004_1.xdigi",
        "root://x509up_u74587@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/XDIGI/00197810/0000/00197810_00000007_1.xdigi",
        "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/Dev/XDIGI/00197810/0000/00197810_00000001_1.xdigi",
        "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/Dev/XDIGI/00197810/0000/00197810_00000002_1.xdigi",
        "root://xrootd-lhcb.cr.cnaf.infn.it:1094//storage/gpfs_lhcb/disk/lhcb/MC/Dev/XDIGI/00197810/0000/00197810_00000003_1.xdigi",
        "root://x509up_u74587@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197810/0000/00197810_00000008_1.xdigi",
        "root://x509up_u74587@xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Dev/XDIGI/00197810/0000/00197810_00000006_1.xdigi",
        "root://xrootd.echo.stfc.ac.uk//lhcb:prod/lhcb/MC/Dev/XDIGI/00197810/0000/00197810_00000005_1.xdigi",
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'ROOT',
        'Simulation': True,
        'CondDB': 'sim-20230626-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20230313',
        'Date': '2023-06-01 00:00:00'
    },
    comment=('Official sample using EPOS MB with 23.5 mm VELO apperture'
             'with the impact parameter b ranging '
             'between 8-22 fm (30%-100% centrality).'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-official-PbPb-EPOS-b-8_22-VELO-23.5mm-jpsi-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/upgrade-official-PbPb-EPOS-b-8_22-VELO-23.5mm-jpsi-mdf/dump/mdf_sim-20230626-vc-md100/dumped_file.mdf'
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20230626-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20230313',
        'Date': '2023-06-01 00:00:00'
    },
    comment=('Official sample using EPOS MB with 23.5 mm VELO apperture'
             'with the impact parameter b ranging '
             'between 8-22 fm (30%-100% centrality).'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-202305-PbAr-FT-EPOS-b-8_22-VELO-10mm-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/2023_PbAr_MB/'
        'job0{0}/Boole-30000000-1000ev-20230601-Extended.digi'.format(i)
        for i in range(1, 32)
    ],
    qualifiers={
        'Author': 'Chenxi Gu',
        'DataType': 'Upgrade',
        'Format': 'ROOT',
        'Simulation': True,
        'CondDB': 'sim-20230322-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20230313',
        'Date': '2023-06-01 00:00:00'
    },
    comment=('Private sample using EPOS MB with 10 mm VELO apperture'
             'with SMOG2 configuration.'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-202305-PbAr-FT-EPOS-b-8_22-VELO-10mm-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/2023_PbAr_MB/dump/mdf_sim-20230322-vc-md100/dumped_file.mdf'
    ],
    qualifiers={
        'Author': 'Chenxi Gu',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20230322-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20230313',
        'Date': '2023-06-01 00:00:00'
    },
    comment=('Private sample using EPOS MB with 10 mm VELO apperture'
             'with SMOG2 configuration.'),
    test_file_db=test_file_db)

testfiles(
    myname='expected-2024-PbPb-minbias-xdigi',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbPb_minbias_DIGI/00207020_00000019_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbPb_minbias_DIGI/00207020_00000106_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbPb_minbias_DIGI/00207020_00000151_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbPb_minbias_DIGI/00207020_00000088_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbPb_minbias_DIGI/00207020_00000114_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbPb_minbias_DIGI/00207020_00000273_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbPb_minbias_DIGI/00207020_00000101_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbPb_minbias_DIGI/00207020_00000119_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbPb_minbias_DIGI/00207020_00000366_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbPb_minbias_DIGI/00207020_00000103_1.xdigi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbPb_minbias_DIGI/00207020_00000149_1.xdigi",
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Simulation': True,
        'CondDB': 'sim-20231017-vc-md100',
        'DDDB': 'dddb-20231017',
        'Date': '2024-07-12 00:00:00'
    },
    comment=('Official PbPb sample with expected 2024 conditions.'),
    test_file_db=test_file_db)

testfiles(
    myname='expected-2024-PbAr-minbias-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbAr_minbias_DIGI/00220508_00000009_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbAr_minbias_DIGI/00220508_00000031_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbAr_minbias_DIGI/00220508_00000122_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbAr_minbias_DIGI/00220508_00000175_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbAr_minbias_DIGI/00220508_00000248_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbAr_minbias_DIGI/00220508_00000024_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbAr_minbias_DIGI/00220508_00000116_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbAr_minbias_DIGI/00220508_00000131_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Expected_2024_PbAr_minbias_DIGI/00220508_00000179_1.digi'
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'sim-20231017-vc-md100',
        'DDDB': 'dddb-20231017',
        'Date': '2024-07-12 00:00:00'
    },
    comment=('Official PbAr sample with expected 2024 conditions.'),
    test_file_db=test_file_db)

testfiles(
    myname='official-expected-2024-PbPb-minbias-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/official-expected-2024-PbPb-minbias-mdf/mdf_sim-20231017-vc-md100/dumped_file.mdf'
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'CondDB': 'sim-20231017-vc-md100',
        'DDDB': 'dddb-20231017',
        'Date': '2024-07-23 00:00:00'
    },
    comment=('Official peripheral PbPb sample with expected 2024 conditions.'),
    test_file_db=test_file_db)

testfiles(
    myname='official-expected-2024-PbAr-minbias-mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/official-expected-2024-PbAr-minbias-mdf/mdf_sim-20231017-vc-md100/dumped_file.mdf'
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'CondDB': 'sim-20231017-vc-md100',
        'DDDB': 'dddb-20231017',
        'Date': '2024-07-23 00:00:00'
    },
    comment=(
        'Official PbAr (full centrality) sample with expected 2024 conditions.'
    ),
    test_file_db=test_file_db)

### auto created ###
testfiles(
    myname='genFSR_upgrade_ldst',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR/genfsr_upgrade0.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR/genfsr_upgrade1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR/genfsr_upgrade2.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR/genfsr_upgrade3.ldst',
    ],
    qualifiers={
        'Author': 'dfazzini',
        'DataType': 'Upgrade',
        'Format': 'LDST',
        'Simulation': True,
        'CondDB': 'sim-20171127-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20171126',
        'Date': '2020-06-17 18:34:43.57756',
        'QMTests': 'test-genfsr.qmt'
    },
    comment=
    'Files .ldst with Upgrade condition for testing the propagation and merging of genFSR in DaVinci',
    test_file_db=test_file_db)

testfiles(
    "Sim09j-2015-Digi14c-incl_b",
    filenames=[
        ("root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2015/DIGI/"
         "00115195/0000/00115195_0000000{0}_1.digi").format(i)
        for i in range(1, 10)
    ],
    qualifiers={
        "Author": "Rosen Matev",
        "Format": "DIGI",
        "DataType": "2015",
        "Date": "2020.09.25",
        "Application": "Boole",
        "Simulation": True,
        "DDDB": "dddb-20170721-3",
        "CondDB": "sim-20161124-vc-md100",
    },
    comment=("Inclusive B sample for testing 2015 legacy applications. From "
             "/MC/2015/Beam6500GeV-2015-MagDown-Nu1.6-25ns-Pythia8"
             "/Sim09j/Digi14c/10000000/DIGI"),
    test_file_db=test_file_db)

### auto created ###
testfiles(
    'upgrade-TELL40-UT-miniBias',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-ut/Boole-Extended.digi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'Author': 'xuyuan',
        'DataType': 'Upgrade',
        'Processing': 'Boole',
        'Simulation': True,
        'CondDB': 'upgrade/UTv4r2',
        'DDDB': 'upgrade/dddb-20201105',
        'Date': '2020-12-04 08:01:44.324813',
        'Application': 'Boole',
        'QMTests': 'brunel-upgrade-UTTELL40-decoder.qmt'
    },
    comment='miniBias sample with new UTTELL40 data format',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magup-sim10-up08-digi15-up04-30000000-velo_open-digi',
    comment=
    ('Files XDIGI from Upgrade08 simulations of MinBias, May 2021. From /MC/Upgrade/Beam7000GeV-Upgrade-VPOpen-MagUp-Nu7.6-25ns-Pythia8/Sim10-Up08/Digi15-Up04'
     ),
    filenames=[
        'root://x509up_u24378@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000001_1.xdigi',
        'root://x509up_u24378@ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000002_1.xdigi',
        'root://x509up_u24378@xrootd.pic.es//pnfs/pic.es/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000003_1.xdigi',
        'root://x509up_u24378@xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000004_1.xdigi',
        'root://x509up_u24378@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000005_1.xdigi',
        'root://x509up_u24378@xrootd.grid.surfsara.nl//pnfs/grid.sara.nl/data/lhcb/LHCb-Disk/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000006_1.xdigi',
        'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000007_1.xdigi',
        'root://x509up_u24378@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00135243/0000/00135243_00000008_1.xdigi'
    ],
    qualifiers={
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2022-03-03',
        'Processing': 'Sim-Up08-Digi15-Up04',
        'DDDB': 'velo-open',
        'CondDB': 'sim-20201218-vc-mu100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'Simulation': True,
    },
    test_file_db=test_file_db)

testfiles(
    myname='Moore-Output-DST',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/DaVinci-Run3/hlt2_test_persistreco_fromfile.dst'
    ],
    qualifiers={
        'Author': 'Sevda Esen',
        'Format': 'DST',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20171126',
        'CondDB': 'sim-20171127-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'Date': '2021-03-26'
    },
    comment=
    'Uploaded by Patrick Koppenburg. Output of Moore test hlt2_test_persistreco_fromfile.',
    test_file_db=test_file_db)

testfiles(
    myname='SMOG2_pHe',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_11-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_19-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_5-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_18-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_0-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_23-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_4-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_12-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_27-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_33-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_34-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_38-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_42-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_32-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_48-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_47-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_50-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_64-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_58-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_68-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_73-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_65-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_75-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_74-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_77-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_57-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_72-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_69-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_80-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_83-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_89-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_100-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_93-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_94-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_95-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_98-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_106-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_108-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_114-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_110-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_112-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_118-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_120-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_121-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_122-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_123-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_124-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_127-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_135-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_141-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_131-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_139-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_137-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_143-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_147-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_149-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_151-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_152-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_161-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_157-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_162-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_167-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_169-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_168-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_173-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_174-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_177-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_180-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_193-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_194-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_190-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_202-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_197-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_201-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_203-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_200-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_199-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_29-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_215-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_225-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_223-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_226-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_232-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_230-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_231-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_237-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_238-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_244-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_246-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_249-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_243-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_248-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_255-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_252-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_258-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_259-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_260-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_261-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_264-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_271-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_279-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_278-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_283-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_286-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_287-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_288-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_291-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_290-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_293-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_292-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_295-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_302-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_303-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_301-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_305-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_304-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_307-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_313-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_312-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_315-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_310-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_319-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_318-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_321-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_322-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_323-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_324-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_325-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_327-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_326-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_331-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_329-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_330-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_344-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_349-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_350-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_352-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_353-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_356-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_355-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_359-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_251-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_363-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_367-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_370-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_374-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_379-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_384-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_382-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_383-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_387-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_390-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_391-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_393-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_394-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_395-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_400-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_408-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_402-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_407-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_396-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_418-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_413-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_417-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_420-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_357-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_427-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_428-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_426-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_422-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_432-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_431-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_430-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_433-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_435-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_444-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_448-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_451-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_455-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_456-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_459-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_460-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_462-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_463-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_464-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_466-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_467-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_470-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_476-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_474-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_480-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_482-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_481-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_487-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_486-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_495-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_496-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_500-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_503-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_505-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_511-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_515-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_516-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_522-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_523-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_526-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_530-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_534-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_536-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_392-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_537-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_539-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_540-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_543-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_545-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_547-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_554-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_560-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_558-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_561-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_562-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_571-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_570-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_569-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_532-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_580-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_581-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_583-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_584-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_585-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_594-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_592-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_596-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_597-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_600-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_602-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_606-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_608-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_612-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_619-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_623-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_624-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_625-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_628-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_635-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_627-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_633-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_636-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_639-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_632-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2MB/digi/SMOG2MB_490-Extended.digi",
    ],
    qualifiers={
        'Author': 'Giacomo Graziani, Saverio Mariani',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-07-27',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        'Simulation': True,
        "ConditionsVersion": "master",
    },
    comment=
    'Private simulation with one fixed per-event pHe collision within the SMOG2 cell (z in [-500, -300] mm). SciFi v6 is required. Required for PR jobs.',
    test_file_db=test_file_db)

testfiles(
    myname='SMOG2_pp_pHe',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_0-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_9-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_38-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_37-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_15-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_45-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_44-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_56-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_26-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_24-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_14-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_12-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_53-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_8-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_28-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_29-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_42-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_43-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_17-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_2-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_46-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_13-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_50-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_16-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_27-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_49-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_57-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_7-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_51-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_11-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_40-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_62-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_25-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_54-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_39-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_3-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_52-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_47-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_34-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_4-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_18-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_21-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_31-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_36-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_55-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_35-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_5-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_64-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_68-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_69-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_70-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_78-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_84-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_76-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_86-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_105-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_73-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_96-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_89-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_91-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_94-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_81-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_93-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_100-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_71-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_75-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_85-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_77-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_95-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_90-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_103-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_87-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_80-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_120-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_92-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_88-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_119-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_83-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_114-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_108-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_104-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_122-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_128-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_109-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_129-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_98-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_124-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_112-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_132-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_131-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_106-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_99-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_110-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_130-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_115-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_123-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_118-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_127-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_133-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_134-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_137-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_147-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_135-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_139-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_141-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_148-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_138-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_160-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_144-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_149-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_146-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_157-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_185-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_183-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_165-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_167-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_176-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_169-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_150-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_175-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_159-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_164-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_151-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_170-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_181-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_174-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_171-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_143-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_192-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_166-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_195-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_186-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_182-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_173-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_140-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_162-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_163-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_177-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_172-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_179-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_197-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_178-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_198-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_189-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_193-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_191-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_190-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_199-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_200-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_204-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_202-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_201-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_213-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_203-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_212-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_219-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_230-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_225-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_205-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_222-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_211-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_228-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_220-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_218-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_241-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_207-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_235-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_217-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_237-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_244-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_224-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_238-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_239-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_231-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_232-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_233-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_259-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_250-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_246-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_251-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_216-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_256-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_258-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_226-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_261-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_234-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_229-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_240-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_260-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_252-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_245-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_264-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_268-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_255-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_248-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_257-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_266-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_249-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_269-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_272-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_270-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_271-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_273-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_280-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_274-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_279-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_287-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_286-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_275-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_298-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_282-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_305-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_307-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_291-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_296-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_276-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_288-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_277-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_312-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_290-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_295-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_294-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_299-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_302-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_321-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_315-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_289-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_281-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_308-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_306-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_319-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_293-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_320-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_309-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_323-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_311-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_328-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_324-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_331-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_322-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_325-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_318-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_338-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_335-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_330-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_340-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_339-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_337-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_333-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_344-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_343-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_345-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_347-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_346-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_348-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_350-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_353-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_351-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_342-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_355-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_358-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_362-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_354-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_366-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_352-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_359-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_367-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_373-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_360-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_357-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_364-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_368-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_370-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_374-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_356-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_365-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_369-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_372-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_380-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_387-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_375-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_388-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_371-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_363-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_384-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_385-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_379-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_392-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_399-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_396-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_393-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_397-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_402-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_395-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_390-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_391-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_394-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_401-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_400-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_404-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_403-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_408-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_413-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_411-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_414-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_429-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_407-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_418-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_436-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_441-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_427-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_410-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_423-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_424-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_438-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_431-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_415-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_426-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_416-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_432-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_428-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_433-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_440-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_435-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_419-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_420-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_430-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_445-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_443-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_421-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_452-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_439-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_449-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_447-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_453-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_434-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_451-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_450-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_448-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_442-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_457-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_446-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_461-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_459-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_455-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_454-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_460-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_458-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_456-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_463-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_462-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_464-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_465-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_466-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_467-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_475-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_474-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_470-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_472-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_483-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_478-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_469-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_487-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_473-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_488-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_496-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_479-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_477-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_484-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_482-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_471-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_491-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_481-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_492-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_495-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_493-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_490-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_476-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_489-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_494-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_504-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_500-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_499-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_505-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_498-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_510-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_502-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_501-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_512-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_514-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_509-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_511-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_513-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_515-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_516-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_507-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_517-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_518-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_519-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_521-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_522-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_524-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_526-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_531-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_525-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_527-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_530-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_537-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_544-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_553-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_540-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_532-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_538-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_536-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_551-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_539-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_541-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_545-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_546-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_535-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_534-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_533-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_542-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_556-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_552-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_564-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_561-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_554-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_559-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_548-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_581-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_568-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_558-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_567-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_565-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_576-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_575-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_562-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_573-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_589-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_574-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_592-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_579-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_583-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_588-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_582-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_584-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_594-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_591-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_595-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_597-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_603-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_598-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_605-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_604-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_608-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_599-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_619-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_612-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_634-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_622-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_609-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_632-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_618-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_614-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_627-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_616-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_625-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_628-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_641-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_630-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_655-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_635-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_621-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_617-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_623-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_650-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_654-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_629-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_640-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_633-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_648-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_649-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_638-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_636-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_646-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_659-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_658-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_645-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_665-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_660-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_661-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_662-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_656-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_667-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_653-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_666-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_669-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_668-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_670-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_671-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_673-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_678-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_677-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_676-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_683-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_684-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_681-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_680-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_682-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_679-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_685-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_686-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_693-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_698-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_691-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_696-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_702-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_701-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_692-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_695-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_690-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_708-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_706-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_697-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_687-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_714-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_710-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_707-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_719-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_720-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_721-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_717-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_727-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_715-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_724-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_713-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_705-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_716-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_728-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_718-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_725-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_722-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_723-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_734-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_732-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_736-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_730-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_737-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_740-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_738-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_739-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_741-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_743-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_742-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_750-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_749-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_744-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_746-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_745-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_758-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_760-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_748-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_751-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_757-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_762-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_756-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_761-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_752-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_754-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_763-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_765-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_755-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_747-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_768-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_772-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_777-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_767-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_759-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_764-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_769-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_773-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_766-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_771-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_770-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_780-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_774-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_775-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_779-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_776-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_784-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_781-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_778-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_787-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_793-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_790-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_794-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_799-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_797-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_801-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_800-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_805-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_806-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_809-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_808-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_817-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_815-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_823-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_811-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_824-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_812-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_834-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_816-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_810-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_821-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_807-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_814-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_825-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_818-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_822-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_830-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_813-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_828-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_836-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_831-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_839-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_842-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_844-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_838-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_835-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_829-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_832-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_852-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_827-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_850-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_845-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_841-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_853-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_848-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_843-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_856-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_847-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_851-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_858-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_859-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_857-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_867-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_865-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_866-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_868-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_869-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_871-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_870-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_872-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_873-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_876-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_874-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_875-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_882-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_877-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_881-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_883-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_878-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_896-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_884-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_900-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_899-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_897-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_885-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_880-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_893-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_902-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_916-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_901-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_898-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_915-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_904-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_895-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_903-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_905-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_909-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_914-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_919-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_921-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_918-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_910-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_908-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_913-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_917-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_928-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_920-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_922-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_912-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_924-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_930-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_929-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_932-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_935-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_933-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_937-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_939-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_938-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_936-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_944-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_941-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_950-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_942-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_948-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_946-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_945-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_949-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_957-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_953-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_956-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_947-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_960-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_954-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_955-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_951-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_966-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_958-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_977-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_959-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_976-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_968-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_974-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_978-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_965-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_970-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_990-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_989-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_979-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_972-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_986-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_985-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_994-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_987-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_993-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_996-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_988-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_992-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_995-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_982-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_999-Extended.digi",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOGHepp8MB/digi/SMOGHepp8MB_60-Extended.digi",
    ],
    qualifiers={
        'Author': 'Giacomo Graziani, Saverio Mariani',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-07-27',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        'Simulation': True,
        "ConditionsVersion": "master",
    },
    comment=
    'Private simulation with overlapped pp collisions in Run3 and one fixed per-event pHe collision within the SMOG2 cell (z in [-500, -300] mm). The gas presence is overestimated, being the expected mu in SMOG2, still not fixed and depending from the gas species and pressure, ~0.2. SciFi v6 is required. Required for PR jobs.',
    test_file_db=test_file_db)

testfiles(
    myname='SMOG2_pHe_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples_v1/SMOG2_pHe_retinacluster_digi/SMOG2_pHe_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-07-27',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        'Simulation': True,
        "ConditionsVersion": "master",
    },
    comment='Private simulation adding RetinaClusters to the SMOG2_pHe sample.',
    test_file_db=test_file_db)

testfiles(
    myname='SMOGHepp8MB_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples_v1/SMOGHepp8MB_retinacluster_digi/smog2_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2021-07-27',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        'Simulation': True,
        "ConditionsVersion": "master",
    },
    comment=
    'Private simulation adding RetinaClusters to the SMOG2_pp_pHe sample.',
    test_file_db=test_file_db)

testfiles(
    myname='Upgrade_BsPhiPhi_MD_FTv4_DIGI_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples_v1/Upgrade_BsPhiPhi_MD_FTv4_DIGI_retinacluster_digi/Upgrade_BsPhiPhi_MD_FTv4_DIGI_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2020.01.23',
        'DDDB': 'dddb-20180815',
        'CondDB': 'sim-20180530-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the Upgrade_BsPhiPhi_MD_FTv4_DIGI sample.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_DC19_01_MinBiasMD_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples_v1/upgrade_DC19_01_MinBiasMD_retinacluster_xdigi/upgrade_DC19_01_MinBiasMD_retinacluster_xdigi.xdigi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2019.07.20',
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the upgrade_DC19_01_MinBiasMD sample.',
    test_file_db=test_file_db)

testfiles(
    myname='Upgrade_KstEE_MU_FTv4_DIGI_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples_v1/Upgrade_KstEE_MU_FTv4_DIGI_retinacluster_digi/kstee_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2020.02.15',
        'DDDB': 'dddb-20180815',
        'CondDB': 'sim-20180530-vc-mu100',
        'Simulation': True,
        "ConditionsVersion": "master",
    },
    comment=
    'Private simulation adding RetinaClusters to the Upgrade_KstEE_MU_FTv4_DIGI sample.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-minbias-magdown-scifi-v5_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples_v1/upgrade-minbias-magdown-scifi-v5_retinacluster_digi/upgrade-minbias-magdown-scifi-v5_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2018.08.15',
        'DDDB': 'dddb-20180815',
        'CondDB': 'sim-20180530-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the upgrade-minbias-magdown-scifi-v5 sample.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-FT64-digi_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples_v1/upgrade-baseline-FT64-digi_retinacluster/minbias-64-1068-MagDown-Extended_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2018-10-26',
        'DDDB': 'dddb-20180815',
        'CondDB': 'sim-20180530-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the upgrade-baseline-FT64-digi sample.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-TELL40-UT-miniBias_retinacluster',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP6/Allen/digi_input/RetinaCluster_samples_v1/upgrade-TELL40-UT-miniBias_retinacluster/Boole-Extended_retinacluster.digi"
    ],
    qualifiers={
        'Author': 'Giovanni Bassi',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2020-12-04 08:01:44.324813',
        'DDDB': 'upgrade/dddb-20201105',
        'CondDB': 'upgrade/UTv4r2',
        'Simulation': True,
    },
    comment=
    'Private simulation adding RetinaClusters to the upgrade-TELL40-UT-miniBias sample.',
    test_file_db=test_file_db)

testfiles(
    myname='plume-raw-data-v1',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/plume-raw-data-v1/Run_0000226558_20220311-124351-571_PLEB01.mdf"
    ],
    qualifiers={
        'Author': 'Rosen Matev',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-03-15',
        'Simulation': False,
    },
    comment='PLUME v1 raw data from DAQ.',
    test_file_db=test_file_db)

testfiles(
    myname='plume-raw-data-v1.1',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/plume-raw-data-v1.1/Run_0000232607_20220604-065800-966_SODIN01_4264_10000.mdf"
    ],
    qualifiers={
        'Author': 'Rosen Matev',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-06-17',
        'Simulation': False,
        "DDDB": "master",
        "CondDB": "master",
    },
    comment='PLUME v1 (with coincidences) raw data from DAQ.',
    test_file_db=test_file_db)

testfiles(
    myname='plume-raw-data-v3',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/data/2024-PLUME-RAW/plume_v3.mdf"
    ],
    qualifiers={
        'Author': 'Fabio Ferrari',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-02-16',
        'Simulation': True,
        "DDDB": "dddb-20220705",
        "CondDB": "sim-20220705-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
    },
    comment='PLUME v3 raw data from local PLUME run.',
    test_file_db=test_file_db)

testfiles(
    myname='plume-raw-data-v4',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/data/2024-PLUME-RAW/plume_v4.mdf"
    ],
    qualifiers={
        'Author': 'Fabio Ferrari',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-02-16',
        'Simulation': True,
        "DDDB": "dddb-20220705",
        "CondDB": "sim-20220705-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
    },
    comment='PLUME v4 (little endian) raw data from local PLUME run.',
    test_file_db=test_file_db)

testfiles(
    myname='hltlumisummary-raw-data-v1',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/user/e/edallocc/lumi/raw_data/Run_0000249421_20221015-160520-203_HCEB04_0224.mdf"
    ],
    qualifiers={
        'Author': 'Rosen Matev',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-10-25',
        'Simulation': False,
    },
    comment='Real data with HltLumiSummary in v1 format.',
    test_file_db=test_file_db)

testfiles(
    myname='hltlumisummary-raw-data-v2',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/hltlumisummary-raw-data-v2/Run_0000252975_HLT20101_20221110-090529-822.mdf"
    ],
    qualifiers={
        'Author': 'Rosen Matev',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-11-11',
        'Simulation': False,
    },
    comment='Real data with HltLumiSummary in v2 format.',
    test_file_db=test_file_db)

testfiles(
    myname='2024_raw_hlt1_288877_tae',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2024_raw_hlt1_288877_tae/Run_0000288877_20240406-234455-805_PLEB01_0094.mdf"
    ],
    qualifiers={
        'Author': 'Rosen Matev',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-04-17',
        'Simulation': False,
        'DDDB': 'master',
        'CondDB': 'master',
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "master",
    },
    comment='Real data out of HLT1 that contains a large fraction of TAE',
    test_file_db=test_file_db)

testfiles(
    myname='2024_raw_hlt1_290683',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2024_raw_hlt1_290683/Run_0000290683_20240417-065023-114_UCEB02_0001.mdf"
    ],
    qualifiers={
        'Author': 'Rosen Matev',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-05-03',
        'Simulation': False,
        'DDDB': 'master',
        'CondDB': 'master',
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "master",
    },
    comment='Real data out of HLT1',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-minbias-alignHLT1filtered',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP4/test_files/HLT1Filtered/MinBiasD0JpsiFiltered_1.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP4/test_files/HLT1Filtered/MinBiasD0JpsiFiltered_2.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP4/test_files/HLT1Filtered/MinBiasD0JpsiFiltered_3.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP4/test_files/HLT1Filtered/MinBiasD0JpsiFiltered_4.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP4/test_files/HLT1Filtered/MinBiasD0JpsiFiltered_5.mdf"
    ],
    qualifiers={
        'Author': 'Sophie Hollitt',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-04-19',
        'DDDB': 'dddb-20210617',
        'CondDB': 'sim-20210617-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "jonrob/all-pmts-active",
        'Simulation': True,
    },
    comment=
    'Private HLT1-filtered MC for alignment testing. Sim10aU1 MinBias (30000000) MC filtered with HLT1D2KPi and HLT1DiMuonMass lines',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-magdown-Dst2D0pi-alignHLT1filtered',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP4/test_files/HLT1Filtered/Dst2D0_D0JpsiFiltered_1.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP4/test_files/HLT1Filtered/Dst2D0_D0JpsiFiltered_2.mdf"
    ],
    qualifiers={
        'Author': 'Sophie Hollitt',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-04-19',
        'DDDB': 'dddb-20210617',
        'CondDB': 'sim-20210617-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "jonrob/all-pmts-active",
        'Simulation': True,
    },
    comment=
    'Private HLT1-filtered MC for alignment testing. Sim10aU1 D*->D0pi (27163003) MC filtered with HLT1D2KPi and HLT1DiMuonMass lines',
    test_file_db=test_file_db)

testfiles(
    myname='Upgrade_PbAr_D0Lc_mdf',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_PbAr_D0Lc_mdf/Gauss_D0.mdf',
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_PbAr_D0Lc_mdf/Gauss_Lc.mdf',
    ],
    qualifiers={
        'Author': 'Chenxi Gu',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-07-22',
        'Simulation': True,
        'CondDB': 'sim-20210617-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "jonrob/all-pmts-active",
        'DDDB': 'dddb-20210617',
    },
    comment=
    'Upgrade PbAr private sample : D0:22162000 (500) And Lc:25103000(500)',
    test_file_db=test_file_db)

testfiles(
    myname='Upgrade_PbPb_minbias_mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_PbPb_minbias_mdf/MC_Upgrade_PbPbBeam2510GeVUpgradeMagDownFix1Epos_Sim10aU1_30000000.mdf',
    ],
    qualifiers={
        'Author': 'Chenxi Gu',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-07-29',
        'Simulation': True,
        'CondDB': 'sim-20210617-vc-mu100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "jonrob/all-pmts-active",
        'DDDB': 'dddb-20210617',
    },
    comment='Upgrade PbPb private minbias sample',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_Sim10aU1_minbias_xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000005_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000017_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000020_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000028_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000031_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000033_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000035_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000037_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00151675/0000/00151675_00000039_1.xdigi'
    ],
    qualifiers={
        'Author': 'Michel De Cian',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Date': '2022-08-01',
        'Simulation': True,
        'CondDB': 'sim-20210617-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "jonrob/all-pmts-active",
        'DDDB': 'dddb-20210617',
    },
    comment='Upgrade minbias sample, generated with Sim10aU1, xdigi',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_Sept2022_minbias_0fb_md_xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Jira_LHCBGAUSS-2635/Minbias_MagDown_Boole_lhcbhead3379_dddb-20220705_sim-20220705-vc-md100_lumi0.xdigi'
    ],
    qualifiers={
        'Author': 'Michel De Cian',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Date': '2022-09-29',
        'Simulation': True,
        'CondDB': 'sim-20220705-vc-md100',
        "GeometryVersion": "run3/before-rich1-geom-update-26052022",
        "ConditionsVersion": "jonrob/all-pmts-active",
        'DDDB': 'dddb-20220705',
    },
    comment=
    'Upgrade minbias sample, generated according to JIRA-LHCBGAUSS-2635, xdigi',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_Sept2022_minbias_0fb_mu_xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Jira_LHCBGAUSS-2635/Minbias_MagUp_Boole_lhcbhead3379_dddb-20220705_sim-20220705-vc-mu100_lumi0.xdigi'
    ],
    qualifiers={
        'Author': 'Michel De Cian',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Date': '2022-09-29',
        'Simulation': True,
        'CondDB': 'sim-20220705-vc-mu100',
        "GeometryVersion": "run3/before-rich1-geom-update-26052022",
        "ConditionsVersion": "jonrob/all-pmts-active",
        'DDDB': 'dddb-20220705',
    },
    comment=
    'Upgrade minbias sample, generated according to JIRA-LHCBGAUSS-2635, xdigi',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_Sept2022_minbias_0fb_md_mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Jira_LHCBGAUSS-2635/Minbias_MagDown_Boole_lhcbhead3379_dddb-20220705_sim-20220705-vc-md100_lumi0.mdf'
    ],
    qualifiers={
        'Author': 'Michel De Cian',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-09-29',
        'Simulation': True,
        'CondDB': 'sim-20220705-vc-md100',
        "GeometryVersion": "run3/before-rich1-geom-update-26052022",
        "ConditionsVersion": "jonrob/all-pmts-active",
        'DDDB': 'dddb-20220705',
    },
    comment=
    'Upgrade minbias sample, generated according to JIRA-LHCBGAUSS-2635, mdf',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_Sept2022_minbias_0fb_mu_mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Jira_LHCBGAUSS-2635/Minbias_MagUp_Boole_lhcbhead3379_dddb-20220705_sim-20220705-vc-mu100_lumi0.mdf'
    ],
    qualifiers={
        'Author': 'Michel De Cian',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-09-29',
        'Simulation': True,
        'CondDB': 'sim-20220705-vc-mu100',
        'DDDB': 'dddb-20220705',
        "GeometryVersion": "run3/before-rich1-geom-update-26052022",
        "ConditionsVersion": "jonrob/all-pmts-active",
    },
    comment=
    'Upgrade minbias sample, generated according to JIRA-LHCBGAUSS-2635, mdf',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_Sept2022_BsPhiPhi_0fb_md_xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Jira_LHCBGAUSS-2635/BsPhiPhi_MagDown_Boole_lhcbhead3379_dddb-20220705_sim-20220705-vc-md100_lumi0.xdigi'
    ],
    qualifiers={
        'Author': 'Michel De Cian',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Date': '2022-09-29',
        'Simulation': True,
        'CondDB': 'sim-20220705-vc-md100',
        "GeometryVersion": "run3/before-rich1-geom-update-26052022",
        "ConditionsVersion": "jonrob/all-pmts-active",
        'DDDB': 'dddb-20220705',
    },
    comment=
    'Upgrade BsPhiPhi sample, generated according to JIRA-LHCBGAUSS-2635, xdigi',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_Sept2022_BsPhiPhi_0fb_mu_xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Jira_LHCBGAUSS-2635/BsPhiPhi_MagUp_Boole_lhcbhead3379_dddb-20220705_sim-20220705-vc-mu100_lumi0.xdigi'
    ],
    qualifiers={
        'Author': 'Michel De Cian',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Date': '2022-09-29',
        'Simulation': True,
        'CondDB': 'sim-20220705-vc-mu100',
        'DDDB': 'dddb-20220705',
        "GeometryVersion": "run3/before-rich1-geom-update-26052022",
        "ConditionsVersion": "jonrob/all-pmts-active",
    },
    comment=
    'Upgrade BsPhiPhi sample, generated according to JIRA-LHCBGAUSS-2635, xdigi',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_Sept2022_BsPhiPhi_0fb_md_mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Jira_LHCBGAUSS-2635/BsPhiPhi_MagDown_Boole_lhcbhead3379_dddb-20220705_sim-20220705-vc-md100_lumi0.mdf'
    ],
    qualifiers={
        'Author': 'Michel De Cian',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-09-29',
        'Simulation': True,
        'CondDB': 'sim-20220705-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "jonrob/all-pmts-active",
        'DDDB': 'dddb-20220705',
    },
    comment=
    'Upgrade BsPhiPhi sample, generated according to JIRA-LHCBGAUSS-2635, mdf',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_Sept2022_BsPhiPhi_0fb_mu_mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Jira_LHCBGAUSS-2635/BsPhiPhi_MagUp_Boole_lhcbhead3379_dddb-20220705_sim-20220705-vc-mu100_lumi0.mdf'
    ],
    qualifiers={
        'Author': 'Michel De Cian',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-09-29',
        'Simulation': True,
        'CondDB': 'sim-20220705-vc-mu100',
        'DDDB': 'dddb-20220705',
        "GeometryVersion": "run3/before-rich1-geom-update-26052022",
        "ConditionsVersion": "jonrob/all-pmts-active",
    },
    comment=
    'Upgrade BsPhiPhi sample, generated according to JIRA-LHCBGAUSS-2635, mdf',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_Aug2022_BsPhiPhi_mu_sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/' +
        'upgrade_Aug2022_BsPhiPhi_mu_sim/00165649_0000006{0}_1.sim'.format(i)
        for i in range(10)
    ],
    qualifiers={
        'Author': 'Rosen Matev',
        'Format': 'SIM',
        'DataType': 'Upgrade',
        'Date': '2022-08-16',
        'Simulation': True,
        'CondDB': 'sim-20220705-vc-mu100',
        'DDDB': 'dddb-20220705',
        "GeometryVersion": "run3/before-rich1-geom-update-26052022",
        "ConditionsVersion": "jonrob/all-pmts-active",
    },
    comment=
    'Upgrade BsPhiPhi sample, generated according to LHCBGAUSS-2635, sim',
    test_file_db=test_file_db)

testfiles(
    myname='2022_raw_hlt1_253597',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/data/nov_2022/Run_0000253597_HLT24640_20221114-023831-878.mdf"
    ],
    qualifiers={
        'Author': 'Rosen Matev',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-11-13',
        'Simulation': False,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "upgrade/dddb-20221004-new-particle-table",
        "CondDB": "md_VP_SciFi_macromicrosurvey_from20220923",
    },
    comment='PLUME v1 (with coincidences) raw data from DAQ.',
    test_file_db=test_file_db)

testfiles(
    myname='2022_mep_253895',
    filenames=[
        "mdf:root://eoslhcb.cern.ch///eos/lhcb/wg/rta/data/nov_2022_mep/bu_253895_LHCb_ECEB01_BU_0.mep",
        "mdf:root://eoslhcb.cern.ch///eos/lhcb/wg/rta/data/nov_2022_mep/bu_253895_LHCb_ECEB02_BU_0.mep",
        "mdf:root://eoslhcb.cern.ch///eos/lhcb/wg/rta/data/nov_2022_mep/bu_253895_LHCb_ECEB03_BU_0.mep",
        "mdf:root://eoslhcb.cern.ch///eos/lhcb/wg/rta/data/nov_2022_mep/bu_253895_LHCb_ECEB04_BU_0.mep",
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'Format': 'MEP',
        'DataType': 'Upgrade',
        'Date': '2022-11-15',
        'Simulation': False,
        "DDDB": "dddb-20221004",
        "CondDB": "sim-20221120-vc-md100",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "alignment2022"
    },
    comment='MEP raw data from DAQ.',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade-baseline-ut-new-geo-digi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/upgrade-ut/Boole-10ev-Extended.digi'
    ],
    qualifiers={
        'Author': 'xuyuan',
        'DataType': 'Upgrade',
        'Format': 'DIGI',
        'Simulation': True,
        'CondDB': 'upgrade/UTv4r2-newUTID',
        'DDDB': 'upgrade/UTv4r2-newUTID',
        'Date': '2022-05-17 23:47:20.529593',
        'QMTests': 'brunel-upgrade-baseline-ut-new-geo.qmt'
    },
    comment='miniBias sample with new UT geometry and new chanID',
    test_file_db=test_file_db)

testfiles(
    myname='2023_07_04_run268773',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2023_07_04_run268773/Run_0000268773_20230704-110603-044_SAEB11_0001.mdf",
    ],
    qualifiers={
        'Author': 'Sebastien Ponce',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-07-04',
        'Simulation': False,
        "DDDB": "master",
        "CondDB": "master",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master"
    },
    comment='Example data file, used for phoenix event display testing.',
    test_file_db=test_file_db)

testfiles(
    myname='mdfreadingexample',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/mdfreadingexample/1k-compressed.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/mdfreadingexample/1k-compressed.zstd.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/mdfreadingexample/1k-uncompressed.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/mdfreadingexample/1k-uncompressed.zstd.mdf",
    ],
    qualifiers={
        'Author': 'jonrob',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-08-01',
        'Simulation': False,
        "DDDB": "master",
        "CondDB": "master",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master"
    },
    comment='2022 raw data to be used for testing IOAlg/IOSvc*',
    test_file_db=test_file_db)

testfiles(
    myname='Run251995_ppandSMOG2_RealData',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/Run251995_ppandSMOG2_RealData/Run_0000251995_HLT20101_20221102-154523-256.mdf",
    ],
    qualifiers={
        'Author': 'Kara Mattioli',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-08-04',
        'Simulation': False,
        "DDDB": "master",
        "CondDB": "master",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master"
    },
    comment=
    '2022 raw data with SMOG2 injection, for testing on data with both pp and SMOG2 collisions',
    test_file_db=test_file_db)

testfiles(
    myname='2023_raw_hlt1_269939',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2023_raw_hlt1_269939/Run_0000269939_20230711-194825-096_PLEB01_0001.mdf"
    ],
    qualifiers={
        'Author': 'Rosen Matev',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-10-13',
        'Simulation': False,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "master",
        "CondDB": "master",
    },
    comment='Real data selected by HLT1 from run 269939.',
    test_file_db=test_file_db,
)

testfiles(
    myname='2023_raw_hlt1_269138',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/data/2023_raw_hlt1_269138/Run_0000269138_20230706-083945-793_PLEB01_1930.mdf"
    ],
    qualifiers={
        'Author': 'Rosen Matev',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-12-19',
        'Simulation': False,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "master",
        "CondDB": "master",
    },
    comment=
    'Real data selected by HLT1 from run 269138 that is affected by broken HLT1 DecReports, see https://lbproblems.cern.ch/problemdb/problems/15/',
    test_file_db=test_file_db)

testfiles(
    myname='2024_raw_hlt1_289254',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/data/289254-LHCb-RAW/Run_0000289254_20240409-045959-322_R1EB18_0298.mdf"
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-04-09',
        'Simulation': False,
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "master",
        "DDDB": "master",
        "CondDB": "master",
    },
    comment='Real data selected by HLT1 from run 289254.',
    test_file_db=test_file_db,
)

testfiles(
    myname='Upgrade_Bu2TauNu_Tau2PiPiPiNu_HeavyFlavour_MCHit_Filtered',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Upgrade_Bu2TauNu/b2taunu_tau2pipipinu_hf_mchit_filtered_magdown.xdigi"
    ],
    qualifiers={
        'Author': 'Maarten van Veghel',
        'DataType': 'Upgrade',
        'Format': 'XDIGI',
        'Date': '2023-08-27',
        'Simulation': True,
        'DDDB': 'dddb-20210617',
        'CondDB': 'sim-20210617-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'jonrob/all-pmts-active',
    },
    comment=
    'Upgrade MC Bu2TauNu, Tau2PiPiPiNu; filtered on having MCHit (in VELO) from Bu or Tau, for heavy-flavour track reco tests',
    test_file_db=test_file_db)

testfiles(
    myname='exp_24_minbias_Sim10c_magdown',
    filenames=[
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00001476_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00001481_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00003710_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00003721_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00005093_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00005160_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00005219_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00006206_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00006666_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00006855_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00006995_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00007281_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00007962_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00008009_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00008538_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00009297_1.digi",
        "root://gridproxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00204940/0000/00204940_00009449_1.digi",
    ],
    qualifiers={
        'Author': 'Luke Grazette',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2023-11-27',
        'Simulation': True,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20231017",
        "CondDB": "sim-20231017-vc-md100",
    },
    comment='\n'.join([
        "Small amount of exp.24 minbias MC. See bkk for ~30M evts/polarity",
        "bkk: /MC/Dev/Beam6800GeV-expected-2024-MagDown-Nu7.6-25ns-Pythia8/Sim10c/30000000/DIGI",
        "request: https://gitlab.cern.ch/lhcb-rta/mc-requests/-/issues/15",
        "exp24: https://gitlab.cern.ch/lhcb/opg/-/issues/28#note_7278057"
    ]),
    test_file_db=test_file_db)

testfiles(
    'expected-2024_B2JpsiK_ee_MD', [
        'root://x509up_u137380@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Dev/DIGI/00205665/0000/00205665_00000051_1.digi'
    ], {
        'Author': 'Albert Lopez',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Simulation': True,
        'Date': '2023-12-13',
        'DDDB': 'dddb-20231017',
        'CondDB': 'sim-20231017-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
    },
    'Test sample of B2JpsiK with Jpsi2ee for electron run 3 studies, MagDown, Sim10c',
    test_file_db=test_file_db)

testfiles(
    myname='expected_2024_min_bias_mdf',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta//WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/30000000/allen_input_{0}.mdf'
        .format(i) for i in range(65)
    ],
    qualifiers={
        'Author': 'Ross Hunter',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-03-22',
        'Simulation': True,
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'jonrob/all-pmts-active',
        "DDDB": "dddb-20231017",
        "CondDB": "sim-20231017-vc-md100",
    },
    comment='\n'.join([
        "Same as exp_24_minbias_Sim10c_magdown but just in MDF format",
        "for usage in HLT1 bandwidth tests"
    ]),
    test_file_db=test_file_db)

testfiles(
    myname='expected_2024_min_bias_hlt1_filtered',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v1/30000000/input_0.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v1/30000000/input_1.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v1/30000000/input_2.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v1/30000000/input_3.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v1/30000000/input_4.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v1/30000000/input_5.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v1/30000000/input_6.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v1/30000000/input_7.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v1/30000000/input_8.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v1/30000000/input_9.mdf'
    ],
    qualifiers={
        'Author': 'Ross Hunter',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-01-23',
        'Simulation': True,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20231017",
        "CondDB": "sim-20231017-vc-md100",
    },
    comment='\n'.join([
        "Around 100k HLT1-filtered min bias in expected 2024 conditions",
        "filtered by HLT1 bandwidth division according to 'post-patch weighted' scenario",
        "see: https://indico.cern.ch/event/1332271/contributions/5710654/attachments/2778230/4842174/HLT1_BW_Division_LHCb_UK_Update.pdf",
        "also: https://indico.cern.ch/event/1361635/contributions/5753029/attachments/2780757/4846644/bandwidth_division_jan15.pdf",
        "request: https://gitlab.cern.ch/lhcb-rta/mc-requests/-/issues/15",
        "exp24: https://gitlab.cern.ch/lhcb/opg/-/issues/28#note_7278057"
    ]),
    test_file_db=test_file_db)

testfiles(
    myname='minbias-Beam6800GeV-expected-2024-MagUp-Nu1.6-hlt1-filtered',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagUp-Nu1.6/hlt1_filtered/v1/30000000/input_{0}.mdf"
        .format(i) for i in range(8)
    ],
    qualifiers={
        'Author': 'Tim Evans',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20231017',
        'CondDB': 'sim-20231017-vc-md100',
        'Date': '2024-01-31'
    },
    comment=
    'Min bias mdfs for testing upgrade Hlt2 reconstruction. From sim+std://MC/Dev/Beam6800GeV-expected-2024-Mag*-Nu*-Pythia8 . Filtered using by HLT1 using the configuration resulting from the BW optimisation for 2024 presented at https://indico.cern.ch/event/1361635/contributions/5753029/attachments/2780757/4846644/bandwidth_division_jan15.pdf',
    test_file_db=test_file_db)

testfiles(
    myname='minbias-Beam6800GeV-expected-2024-MagUp-Nu4.3-hlt1-filtered',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagUp-Nu4.3/hlt1_filtered/v1/30000000/input_{0}.mdf"
        .format(i) for i in range(16)
    ],
    qualifiers={
        'Author': 'Tim Evans',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20231017',
        'CondDB': 'sim-20231017-vc-md100',
        'Date': '2024-01-31'
    },
    comment=
    'Min bias mdfs for testing upgrade Hlt2 reconstruction. From sim+std://MC/Dev/Beam6800GeV-expected-2024-Mag*-Nu*-Pythia8 . Filtered using by HLT1 using the configuration resulting from the BW optimisation for 2024 presented at https://indico.cern.ch/event/1361635/contributions/5753029/attachments/2780757/4846644/bandwidth_division_jan15.pdf',
    test_file_db=test_file_db)

testfiles(
    myname='minbias-Beam6800GeV-expected-2024-MagUp-Nu5.7-hlt1-filtered',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagUp-Nu5.7/hlt1_filtered/v1/30000000/input_{0}.mdf"
        .format(i) for i in range(20)
    ],
    qualifiers={
        'Author': 'Tim Evans',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20231017',
        'CondDB': 'sim-20231017-vc-md100',
        'Date': '2024-01-31'
    },
    comment=
    'Min bias mdfs for testing upgrade Hlt2 reconstruction. From sim+std://MC/Dev/Beam6800GeV-expected-2024-Mag*-Nu*-Pythia8 . Filtered using by HLT1 using the configuration resulting from the BW optimisation for 2024 presented at https://indico.cern.ch/event/1361635/contributions/5753029/attachments/2780757/4846644/bandwidth_division_jan15.pdf',
    test_file_db=test_file_db)

testfiles(
    myname='minbias-Beam6800GeV-expected-2024-MagDown-Nu1.6-hlt1-filtered',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu1.6/hlt1_filtered/v1/30000000/input_{0}.mdf"
        .format(i) for i in range(7)
    ],
    qualifiers={
        'Author': 'Tim Evans',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20231017',
        'CondDB': 'sim-20231017-vc-md100',
        'Date': '2024-01-31'
    },
    comment=
    'Min bias mdfs for testing upgrade Hlt2 reconstruction. From sim+std://MC/Dev/Beam6800GeV-expected-2024-Mag*-Nu*-Pythia8 . Filtered using by HLT1 using the configuration resulting from the BW optimisation for 2024 presented at https://indico.cern.ch/event/1361635/contributions/5753029/attachments/2780757/4846644/bandwidth_division_jan15.pdf',
    test_file_db=test_file_db)

testfiles(
    myname='minbias-Beam6800GeV-expected-2024-MagDown-Nu4.3-hlt1-filtered',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu4.3/hlt1_filtered/v1/30000000/input_{0}.mdf"
        .format(i) for i in range(16)
    ],
    qualifiers={
        'Author': 'Tim Evans',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20231017',
        'CondDB': 'sim-20231017-vc-md100',
        'Date': '2024-01-31'
    },
    comment=
    'Min bias mdfs for testing upgrade Hlt2 reconstruction. From sim+std://MC/Dev/Beam6800GeV-expected-2024-Mag*-Nu*-Pythia8 . Filtered using by HLT1 using the configuration resulting from the BW optimisation for 2024 presented at https://indico.cern.ch/event/1361635/contributions/5753029/attachments/2780757/4846644/bandwidth_division_jan15.pdf',
    test_file_db=test_file_db)

testfiles(
    myname='minbias-Beam6800GeV-expected-2024-MagDown-Nu5.7-hlt1-filtered',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu5.7/hlt1_filtered/v1/30000000/input_{0}.mdf"
        .format(i) for i in range(21)
    ],
    qualifiers={
        'Author': 'Tim Evans',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Simulation': True,
        'DDDB': 'dddb-20231017',
        'CondDB': 'sim-20231017-vc-md100',
        'Date': '2024-01-31'
    },
    comment=
    'Min bias mdfs for testing upgrade Hlt2 reconstruction. From sim+std://MC/Dev/Beam6800GeV-expected-2024-Mag*-Nu*-Pythia8 . Filtered using by HLT1 using the configuration resulting from the BW optimisation for 2024 presented at https://indico.cern.ch/event/1361635/contributions/5753029/attachments/2780757/4846644/bandwidth_division_jan15.pdf',
    test_file_db=test_file_db)

testfiles(
    myname='expected_2024_min_bias_hlt1_filtered_v2',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v2/30000000/input_0.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v2/30000000/input_1.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v2/30000000/input_2.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v2/30000000/input_3.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v2/30000000/input_4.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v2/30000000/input_5.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v2/30000000/input_6.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v2/30000000/input_7.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v2/30000000/input_8.mdf',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/Beam6800GeV-expected-2024-MagDown-Nu7.6/hlt1_filtered/v2/30000000/input_9.mdf',
    ],
    qualifiers={
        'Author': 'Shunan Zhang',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-03-22',
        'Simulation': True,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "jonrob/all-pmts-active",
        "DDDB": "dddb-20231017",
        "CondDB": "sim-20231017-vc-md100",
    },
    comment='\n'.join([
        "Around 100k HLT1-filtered min bias in expected 2024 conditions",
        "v2 update in March 2024 - HLT1 output rate of 1 MHz",
        "same HLT1 input as expected_2024_min_bias_hlt1_filtered",
        "filtered by HLT1 bandwidth division according to Allen thresholds on commit cf2073ab of tevans/tuned_2024"
    ]),
    test_file_db=test_file_db)

testfiles(
    myname='expected_2024_min_bias_hlt2_full_stream_v2',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/hlt2_full_stream_20240322/hlt2_full_stream_{0}.mdf'
        .format(i) for i in range(148)
    ],
    qualifiers={
        'Author': 'Shunan Zhang',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-03-22',
        'Simulation': True,
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20231017",
        "CondDB": "sim-20231017-vc-md100",
    },
    comment='\n'.join([
        "Output of HLT2 full stream running on HLT1-filtered min bias in expected 2024 conditions",
        "v2 update in March 2024",
        "include HLT2 full stream update as of March 21, 2024",
        "used as input files to the Sprucing PR tests",
        "efficiency around 5.9%, input rate around 59 kHz",
        "lhcb-metadata key: adb09877"
    ]),
    test_file_db=test_file_db)

testfiles(
    myname='expected_2024_minbias_xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_minbias_xdigi/00212966_00000086_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_minbias_xdigi/00212966_00000013_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_minbias_xdigi/00212966_00000018_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_minbias_xdigi/00212966_00000026_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_minbias_xdigi/00212966_00000031_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_minbias_xdigi/00212966_00000036_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_minbias_xdigi/00212966_00000044_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_minbias_xdigi/00212966_00000074_1.xdigi',
    ],
    qualifiers={
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Date': '2024-02-15',
        'Simulation': True,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "jonrob/all-pmts-active",
        "DDDB": "dddb-20231017",
        "CondDB": "sim-20231017-vc-mu100",
    },
    comment='\n'.join([
        "MinBias (30000000) XDIGI based on expected 2024 conditions. Input for tests and LHCbPR jobs.",
        "Request: https://gitlab.cern.ch/lhcb-rta/mc-requests/-/issues/24",
        "Sim exp24: https://gitlab.cern.ch/lhcb/opg/-/issues/28",
        "ProductionID: 126479",
        "BKK: /MC/Dev/Beam6800GeV-expected-2024-MagUp-Nu7.6-25ns-Pythia8/Sim10c/30000000/XDIGI"
    ]),
    test_file_db=test_file_db)

testfiles(
    myname='expected_2024_BsToJpsiPhi_xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000012_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000021_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000029_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000040_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000046_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000063_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000070_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000077_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000081_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000090_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BsToJpsiPhi_xdigi/00212964_00000118_1.xdigi'
    ],
    qualifiers={
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Date': '2024-02-15',
        'Simulation': True,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "jonrob/all-pmts-active",
        "DDDB": "dddb-20231017",
        "CondDB": "sim-20231017-vc-mu100",
    },
    comment='\n'.join([
        "BsToJpsiPhi (13144011) XDIGI based on expected 2024 conditions. Input for tests and LHCbPR jobs.",
        "Request: https://gitlab.cern.ch/lhcb-rta/mc-requests/-/issues/24",
        "Sim exp24: https://gitlab.cern.ch/lhcb/opg/-/issues/28",
        "ProductionID: 126479",
        "BKK: /MC/Dev/Beam6800GeV-expected-2024-MagUp-Nu7.6-25ns-Pythia8/Sim10c/13144011/XDIGI"
    ]),
    test_file_db=test_file_db)

testfiles(
    myname='expected_2024_BdToKstgamma_xdigi',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000010_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000011_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000016_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000025_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000030_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000039_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000052_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000058_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000073_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000083_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000089_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000090_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000093_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000101_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000113_1.xdigi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_BdToKstgamma_xdigi/00212962_00000116_1.xdigi'
    ],
    qualifiers={
        'Author': 'Miroslav Saur',
        'Format': 'XDIGI',
        'DataType': 'Upgrade',
        'Date': '2024-02-15',
        'Simulation': True,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "jonrob/all-pmts-active",
        "DDDB": "dddb-20231017",
        "CondDB": "sim-20231017-vc-mu100",
    },
    comment='\n'.join([
        "BdToKstgamma (11102201) XDIGI based on expected 2024 conditions. Input for tests and LHCbPR jobs.",
        "Request: https://gitlab.cern.ch/lhcb-rta/mc-requests/-/issues/24",
        "Sim exp24: https://gitlab.cern.ch/lhcb/opg/-/issues/28",
        "ProductionID: 126479",
        "BKK: /MC/Dev/Beam6800GeV-expected-2024-MagUp-Nu7.6-25ns-Pythia8/Sim10c/13144011/XDIGI"
    ]),
    test_file_db=test_file_db)

testfiles(
    myname='rootreadingexample',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rootreadingexample/hlt2_example.dst",
    ],
    qualifiers={
        'Author': 'sponce',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2024-04-08',
        'Simulation': False,
        "DDDB": "master",
        "CondDB": "master",
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master"
    },
    comment='Tiny example root file, useful for testing IO',
    test_file_db=test_file_db)

testfiles(
    myname='2024_hlt1_mdf_dump_291820',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/data/apr_2024_mdf/291820/Run_0000291820_20240424-025044-259_PLEB01_0607.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/data/apr_2024_mdf/291820/Run_0000291820_20240424-025045-704_SAEB09_0611.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/data/apr_2024_mdf/291820/Run_0000291820_20240424-025045-711_SAEB06_0575.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/data/apr_2024_mdf/291820/Run_0000291820_20240424-025045-757_SCEB17_0617.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/data/apr_2024_mdf/291820/Run_0000291820_20240424-025045-785_R2EB02_0615.mdf",
    ],
    qualifiers={
        'Author': 'Andre Guenther',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-05-17',
        'Simulation': False,
        "DDDB": "",
        "CondDB": "",
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "master"
    },
    comment=
    'HLT1-filtered (0x1000104A) MDFs for 2024 throughput test on data with mu=5.5 from run 291820.',
    test_file_db=test_file_db)

testfiles(
    myname='2024_hlt1_mdf_dumps_ReverseGEC12K',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/user/d/dmagdali/reverseGECHlt1Filtered/hlt2_reverseGec12000_2500000.mdf"
    ],
    qualifiers={
        'Author': 'Daniel Magdalinski',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-09-19',
        'Simulation': False,
        "DDDB": "",
        "CondDB": "",
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "master"
    },
    comment=
    'HLT1-filtered MDF dumps from runs:[3050{40,41,42,46,48,50,51,52,53,54,55,56,57,58,59,70}] and then filtered using a reverse GEC set to 12000 SciFi hits. \
    Originally 2.5M minbias events, now 50k busy events for max candidates studies.',
    test_file_db=test_file_db)

testfiles(
    myname='unfiltered-expected-2024-MB-pAr-nu0_37-pp-nu7_6',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/MixedEvents_pAr_nu037/unfiltered/'
        'eventmixing_eventmix_Job{0}.mdf'.format(i) for i in range(1, 71)
    ],
    qualifiers={
        'Author': 'Oscar Boente',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20231017-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20231017',
        'Date': '2024-04-25'
    },
    comment=
    ('Unfiltered sample with pAr collisions (generated in SMOG2) and pp collisions'
     'nu of pAr is 0.37 and nu of pp is 7.6.'
     'The sample includes events with zero pAr interactions, these were including by mixing the events '
     ' of standalone pp simulation and pAr+pp simulation with a proportion pp to pAr of ( 0.691, 0.309 )'
     ),
    test_file_db=test_file_db)

testfiles(
    myname='hlt1-filtered-expected-2024-MB-pAr-nu0_37-pp-nu7_6',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/MixedEvents_pAr_nu037/hlt1_filtered/'
        'hlt1_mixedsample_Job{0}.mdf'.format(i) for i in range(0, 71)
    ],
    qualifiers={
        'Author': 'Oscar Boente',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20231017-vc-md100',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        'DDDB': 'dddb-20231017',
        'Date': '2024-05-06'
    },
    comment=
    ('hlt1-filtered simulation sample with pAr collisions (generated in SMOG2) and pp collisions'
     'nu of pAr is 0.37 and nu of pp is 7.6.'
     'hlt1 is run with the hlt1_pp_matching_no_ut_1000KHz sequence. Total output hlt1 rate is 1610 +/- 28.7 kHz'
     'Per group of line, the rate distribution is the following: '
     'Group:	 hlt1_smog lines               Incl: 206.569 +/- 10.518 kHz, Excl: 198.479 +/- 10.312 kHz'
     'Group:	 Hlt1TrackMVA lines            Incl: 842.996 +/- 21.021 kHz, Excl: 706.002 +/- 19.282 kHz'
     'Group:	 Hlt1Muon lines                Incl: 327.382 +/- 13.215 kHz, Excl: 265.897 +/- 11.922 kHz'
     'Group:	 Hlt1Charm lines               Incl: 122.431 +/-  8.109 kHz, Excl:  48.541 +/-  5.112 kHz'
     'Group:	 Hlt1NotPhysics lines          Incl: 530.715 +/- 16.768 kHz, Excl: 441.723 +/- 15.321 kHz'
     'The sample includes events with zero pAr interactions, these were including by mixing the events '
     ' of standalone pp simulation and pAr+pp simulation with a proportion pp to pAr of ( 0.691, 0.309 )'
     ),
    test_file_db=test_file_db)

testfiles(
    myname='2024_mep_292860_run_change_test',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2024_mep_292860_run_change_test/bu_292860_LHCb_SAEB10_BU_0_0x10000001.mep",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2024_mep_292860_run_change_test/bu_292861_LHCb_SAEB10_BU_1_0x10000002.mep"
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'Format': 'MEP',
        'DataType': 'Upgrade',
        'Date': '2024-05-24',
        'Simulation': False,
        "DDDB": "",
        "CondDB": "",
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "master"
    },
    comment=
    'Modified MEPs (run number and TCK-in-ODIN) for testing of fast run change capability of HLT1; originally from run 292860.',
    test_file_db=test_file_db)

testfiles(
    myname='2024_mep_289232',
    filenames=[
        "mdf:root://eoslhcb.cern.ch///eos/lhcb/wg/rta/data/apr_2024_mep/bu_289232_LHCb_300k.mep",
    ],
    qualifiers={
        'Author': 'Roel Aaij',
        'Format': 'MEP',
        'DataType': 'Upgrade',
        'Date': '2024-04-09',
        'Simulation': False,
        "DDDB": "dddb-20221004",
        "CondDB": "sim-20221120-vc-md100",
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "master"
    },
    comment=
    'MEP raw data from run 289232 in one large file for the HLT1 throughput test.',
    test_file_db=test_file_db)

testfiles(
    myname='hltlumisummarytupling',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/Luminosity/dataForTesting/295153_00140014_0041LumiVdM0524.raw'
    ],
    qualifiers={
        'Author': 'Edoardo Franzoso',
        'Format': 'MDF',
        'DataType': '2024',
        'Date': '2024-05-18',
        'Simulation': False,
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "master",
        "DDDB": "",
        "CondDB": "",
    },
    comment=
    'Real vdM data with HltLumiSummary in Lumi stream after Hlt2 processing',
    test_file_db=test_file_db)

testfiles(
    myname='lumibeamgastupling',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/Luminosity/dataForTesting/294891_00150001_0023BeamGasVdM0524.raw'
    ],
    qualifiers={
        'Author': 'Edoardo Franzoso',
        'Format': 'MDF',
        'DataType': '2024',
        'Date': '2024-05-18',
        'Simulation': False,
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "master",
        "DDDB": "",
        "CondDB": "",
    },
    comment='Real vdM data in BeamGas stream after Hlt2 processing',
    test_file_db=test_file_db)

testfiles(
    myname='2024_magoff',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/data/jun_2024_mdf/298370/Run_0000298370_20240620-002642-348_PLEB01_0576.mdf"
    ],
    qualifiers={
        'Author': 'Maarten van Veghel',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-06-21',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'master'
    },
    comment='2024 magnet off to test mag off reco sequences for e.g. alignment',
    test_file_db=test_file_db)

testfiles(
    myname='hlt1-filtered-expected-2024-MB-PbAr',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/RTA/mdfs_processed_hlt1_PbPb_PbSMOG/hlt1_expected-2024-PbAr-minbias-digi.mdf'
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20231017-vc-md100',
        'DDDB': 'dddb-20231017',
        'Date': '2024-07-12 00:00:00'
    },
    comment=('Official PbAr sample produced with HLT1_PbPb_PbAr sequence.'),
    test_file_db=test_file_db)

testfiles(
    myname='hlt1-filtered-expected-2024-MB-PbPb',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/RTA/mdfs_processed_hlt1_PbPb_PbSMOG/hlt1_expected-2024-PbPb-minbias-xdigi.mdf'
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20231017-vc-md100',
        'DDDB': 'dddb-20231017',
        'Date': '2024-07-12 00:00:00'
    },
    comment=('Official PbAr sample produced with HLT1_PbPb_PbAr sequence.'),
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_2024.Q1.2_Minbias_md_sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000104_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000106_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000112_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000128_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000131_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000133_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000134_1.sim',
    ],
    qualifiers={
        'Author': 'Zehua Xu',
        'Format': 'SIM',
        'DataType': 'Upgrade',
        'Date': '2024-09-16',
        'Simulation': True,
        'CondDB': 'sim10-2024.Q1.2-v1.1-md100',
        'DDDB': 'dddb-20240427',
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "sim10/run3-ideal",
    },
    comment='Run3 minimum bias sample, sim',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_2024.Q1.2_BsToPhiPhi_md_sim',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000105_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000106_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000108_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000109_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000110_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000113_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000118_1.sim',
    ],
    qualifiers={
        'Author': 'Zehua Xu',
        'Format': 'SIM',
        'DataType': 'Upgrade',
        'Date': '2024-09-16',
        'Simulation': True,
        'CondDB': 'sim10-2024.Q1.2-v1.1-md100',
        'DDDB': 'dddb-20240427',
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "sim10/run3-ideal",
    },
    comment='Run3 Bs2PhiPhi sample, sim',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_2024.Q1.2_Minbias_md_sim_nu7.6',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230946_00000102_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230946_00000108_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230946_00000109_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230946_00000118_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230946_00000122_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230946_00000123_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230946_00000124_1.sim',
    ],
    qualifiers={
        'Author': 'Zehua Xu',
        'Format': 'SIM',
        'DataType': 'Upgrade',
        'Date': '2024-09-16',
        'Simulation': True,
        'CondDB': 'sim10-2024.Q1.2-v1.1-md100',
        'DDDB': 'dddb-20240427',
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "sim10/run3-ideal",
    },
    comment='Run3 minimum bias sample, sim, nu=7.6',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_2024.Q1.2_BsToPhiPhi_md_sim_nu7.6',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230945_00000107_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230945_00000112_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230945_00000113_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230945_00000118_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230945_00000119_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230945_00000123_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230945_00000126_1.sim',
    ],
    qualifiers={
        'Author': 'Zehua Xu',
        'Format': 'SIM',
        'DataType': 'Upgrade',
        'Date': '2024-09-16',
        'Simulation': True,
        'CondDB': 'sim10-2024.Q1.2-v1.1-md100',
        'DDDB': 'dddb-20240427',
        "GeometryVersion": "run3/2024.Q1.2-v00.00",
        "ConditionsVersion": "sim10/run3-ideal",
    },
    comment='Run3 Bs2PhiPhi sample, sim, nu=7.6',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_2024.Q1.2_Minbias_md_sim_master',
    filenames=[
        '/root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000104_1.sim',
        '/root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000106_1.sim',
        '/root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000112_1.sim',
        '/root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000128_1.sim',
        '/root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000131_1.sim',
        '/root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000133_1.sim',
        '/root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230954_00000134_1.sim',
    ],
    qualifiers={
        'Author': 'Zehua Xu',
        'Format': 'SIM',
        'DataType': 'Upgrade',
        'Date': '2024-09-16',
        'Simulation': True,
        'CondDB': 'upgrade/master',
        'DDDB': 'upgrade/master',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "sim10/run3-ideal",
    },
    comment='Run3 minimum bias sample, sim',
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_2024.Q1.2_BsToPhiPhi_md_sim_master',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000105_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000106_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000108_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000109_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000110_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000113_1.sim',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/boole_integration_run3/MC_2024_SIM/00230953_00000118_1.sim',
    ],
    qualifiers={
        'Author': 'Zehua Xu',
        'Format': 'SIM',
        'DataType': 'Upgrade',
        'Date': '2024-09-16',
        'Simulation': True,
        'CondDB': 'upgrade/master',
        'DDDB': 'upgrade/master',
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "sim10/run3-ideal",
    },
    comment='Run3 Bs2PhiPhi sample, sim',
    test_file_db=test_file_db)

testfiles(
    myname='rich-decode-2022',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-2022/data-0015.mdf"
    ],
    qualifiers={
        'Author': 'jonrob',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-01-20',
        'Simulation': False,
        'DDDB': 'upgrade/master',
        'CondDB': 'upgrade/master',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'master'
    },
    comment=
    "Input data for LHCb's RichDetectors.test-decode-and-spacepoints-2022-data test",
    test_file_db=test_file_db)

testfiles(
    myname='rich-decode-2022-panoptes',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-2022-panoptes/data-0000.mdf"
    ],
    qualifiers={
        'Author': 'jonrob',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-01-20',
        'Simulation': False,
        'DDDB': 'upgrade/master',
        'CondDB': 'upgrade/master',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'AlignmentV12_2023_06_22'
    },
    comment="Input data for Panoptes' tests",
    test_file_db=test_file_db)

testfiles(
    myname='rich-mirror-align-2022',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-mirror-align-2022/256276_1.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-mirror-align-2022/256276_2.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-mirror-align-2022/256276_3.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-mirror-align-2022/256276_4.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-mirror-align-2022/256276_5.mdf"
    ],
    qualifiers={
        'Author': 'jonrob',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-01-20',
        'Simulation': False,
        'DDDB': 'upgrade/master',
        'CondDB': 'upgrade/master',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'AlignmentV12_2023_06_22'
    },
    comment=
    "Input data for LHCb's RichDetectors.test-decode-and-spacepoints-2022-data test",
    test_file_db=test_file_db)

testfiles(
    myname='rich-decode-2023',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-2023/data-0020.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-2023/data-0010.mdf"
    ],
    qualifiers={
        'Author': 'jonrob',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-06-21',
        'Simulation': False,
        'DDDB': 'upgrade/master',
        'CondDB': 'upgrade/master',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'master'
    },
    comment=
    "Input data for LHCb's RichDetectors.test-decode-and-spacepoints-2023-data test",
    test_file_db=test_file_db)

testfiles(
    myname='rich-decode-2024',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-2024/data-0002.mdf"
    ],
    qualifiers={
        'Author': 'jonrob',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-06-12',
        'Simulation': False,
        'DDDB': 'upgrade/master',
        'CondDB': 'upgrade/master',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'master'
    },
    comment=
    "Input data for LHCb's RichDetectors.test-decode-and-spacepoints-2024-data test",
    test_file_db=test_file_db)

testfiles(
    myname='rich-decode-2024-panoptes',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-2024-panoptes/data-0004.mdf"
    ],
    qualifiers={
        'Author': 'jonrob',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-06-12',
        'Simulation': False,
        'DDDB': 'upgrade/master',
        'CondDB': 'upgrade/master',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'master'
    },
    comment="Input data for Panoptes' tests",
    test_file_db=test_file_db)

testfiles(
    myname='rich-decode-2024-mc',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/user/j/jonrob/data/MC/Run3/30000000/dddb-20231017/sim-20231017-vc-md100/2024-Expected-lumi_2.0e33/DIGI/30000000_42604869_%08d.digi"
        % i for i in range(0, 80)
    ],
    qualifiers={
        'Author': 'jonrob',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2024-09-07',
        'Simulation': True,
        'DDDB': 'upgrade/dddb-20231017-new-particle-table',
        'CondDB': 'upgrade/sim-20231017-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'jonrob/all-pmts-active'
    },
    comment=
    "Input data for LHCb's RichDetectors.test-decode-and-spacepoints-2024-data test ",
    test_file_db=test_file_db)

testfiles(
    myname='rich-decode-detdesc-compat-old-rich1',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_100.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_101.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_102.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_103.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_104.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_105.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_106.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_107.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_108.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_109.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_110.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_111.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_112.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_113.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_114.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_115.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_116.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_117.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_118.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_119.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_120.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_121.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_122.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_123.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_124.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_125.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_126.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_127.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_128.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_129.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_130.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_131.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_132.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_133.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_134.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_135.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_136.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_137.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_138.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_139.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_140.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_141.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_142.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_143.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_144.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_145.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_146.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_147.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_148.xdst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-decode-detdesc-compat-old-rich1/Brunel_149.xdst"
    ],
    qualifiers={
        'Author': 'jonrob',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2024-06-12',
        'Simulation': True,
        'DDDB': 'dddb-20220705',
        'CondDB': 'sim-20220705-vc-mu100',
        'GeometryVersion': 'run3/before-rich1-geom-update-26052022',
        'ConditionsVersion': 'jonrob/all-pmts-active'
    },
    comment="Input data for several LHCb's RichDetectors tests",
    test_file_db=test_file_db)

testfiles(
    myname='rich-multiple-runs',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/rich-multiple-runs/2022-data.mdf"
    ],
    qualifiers={
        'Author': 'jonrob',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-12-06',
        'Simulation': False,
        'DDDB': 'upgrade/master',
        'CondDB': 'AlignmentV9_2023_03_16_VPSciFiRich',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'AlignmentV9_2023_03_16_VPSciFiRich'
    },
    comment=
    "Input data for LHCb's RichDetectors.test-run-change-2022-data.qmt test ",
    test_file_db=test_file_db)

testfiles(
    myname='corruptedMDF',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/corruptedMDF/289233_00120016_0000.evt-404335.raw"
    ],
    qualifiers={
        'Author': 'cburr',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-10-15',
        'Simulation': False,
        'DDDB': 'trunk',
        'CondDB': 'master',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'sim10/run3-ideal',
    },
    comment=
    "This MDF file has corrupted banks. It should fail when reading, and not loop indefinitely as used to be",
    test_file_db=test_file_db)

testfiles(
    myname='corruptedMDF2',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/corruptedMDF2/EmptyMDF.mdf"
    ],
    qualifiers={
        'Author': 'sponce',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-10-15',
        'Simulation': False,
        'DDDB': 'trunk',
        'CondDB': 'master',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'sim10/run3-ideal',
    },
    comment=
    "This MDF file is simply empty. It should fail when reading, and is used in a test checking this",
    test_file_db=test_file_db)

testfiles(
    myname='corruptedMDF3',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/corruptedMDF3/corruptedMDF.zstd.mdf"
    ],
    qualifiers={
        'Author': 'sponce',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-10-15',
        'Simulation': False,
        'DDDB': 'trunk',
        'CondDB': 'master',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'sim10/run3-ideal',
    },
    comment=
    "This MDF file used zstd but the file has been corrupted then. It should fail when reading, and is used by a test checking that",
    test_file_db=test_file_db)

testfiles(
    myname='hlt1-filtered-expected-2024-MB-PbPb',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/RTA/mdfs_processed_hlt1_PbPb_PbSMOG/hlt1_expected-2024-PbPb-minbias-xdigi.mdf'
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20231017-vc-md100',
        'DDDB': 'dddb-20231017',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'jonrob/all-pmts-active',
        'Date': '2024-07-12 00:00:00'
    },
    comment=('Official PbPb sample produced with HLT1_PbPb_PbPb sequence.'),
    test_file_db=test_file_db)

testfiles(
    myname='hlt1-filtered-expected-2024-MB-PbAr',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/RTA/mdfs_processed_hlt1_PbPb_PbSMOG/hlt1_expected-2024-PbAr-minbias-digi.mdf'
    ],
    qualifiers={
        'Author': 'Benjamin Audurier',
        'DataType': 'Upgrade',
        'Format': 'MDF',
        'Simulation': True,
        'CondDB': 'sim-20231017-vc-md100',
        'DDDB': 'dddb-20231017',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'jonrob/all-pmts-active',
        'Date': '2024-07-12 00:00:00'
    },
    comment=('hlt1-filtered simulation sample with PbAr collisions'),
    test_file_db=test_file_db)

testfiles(
    myname='FTZSClusterMonitoring',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/Run_0000251217_20221027-121010-887_SCEB15.mdf",
    ],
    qualifiers={
        'Author': 'jheuel',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-10-30',
        'Simulation': True,
        'DDDB': 'upgrade/dddb-20221004',
        'CondDB': 'upgrade/SFCAVERN_SF_20220922_142756_All-FixedOrder',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'sim10/run3-ideal',
    },
    comment="File used for zs_cluster_monitoring test in Rec",
    test_file_db=test_file_db)

testfiles(
    myname='FTZSLiteClusterMonitoring',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/Run_0000269732_20230710-180508-022_SAEB22.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/Run_0000270047_20230712-151724-788_SCEB13.mdf"
    ],
    qualifiers={
        'Author': 'hjage',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-11-22',
        'Simulation': True,
        'DDDB': 'upgrade/dddb-20221004',
        'CondDB': 'upgrade/SFCAVERN_SF_20230418-FixedOrder',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'sim10/run3-ideal',
    },
    comment=
    "File used for zs_lite_cluster_monitoring test in Rec. 2 files, with (269732) and without (270047) isolated bunch. First has only O(500) events, so both are processed with EvtMax = 1000",
    test_file_db=test_file_db)

testfiles(
    myname='FTNZSClusterMonitoring',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/Run_0000250946_20221026-063447-446_SCEB13.mdf",
    ],
    qualifiers={
        'Author': 'jheuel',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-10-30',
        'Simulation': True,
        'DDDB': 'upgrade/dddb-20221004',
        'CondDB': 'upgrade/SFCAVERN_SF_20220922_142756_All-FixedOrder',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'HEAD',
    },
    comment=
    "File used for nzs_cluster_monitoring, nzs_cluster_monitoring_from_digits and nzs_digit_monitoring tests in Rec",
    test_file_db=test_file_db)

testfiles(
    myname='TestXMLSummary',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00020330/0004/00020330_00042960_1.full.first5evts.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/LHCb/Collision12/FULL.DST/00020330/0004/00020330_00042960_1.full.dst",
    ],
    qualifiers={
        'Author': 'chaen',
        'Format': 'ROOT',
        'DataType': '2012',
        'Date': '2017-08-11',
        'Simulation': False,
        'DDDB': 'dddb-20200424-2',
        'CondDB': 'cond-20150409-1',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'HEAD',
    },
    comment="File used for test-XMLSummary test of LHCb",
    test_file_db=test_file_db)

testfiles(
    myname='FRErrorMonitoring',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/errordecoding/Run_0000266442_20230610-020315-669_SAEB22_0955.mdf",
    ],
    qualifiers={
        'Author': 'bleverin',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-07-19',
        'Simulation': True,
        'DDDB': 'upgrade/master',
        'CondDB': 'upgrade/master',
        'GeometryVersion': 'upgrade/master',
        'ConditionsVersion': 'master',
    },
    comment="File used for error_monitoring test of LHCb",
    test_file_db=test_file_db)

testfiles(
    myname='add_retina_clusters',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/IonPhysics/Simulations/SMOG2jpsi/digi/SMOG2jpsi_0-Extended.digi",
    ],
    qualifiers={
        'Author': 'bleverin',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-07-19',
        'Simulation': True,
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        'GeometryVersion': 'upgrade/master',
        'ConditionsVersion': 'master',
    },
    comment="File used for input_add_retina_clusters test of Moore",
    test_file_db=test_file_db)

testfiles(
    myname='excl_spruce_2022',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/2022_1_FULL_255620_00150001_0005.raw'
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-01-19',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for excl_spruce_2022 test of Moore",
    test_file_db=test_file_db)

testfiles(
    myname='pass_spruce_2022',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/2022_1_TURBO_255620_00150000_0003.raw'
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-01-19',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for pass_spruce_2022 test of Moore",
    test_file_db=test_file_db)

testfiles(
    myname='turcal_spruce_2022',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/2022_1_TURCAL_255620_00150013_0002.raw'
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-07-14',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for turcal_spruce_2022 test of Moore",
    test_file_db=test_file_db)

testfiles(
    myname='DaVinci_spruce_2022',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/Commissioning2022/Run255620/255620_00150000_0000.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/Commissioning2022/Run255620/255620_00150000_0001.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/Commissioning2022/Run255620/255620_00150000_0002.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/Commissioning2022/Run255620/255620_00150000_0003.raw",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/Commissioning2022/Run255620/255620_00150011_0001.raw",
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-01-19',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'AlignmentV14_2024_04_05',
    },
    comment="File used for davinci tupling tests",
    test_file_db=test_file_db)

testfiles(
    myname='excl_spruce_2023',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/COLLISION23_FULL_270356_00140003_0001.raw'
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-08-31',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for excl_spruce_2023 test of Moore",
    test_file_db=test_file_db)

testfiles(
    myname='excl_spruce_2023-2',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/COMMISSIONING23_78July_FULL_269370_00110015_0002.raw'
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-08-01',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for excl_spruce_2023 test of Moore",
    test_file_db=test_file_db)

testfiles(
    myname='spruce_all_lines_realtime_test_old_json',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Hlt1Hlt2filtered_MinBias_sprucing/hlt2_2or3bodytopo_realtime_newPacking.mdf'
    ],
    qualifiers={
        'Author': 'sesen',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-03-24',
        'Simulation': True,
        'DDDB': 'dddb-20171126',
        'CondDB': 'sim-20171127-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for spruce_all_lines_realtime_test_old_json test of Moore",
    test_file_db=test_file_db)

testfiles(
    myname='spruce_all_lines_realtime',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Hlt1Hlt2filtered_MinBias_sprucing/hlt2_2or3bodytopo_realtime_newPacking_newDst.mdf'
    ],
    qualifiers={
        'Author': 'sesen',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2022-09-16',
        'Simulation': True,
        'DDDB': 'dddb-20171126',
        'CondDB': 'sim-20171127-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for spruce_all_lines_realtime test of Moore",
    test_file_db=test_file_db)

testfiles(
    myname='spruce_bandwidth_input',
    filenames=[
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/EoY-spruce-2024/307781_00090010_0105.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/EoY-spruce-2024/307781_00090011_0074.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/EoY-spruce-2024/307781_00090012_0106.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/EoY-spruce-2024/307781_00090015_0004.raw',
        'mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/EoY-spruce-2024/307781_00090015_0073.raw',
    ],
    qualifiers={
        'Author': 'abertoli',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-10-14',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'master',
    },
    comment="File used for spruce bandwidth tests",
    test_file_db=test_file_db)

testfiles(
    myname='hlt2_bandwidth_input_2024',
    filenames=[
        # NOTE: there's lots more mdfs in this directory. Here should be 200-300k events
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/mdfs_hlt1filtered_307586/Run_0000307586_20241003-185948-171_VAEB04_1673.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/mdfs_hlt1filtered_307586/Run_0000307586_20241003-185252-769_SAEB10_1547.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/mdfs_hlt1filtered_307586/Run_0000307586_20241003-185309-473_VAEB08_1551.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/mdfs_hlt1filtered_307586/Run_0000307586_20241003-190040-374_SCEB07_1687.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/mdfs_hlt1filtered_307586/Run_0000307586_20241003-190515-414_ECEB03_1774.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/mdfs_hlt1filtered_307586/Run_0000307586_20241003-185646-237_SAEB18_1618.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/mdfs_hlt1filtered_307586/Run_0000307586_20241003-191432-329_R1EB18_1956.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/mdfs_hlt1filtered_307586/Run_0000307586_20241003-185838-398_SAEB09_1651.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/mdfs_hlt1filtered_307586/Run_0000307586_20241003-190451-142_R1EB08_1766.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/mdfs_hlt1filtered_307586/Run_0000307586_20241003-190552-675_SAEB07_1786.mdf",
    ],
    qualifiers={
        'Author': 'decianm',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-10-12',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'master',
    },
    comment="File used for spruce bandwidth tests",
    test_file_db=test_file_db)

testfiles(
    myname='hlt2_pp_realdata_with_UT',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/COLLISION24-run292798/Run_0000292798_20240501-033654-120_PLEB01_1873.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/COLLISION24-run292798/Run_0000292798_20240501-033655-964_UAEB04_1839.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/COLLISION24-run292798/Run_0000292798_20240501-033655-976_R1EB20_1848.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/COLLISION24-run292798/Run_0000292798_20240501-033656-078_MCEB04_1838.mdf",
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/rta/WP3/bandwidth_division/COLLISION24-run292798/Run_0000292798_20240501-033656-098_UCEB05_1857.mdf",
    ],
    qualifiers={
        'Author': 'decianm',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-05-01',
        'Simulation': False,
        'DDDB': 'run3/2024.Q1.2-v00.00',
        'CondDB': '',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'master',
    },
    comment="File used for hlt2_pp_realdata_with_UT's test in Moore",
    test_file_db=test_file_db)

testfiles(
    myname='hlt2_pp_2022_reprocessing_data_production_options',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/data/2022/RAW/PASSTHROUGH/LHCb/COLLISION22/256264/256264_00090018_0006.raw",
    ],
    qualifiers={
        'Author': 'rmatev',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2023-07-17',
        'Simulation': False,
        'DDDB': 'upgrade/master',
        'CondDB': 'upgrade/master',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for hlt2_pp_2022_reprocessing_data_production_options's test in Moore",
    test_file_db=test_file_db)

testfiles(
    myname='DaVinciTutorials',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/hlt2_passthrough_thor_lines.dst'
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-06-26',
        'Simulation': True,
        'DDDB': 'dddb-20180815',
        'CondDB': 'sim-20180530-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for DaVinci tutorials.",
    test_file_db=test_file_db)

testfiles(
    myname='DaVinciTutorialsHLT2MC',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/2024/DST/00266052/0000/00266052_00000020_1.hlt2.dst'
    ],
    qualifiers={
        'Author': 'masmith',
        'Format': 'ROOT',
        'DataType': '2024',
        'Date': '2025-02-07',
        'Simulation': True,
        'DDDB': 'dddb-20240427',
        'CondDB': 'sim10-2024.W40.42-v00.00-mu100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for DaVinci tutorials. HLT2 processed J/psiPhi 2024 MC",
    test_file_db=test_file_db)

testfiles(
    myname='Turbo_rd_data_with_lumi',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb//LHCb/Commissioning23/RD.DST/00195021/0000/00195021_00001377_1.rd.dst",
    ],
    qualifiers={
        'Author': '',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-08-12',
        'Simulation': False,
        'DDDB': 'run3/trunk',
        'CondDB': 'master',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for Turbo_rd_data_with_lumi's test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='Turbo_bandq_data_bad_sel_report',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/test_davinci_turbo_hlt_sel_reports.dst",
    ],
    qualifiers={
        'Author': 'masmith',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2024-07-30',
        'Simulation': False,
        'DDDB': 'run3/trunk',
        'CondDB': 'master',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for Turbo_bandq_data_bad_sel_report's test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='Spruce_with_lumi_all_lines_noEventsInSelection',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/lumi/test4/spruce_all_lines_production_1.rd.dst",
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-08-09',
        'Simulation': False,
        'DDDB': 'upgrade/dddb-20221004',
        'CondDB': 'md_VP_SciFi_macromicrosurvey_from20220923',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for Spruce_with_lumi_all_lines_noEventsInSelection and test_davinci_tupling_with_lumi_from_spruce_noEventsInSelection tests in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='Spruce_with_lumi_all_lines_moreRunsdst',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/lumi/test2/spruce_all_lines_production_moreRuns.b2oc.dst",
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-08-09',
        'Simulation': False,
        'DDDB': 'upgrade/dddb-20221004',
        'CondDB': 'md_VP_SciFi_macromicrosurvey_from20220923',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for Spruce_with_lumi_all_lines_moreRunsdst test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='Spruce_with_lumi_all_lines_moreDstRuns',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/lumi/test4/spruce_all_lines_production_1.b2oc.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/lumi/test4/spruce_all_lines_production_2.b2oc.dst",
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-08-14',
        'Simulation': False,
        'DDDB': 'upgrade/dddb-20221004',
        'CondDB': 'md_VP_SciFi_macromicrosurvey_from20220923',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for Spruce_with_lumi_all_lines_moreDstRuns test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='Spruce_with_lumi_all_lines_moreDstMerged',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/lumi/test5/spruce_all_lines_production_1.b2oc.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/lumi/test5/spruce_all_lines_production_2.b2oc.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/lumi/test5/b2oc_spruce_merged.dst",
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-08-08',
        'Simulation': False,
        'DDDB': 'upgrade/dddb-20221004',
        'CondDB': 'md_VP_SciFi_macromicrosurvey_from20220923',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for Spruce_with_lumi_all_lines_moreDstMerged test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='Spruce_with_lumi_all_lines_mergeDst',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/lumi/test5/b2oc_spruce_merged.dst",
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-08-08',
        'Simulation': False,
        'DDDB': 'upgrade/dddb-20221004',
        'CondDB': 'md_VP_SciFi_macromicrosurvey_from20220923',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for Spruce_with_lumi_all_lines_mergeDst in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='Spruce_with_lumi_all_lines_moreDst',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/lumi/test3/spruce_all_lines_production_1.b2oc.dst",
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-08-09',
        'Simulation': False,
        'DDDB': 'upgrade/dddb-20221004',
        'CondDB': 'md_VP_SciFi_macromicrosurvey_from20220923',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for test_davinci_tupling_with_lumi_from_spruce and test_davinci_tupling_from_spruce_data tests in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='Spruce_with_lumi_all_lines_evenmoreDst',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/lumi/test3/spruce_all_lines_production_1.b2oc.dst",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/lumi/test3/spruce_all_lines_production_2.b2oc.dst",
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-08-09',
        'Simulation': False,
        'DDDB': 'upgrade/dddb-20221004',
        'CondDB': 'md_VP_SciFi_macromicrosurvey_from20220923',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for test_davinci_tupling_with_lumi_from_spruce_moreDst tests in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='Spruce_passthrough_B2Dpi_line',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/spruce_passthrough_bd2dpi.dst",
    ],
    qualifiers={
        'Author': 'cprouve',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2024-04-30',
        'Simulation': True,
        'DDDB': 'dddb-20231017',
        'CondDB': 'sim-20231017-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'jonrob/all-pmts-active',
    },
    comment="File used for Spruce_passthrough_B2Dpi_line test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='HltTisTos_test',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/excl_2023_TISTOSexample.bandq.dst",
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2024-08-31',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for HltTisTos_test test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='DaVinci-issue-100',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/test_davinci-issue-100_multiple_bkgcat_mc-truth.dst"
    ],
    qualifiers={
        'Author': 'erodrigu',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-02-15',
        'Simulation': True,
        'DDDB': 'dddb-20210617',
        'CondDB': 'sim-20210617-vc-md100',
        'GeometryVersion': '',
        'ConditionsVersion': '',
    },
    comment=
    "File used for test_davinci-issue-100_multiple_bkgcat_mc-truth test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='2024_Sprucing24c2',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/Sprucing24c2_Run297156_TruboStreamOnline_charmDST_example.dst"
    ],
    qualifiers={
        'Author': 'erodrigu',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2024-07-17',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for test_davinci_tupling_from_spruced_turbo_with_refitting test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='FEST_November_2021',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/hlt2_D0_Kpi_100evts_newPacking_newDst.dst"
    ],
    qualifiers={
        'Author': 'sesen',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2022-08-10',
        'Simulation': True,
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for a number of tests in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='Gauss_12143001_xgen',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/Gauss-12143001-100ev-20211117.xgen"
    ],
    qualifiers={
        'Author': 'dfazzini',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2021-11-17',
        'Simulation': True,
        'DDDB': 'dddb-20171126',
        'CondDB': 'sim-20171127-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for test_davinci_tupling_from_xgen and test_read_mc_xgen tests in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='Spruce_all_lines_dst',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/spruce_all_lines_realtimereco_newPacking_newDst.dst"
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-06-21',
        'Simulation': True,
        'DDDB': 'dddb-20171126',
        'CondDB': 'sim-20171127-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for several DaVinci tests",
    test_file_db=test_file_db)

testfiles(
    myname='upgrade_LbToLcmunu',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/Upgrade_LbToLcmunu.dst"
    ],
    qualifiers={
        'Author': 'amathad',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2022-07-29',
        'Simulation': True,
        'DDDB': 'dddb-20210617',
        'CondDB': 'sim-20210617-vc-mu100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for a few tests in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='hlt2_b2ksttaumu_opt',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/b_to_ksttaumu_test_isolation.dst"
    ],
    qualifiers={
        'Author': 'erodrigu',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-06-30',
        'Simulation': True,
        'DDDB': 'dddb-20190223',
        'CondDB': 'sim-20180530-vc-mu100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for test_davinci_tupling_relation_isovariables test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='hlt1_trigger_decisions',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/hlt2_integration_B0_100.dst"
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2022-05-09',
        'Simulation': True,
        'DDDB': 'dddb-20201211',
        'CondDB': 'sim-20201218-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for a number of tests in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='passthrough_thor_lines',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/logs/spruce_bsjpsiphi_passthrough_with_pv_pointer..dst"
    ],
    qualifiers={
        'Author': 'sesen',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2024-02-26',
        'Simulation': True,
        'DDDB': 'dddb-20180815',
        'CondDB': 'sim-20180530-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for a number of tests in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='spruce_MCTools',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/spruce_realtimereco_dstinput.dst"
    ],
    qualifiers={
        'Author': 'nskidmor',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-06-21',
        'Simulation': True,
        'DDDB': 'dddb-20171126',
        'CondDB': 'sim-20171127-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment="File used for a number of tests in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='spruced_turbo',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/00195021_00002541_1.qee.dst"
    ],
    qualifiers={
        'Author': 'pkoppenb',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-09-05',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'master',
    },
    comment=
    "File used for test_davinci_tupling_from_spruced_turbo test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='tistos_advanced',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/moore_bs2phiphi_Hlt2filter.dst"
    ],
    qualifiers={
        'Author': 'amathad',
        'Format': 'ROOT',
        'DataType': 'Upgrade',
        'Date': '2023-01-11',
        'Simulation': True,
        'DDDB': 'dddb-20210617',
        'CondDB': 'sim-20210617-vc-mu100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'AlignmentV14_2024_04_05',
    },
    comment=
    "File used for test_davinci_tupling_tistos_advanced test in DaVinci",
    test_file_db=test_file_db)

testfiles(
    myname='mdf_file_with_large_bank',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/MDFLargeBank/bigbanks.mdf",
    ],
    qualifiers={
        'Author': 'sponce',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-12-09',
        'Simulation': True,
        'DDDB': 'dddb-20210617',
        'CondDB': 'sim-20210617-vc-mu100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'AlignmentV14_2024_04_05',
    },
    comment="File used for test_large_banks test in LHCb",
    test_file_db=test_file_db)

testfiles(
    myname='expected_2024_minbias_digi_nominal_beam_line',
    filenames=[
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_minbias_digi_nominal_beam_line/00217353_00000034_1.digi',
        'root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/expected_2024_minbias_digi_nominal_beam_line/00217353_00000007_1.digi'
    ],
    qualifiers={
        'Author': 'Bogdan Kutsenko',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2024-10-24',
        'Simulation': True,
        "GeometryVersion": "run3/trunk",
        "ConditionsVersion": "master",
        "DDDB": "dddb-20231017",
        "CondDB": "sim10-2024-v1.0-vc0mm-mu100",
    },
    comment='\n'.join([
        "MinBias (30000000) DIGI based on expected 2024 conditions with beam position centered (0,0,z). Input for tests and LHCbPR jobs.",
        "Request: https://gitlab.cern.ch/lhcb-rta/mc-requests/-/issues/21",
        "https://gitlab.cern.ch/lhcb-simulation/mc-requests/-/merge_requests/504",
        "ProductionID: 128469",
        "BKK: MC/Dev/Beam6800GeV-expected-2024-NominalBeamLine-MagUp-Nu7.6-25ns-Pythia8///Sim10c/30000000/DIGI"
    ]),
    test_file_db=test_file_db)

testfiles(
    myname='ut-nzs-2024',
    filenames=[
        "mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/ut-nzs-2024/Run_0000306325_20240917-142102-310_UAEB01.mdf",
    ],
    qualifiers={
        'Author': 'wokrupa',
        'Format': 'MDF',
        'DataType': 'Upgrade',
        'Date': '2024-12-03',
        'Simulation': False,
        'DDDB': '',
        'CondDB': '',
        'GeometryVersion': 'run3/2024.Q1.2-v00.00',
        'ConditionsVersion': 'master',
    },
    comment="File used for test nzs decoders for the UT",
    test_file_db=test_file_db)

testfiles(
    myname='LHCbIntegrationTests_FT_tupling_HLT1',
    filenames=[
        "root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/MC/Upgrade/DIGI/00205444/0000/00205444_00000016_1.digi",
    ],
    qualifiers={
        'Author': 'msaur',
        'Format': 'DIGI',
        'DataType': 'Upgrade',
        'Date': '2025-02-04',
        'Simulation': True,
        'DDDB': 'dddb-20231017',
        'CondDB': 'sim-20231017-vc-md100',
        'GeometryVersion': 'run3/trunk',
        'ConditionsVersion': 'jonrob/all-pmts-active',
    },
    comment="File for FT tupling LHCbIntegrationTest, BdToD0Pip sample",
    test_file_db=test_file_db)
