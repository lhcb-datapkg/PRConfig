###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Module that defines the testfiles object, a smart tuple, basically.

For more details on how to use this class, see TestFileDB documentation
"""
from __future__ import print_function
#global test_file_db
#if "test_file_db" not in globals():
#    test_file_db={}

import copy
import os


class testfiles(object):
    """A smart tuple ensuring certain details are always added to the database
    """

    __known_qualifiers__ = [
        "Processing", "Stripping", "Reconstruction", "Simulation", "DDDB",
        "CondDB", "GeometryVersion", "ConditionsVersion", "DataType", "Format",
        "Date", "Author", "Application", "QMTests", "InitialTime", "Turbo",
        "RootInTES", "DataType"
    ]

    __required_qualifiers__ = ["Format", "DataType", "Date"]

    def __init__(self, myname, filenames, qualifiers, comment, test_file_db):
        """
        myname: logical, lexographical, human-readable name of this dataset
        filenames: list of filenames in PFN or LFN form
        qualifiers: logical attributes of this file, see known_qualifiers for details, must be a dictionary with at least some certain entries
        comment: specify a comment string to attribute to this file
        """
        if type(qualifiers) is not dict:
            raise TypeError("Qualifiers must be of type dict")
        for key in qualifiers:
            if key not in self.__known_qualifiers__:
                raise KeyError("You've added an unknown qualifier, " +
                               str(key))
        missed = [
            r for r in self.__required_qualifiers__ if r not in qualifiers
        ]
        if len(missed):
            raise KeyError("You must at least supply a the qualifiers " +
                           ','.join(missed))
        if type(myname) is not str or (
                type(filenames) is not str
                and type(filenames) is not list) or type(comment) is not str:
            raise TypeError("Please use strings for filenames and comments")
        #set my properties
        self.myname = myname
        self.filenames = filenames
        self.qualifiers = copy.deepcopy(qualifiers)
        self.comment = comment
        #automatically add to test file DB
        test_file_db[myname] = self

    def __str__(self):
        return '\n'.join([
            ' '.join([
                self.myname, self.qualifiers["Date"],
                self.qualifiers["Format"], self.qualifiers["DataType"]
            ]), "", "\t" + self.filenames.__str__().replace(",", ",\n\t "), "",
            "\t\"" + self.comment + "\"", "", "\t" + self.qualifiers.__str__()
        ])

    def recreateme(self):
        """
        I can make myself into a string that you can exec() to recreate me as an object, or append me to a file
        """
        return "testfiles(\n\tmyname='" + self.myname + "', \n\tfilenames=" + self.filenames.__str__(
        ) + ", \n\tqualifiers=" + self.qualifiers.__str__(
        ) + ", \n\tcomment='" + self.comment + "', \n\ttest_file_db=test_file_db\n\t)"

    def useme(self):
        """
        I can make myself into a string for adding to your nightly tests
        """
        return "from PRConfig import TestFileDB; TestFileDB.test_file_db['" + self.myname + "'].run(); #set options for conddb/configurable yourself if needed. Don't forget to add use PRConfig v* to your requirements"

    def run(self, withDB=None, configurable=None, clear=False):
        """Add myself to the input data
        withDB: configure DDDB and CondDB? None: set if not already set. True, force setting. False, do not set.
        configurable: where to set DataType and Database Flags? Default LHCbApp.
        """
        from GaudiConf import IOExtension
        iox = IOExtension()
        fn = self.filenames
        if type(fn) is not list:
            fn = [fn]
        iox.inputFiles(fn, clear=clear)

        self.setqualifiers(withDB=withDB, configurable=configurable)

    def setqualifiers(self, withDB=None, configurable=None):
        if configurable is None:
            from Configurables import LHCbApp
            configurable = LHCbApp()
        configurable.DataType = str(self.qualifiers["DataType"])
        if "Simulation" in self.qualifiers:
            if type(self.qualifiers["Simulation"]) is str:
                configurable.Simulation = ("true" in (
                    self.qualifiers["Simulation"]).lower())
            else:
                configurable.Simulation = self.qualifiers["Simulation"]

        for qfy in ["Turbo", "RootInTES", "DataType"]:
            if qfy in self.qualifiers:
                try:
                    setattr(configurable, qfy, self.qualifiers[qfy])
                except:
                    print("Cannot set qualifier " + qfy + ":" +
                          str(self.qualifiers[qfy]))

        #now skipped if withDB set to False
        if withDB is None or withDB:
            if "DDDB" in self.qualifiers:
                if withDB is None and configurable.isPropertySet("DDDBtag"):
                    #warn and do nothing
                    print(
                        "# WARNING: run() will not reset the DDDB tag, because it is already set, set withDB=True(force) or False(skip) to suppress this warning"
                    )
                else:
                    #set
                    configurable.DDDBtag = self.qualifiers["DDDB"]

            if "CondDB" in self.qualifiers:
                if withDB is None and configurable.isPropertySet("CondDBtag"):
                    #warn and do nothing
                    print(
                        "# WARNING: run() will not reset the CondDB tag, because it is already set, set withDB=True(force) or False(skip) to suppress this warning"
                    )
                else:
                    #set
                    configurable.CondDBtag = self.qualifiers["CondDB"]

            try:
                from DDDB.CheckDD4Hep import UseDD4Hep
            except ImportError:
                UseDD4Hep = False
            if not UseDD4Hep and "InitialTime" in self.qualifiers:
                from Configurables import EventClockSvc
                if withDB is None and EventClockSvc().isPropertySet(
                        "InitialTime"):
                    #warn and do nothing
                    print(
                        "# WARNING: run() will not reset the InitialTime, because it is already set, set withDB=True(force) or False(skip) to suppress this warning"
                    )
                else:
                    #set
                    raw = self.qualifiers["InitialTime"]
                    try:
                        initialTime = int(raw)
                    except ValueError:
                        try:
                            import dateutil.parser
                            dt = dateutil.parser.parse(raw)
                        except Exception as e:
                            raise ValueError(
                                "InitialTime '{}' is not int and could not be parsed by dateutil.parser.parse():\n{}"
                                .format(raw, e))
                        if not dt.tzinfo:
                            raise ValueError(
                                "InitialTime must have timezone info, e.g. '2011-07-16 21:46:39 CET'"
                            )
                        import calendar
                        initialTime = calendar.timegm(
                            dt.utctimetuple()) * 1000000000
                    EventClockSvc().InitialTime = initialTime

    def make_lbexec_options(self, **kwargs):
        # Protect these imports as they require Python 3+
        from importlib import import_module
        from GaudiConf.LbExec import Options as DefaultOptions

        try:
            app = import_module(os.environ.get("GAUDIAPPNAME"))
        except ModuleNotFoundError:
            Options = DefaultOptions
        else:
            Options = getattr(app, "Options", DefaultOptions)

        options = {
            "input_files": self.filenames,
            "input_type":
            "ROOT" if self.qualifiers["Format"] != "MDF" else "RAW",
        }
        if "DataType" in self.qualifiers:
            options["data_type"] = self.qualifiers["DataType"]
        if "Simulation" in self.qualifiers:
            options["simulation"] = self.qualifiers["Simulation"]
        if "DDDB" in self.qualifiers:
            options["dddb_tag"] = self.qualifiers["DDDB"]
        if "CondDB" in self.qualifiers:
            options["conddb_tag"] = self.qualifiers["CondDB"]
        if "GeometryVersion" in self.qualifiers:
            options["geometry_version"] = self.qualifiers["GeometryVersion"]
        if "ConditionsVersion" in self.qualifiers:
            options["conditions_version"] = self.qualifiers[
                "ConditionsVersion"]
        options.update(kwargs)

        return Options.parse_obj(options)
