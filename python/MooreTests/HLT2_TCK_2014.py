###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
MooreOptions = dict(
    Simulation=False,
    DataType="2012",
    DDDBtag="dddb-20120831",
    CondDBtag="cond-20120831",
    RemoveInputHltRawBanks=True,
    UseTCK=True,
    InitialTCK="0x0a030046",
    HltLevel='Hlt1Hlt2')


def configure():
    pass
