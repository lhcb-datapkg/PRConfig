###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pandas
import re
from io import StringIO, open
from collections import OrderedDict
"""
Define the algorithm names that are grouped under different categories.
The algorithms are arranged as a list of (possible regular) expressions
This is an OrderedDict so that if an algorithm matches more than one group
it is added to the first one
"""
algGroups = {
    "HLT1":
    OrderedDict([
        ("Framework", [
            "LHCb__MDF__IOAlg",
            "reserveIOV",
            "DummyEventTime",
        ]),
        ("VeloCluster", [
            "VeloClusterTrackingSIMD",
            "TrackBeamLineVertexFinderSoA",
        ]),
        ("GEC", ["PrGECFilter"]),
        ("VeloUT", [
            "PrStorePrUTHits",
            "PrVeloUT",
        ]),
        ("Forward", [
            "FTRawBankDecoder",
            "SciFiTrackForwarding.*",
        ]),
        ("VeloKalman", ["VeloKalman"]),
        ("Muon", [".*Muon.*"]),
    ]),
    "HLT2":
    OrderedDict([
        ("Framework", [
            "LHCb__MDF__IOAlg",
            "reserveIOV",
            "createODIN",
            "DummyEventTime",
            ".*UnpackRawEvent.*",
            "ReserveIOVDD4hep",
            "EventOutput_.*",
            "^Online__RawEventToBanks_.*",
            "Online__BanksToRawEvent_.*",
            "Online__InputAlg",
        ]),
        ("Velo", [
            "Velo(Retina)?ClusterTrackingSIMD",
            "VeloKalman",
        ]),
        ("Forward", [
            "PrForwardTrackingVelo",
            "PrForwardTracking",
            "PrResidualVeloTracks",
            "SciFiTrackForwarding",
        ]),
        ("PVs", [
            "TrackBeamLineVertexFinderSoA",
            "PatPV3DFuture.*",
        ]),
        ("StoreHits", [
            "PrStoreSciFiHits",
            "PrStoreUTHit",
            "PrStorePrUTHits",
            "PrResidualPrUTHits",
            "PrResidualSciFiHits",
            "MuonRawToHits",
            "MuonRawInUpgradeToHits",
            "FTRawBankDecoder",
            "PrStoreFTHit",
            "VPClus",
            "SciFiTrackForwardingStoreHit",
        ]),
        ("Seeding", ["PrHybridSeeding"]),
        ("Match", ["PrMatchNN"]),
        ("Downstream", [
            "PrLongLivedTracking",
            "PrResidualSeeding",
        ]),
        ("Upstream", [
            "PrVeloUT",
        ]),
        (
            "TrackFit",
            [
                "TrackBestTrackCreator.*",  # baseline
                "PrKalmanFilter.*",
                "TBTC.*",
                "CloneKiller(Forward|Match)_.*",
                "PrCloneKiller.*",  # fastest
                "TrackEventFitter.*",
                "TrackCloneKiller.*",  # light
                "CreateBestTrackContainer",
                "Persistable.*TracksContainer.*",
            ]),
        ("Calorimeter", [
            "^(?!.*Packer)Calo.*",
            "FutureEcalZSup",
            "FutureCellularAutomatonAlg",
            "FutureHcalZSup",
            "ClassifyPhotonElectronAlg",
            "GraphClustering",
        ]),
        ("RICH", ["^(?!.*Packer).*Rich.*"]),
        ("Muon", [
            "MuonIDHlt2.*",
            "StandaloneMuonRec",
        ]),
        ("Converters", [
            ".*?[c,C]onverter.*",
            "TracksToSelection.*",
            "TrackSelectionToContainer",
            "fromPrSeedingTracksV1Tracks",
            ".*Merger.*",
            "MuonPIDV2ToMuonTracks.*",
            "fromV2MuonPIDV1MuonPID.*",
            "PrVPHitsToVPLightClusters.*",
        ]),  #matches either Converter or converter
        ("Protoparticles", [
            ".*ProtoParticleMaker.*",
            "FutureNeutralProtoPAlg",
        ]),
        ("Particles", [
            "FunctionalParticleMaker",
            "LHCb__Phys__ParticleMakers__PhotonMaker",
            "ParticleWithBremMaker",
            "LHCb__Phys__ParticleMakers__MergedPi0Maker",
            "UniqueIDGeneratorAlg",
            "ParticleMakerForParticleFlow",
            "^DiElectronMaker.*",
        ]),
        (
            "Selection",
            [
                "require_pvs",
                "DeterministicPrescaler",
                "ParticleRangeFilter",
                ".*[c,C]ombiner.*",
                ".*[b,B]uilder.*",
                "FilterDesktop",
                ".*(?!Di)ElectronMaker.*",
                ".*MuonMaker.*",
                "GetFlavourTaggingParticles",
                "Hlt2Ks0ToPiPiLLMonitoringLineKsLLMonitor",
                "Hlt2L0ToPPiLLMonitoringLineLambdaLLMonitor",
                "JpsiMaker_ChicJpsiDiMuonLine",
                "make_chic2jpsimumu",
                "onia_prompt_pions",
                "^SelectionFromWeightedRelationTable_.*",
                "WeightedRelTableAlg.*",
                "^Charm.*",
                "^.*B2CC_.*",
                "^.*B2OC.*",
                "^.*BandQ.*",
                "^BNOC.*",
                "^Hlt2BnoC.*",
                "^Hlt2Charm.*",
                "^Hlt2Calib.*",
                ".*Hlt2RD.*",
                "^Hlt2IFT.*",
                "^.*_Prescaler",
                "^.*_Hlt1Filter",
                "^B2KsPi.*",
                "^bandq_.*",
                ".*[M,m]ajo.*",
                "^SLB.*",
                "^Monitoring_Hlt1KsLine_.*",
                "^beauty_to.*",
                "^charmonium_.*",
                "^detached_.*",
                "^dilepton_.*",
                "^make_bhadron_.*",
                "^make_rd_.*",
                "^make_std_.*",
                "^std_make.*",
                "^prompt_dielectron_.*",
                "^rd_.*",
                "dihadron_from_pk",
                "^filter_RD_.*",
                "^jpsi.*",
                "^B_from_.*",
                "^BsTo.*",
                "^CBaryonToPh_.*",
                "^CMesonToSl_.*",
                "^D02.*",
                "DY2mumu_.*",
                "EW_.*",
                "Femtoscopy_.*",
                "Omegac02pkkpi",
                "^MyVELOProbeFilter.*",
                "ThOrParticleSelection.*",
                "SimpleJets_.*",
                ".*mva_maker_.*",
                "make_inclusive_cut.*",
                "K.*0 -> K.*",
                ".*TrackEff.*",
                "Ups2mumu.*",
                ".*Com?(m)issioning.*",
                "HltDecReportsFilter.*",
                "MuonProbeToLongMatcher.*",
                "pid_.*",
                "qee_.*",
                "smog2_.*",
                "xibm_lll",
                "xim_ddd",
                "xim_ddl",
                "xim_lll",
                "xim_to_lambda_pim_lll",
                "nHits_BDT_filter",
                "obm_lll",
                "omega_to_xiee_lll",
                "omega_to_xigamma_lll",
                "omega_to_ximumu_lll",
                "omega_to_xipipi_lll",
                "omegam_ddd",
                "omegam_ddl",
                "omegam_lll",
                "Xic02pkkpi",
                "Xicp2pkpi",
                "bachelor_.*",
                "cc2kkkk",
                "cc2kkpipi",
                "etac2ppbar",
                "combine_omegac_pk",
                "cc2pipipipi",
                "chic2jpsigamma.*",
                "DiElectron_IntermediateNeutral_Filter.*",
                ".*ForStrangeDecays_.*",
                "Dpm2kpipi",
                "Ds2kkpi",
                "Hlt1.*Filter",
                "Hlt1TisTosAlg_.*",
                "Jpsi2mumu.*",
                "KS_combinations.*",
                "Kstp_Maker_resolved_Pi0s.*",
                "Lc2pkpi",
                "LcpToL0PimPipPip_.*",
                "LowDiMuon_.*",
                "topo_2body.*",
                "PID_B.*",
                "SL_(KS0|Lambda)_.*",
                "TagsSimpleJets.*",
                "TightProtons_.*",
                "Topo_has_rich_long_pion_.*",
                "Topo_unfiltered_.*",
                "TtracksMVAFilter.*",
                "TtrackSel_Track.*",
                "psi2s.*",
                "generic_(2|3)body_smog2",
                "make_B2.*",
                "filter_(Ttrack|long|qee)_.*",
                "filt_PID_.*",
                "make_EMDM_.*",
                "make_Lb2JpsiLambdaTT",
                "make_qee_diphotons.*",
                "pi",  # that's the worst name of them all
                "onia_prompt_kaons.*",
                "make_Bd2JpsiKsTT",
                "inclusive_dilepton_detached_dimuon.*",
                "B_(minus|plus).*Hlt2SLB.*",
                "BdsTo.*",
                "RD_bbaryon.*",
            ]),
        ("PersistenceCloning", [
            "CopyLinePersistenceLocations",
            "CopyParticle2PVRelationsFromLinePersistenceLocations",
            "CopyParticlesWithPVRelations",
            "CopyProtoParticle2MCRelations",
            "CopySignalMCParticles",
            "^CopyParticles_.*",
        ]),
        ("PersistenceWriting", [
            "ExecutionReportsWriter",
            "HltDecReportsWriter",
            "HltPackedBufferWriter",
            "Hlt__RoutingBitsWriter",
            "SelectiveCombineRawBanks.*",
        ]),
        ("PersistencePacking", [
            ".*Packer.*",
            "PackMC.*",
        ]),
        ("Monitoring", [
            "^.*Monitor.*",
        ])
    ]),
    "Sprucing":
    OrderedDict([
        ("Framework", [
            "LHCb__MDF__IOAlg",
            "reserveIOV",
            "ReserveIOVDD4hep",
            "createODIN",
            "DummyEventTime",
            "LHCb__UnpackRawEvent.*",
            "Fetch__Event_DAQ_RawEvent.*",
            ".*CombineRawBanks.*",
            "Decode_ODIN",
            "AddressKillerAlg.*",
            "LumiCounter",
            "SelectHlt1HltDecReports",
            "SelectHlt1HltLumiSummary",
            "SelectHlt1HltSelReports",
            "SelectHlt2HltDecReports",
            "SelectHlt2HltLumiSummary",
            "SelectHlt2HltRoutingBits",
        ]),
        ("Converters", [
            ".*?[c,C]onverter.*",
            "TracksToSelection.*",
            "TrackSelectionToContainer",
            "fromPrSeedingTracksV1Tracks",
        ]),
        ("Selection (mergers)", [
            ".*Merger.*",
            "Topo_.*_merger",
        ]),
        ("Particles", [
            "FunctionalParticleMaker",
            "FunctionalDiElectronMaker_.*",
            "LHCb__Phys__ParticleMakers__PhotonMaker",
            "ParticleWithBremMaker",
            "LHCb__Phys__ParticleMakers__MergedPi0Maker",
            "UniqueIDGeneratorAlg",
            "ParticleMakerForParticleFlow",
            "PFTracksOnly_.*",
            "Functional.*Tagger_.*",
            "WeightedRelTableAlg_.*",
            "Topo_has_rich_long_pion_.*",
            "topo_2body",
            "topo_2body_with_svtag",
            "SimpleJets_.*",
            "TagsSimpleJets_.*",
            "PF.*",
        ]),
        ("Persistence (unpacking)", [
            "MuonPIDUnpacker",
            "P2VRelationUnpacker",
            "ParticleUnpacker",
            "ProtoParticleUnpacker",
            "RecVertexUnpacker",
            "TrackUnpacker",
            "UnpackMCParticle",
            "UnpackMCVertex",
            "VertexUnpacker",
            "CaloHypoUnpacker",
            "RichPIDUnpacker",
            "RecSummaryUnpacker",
            "UnpackRawEvent_.*",
            "^Unpack_Hlt2__Event_HLT2_.*",
        ]),
        ("Decoders", [
            "PackedBufferDecoder.*",
            "Hlt1DecReportsDecoder.*",
            "Hlt2DecReportsDecoder.*",
        ]),
        ("Selection (filters)", [
            "DefaultGECFilter",
            "FilterDstDataSize_Hlt2",
            "require_pvs",
            ".*_Hlt2Filter$",
            "Hlt2_prefilter",
            "^filter_RD_.*",
            "HighPtIsolatedMuonMaker_.*",
            ".*Filter.*",
            "LumiFilter",
            "PhysFilter",
        ]),
        ("Selection (scalers)", [
            "DeterministicPrescaler",
            ".*_Prescaler$",
            ".*_Postscaler$",
        ]),
        (
            "Selection (combiners)",
            [
                ".*[c,C]ombiner.*",
                ".*[b,B]uilder.*",
                #  ".*Maker.*",
                "GetFlavourTaggingParticles",
                "JpsiMaker_ChicJpsiDiMuonLine",
                "make_chic2jpsimumu",
                "(?i).*(?!Monitor).*B2CC.*(?<!Hlt2Filter)(?<!Prescaler)(?<!Postscaler)(?<!Combiner)(?<!Filter).*",
                "(?i).*(?!Monitor).*B2OC.*(?<!Hlt2Filter)(?<!Prescaler)(?<!Postscaler)(?<!Combiner)(?<!Merger)(?<!Builder).*",
                "(?i).*(?!Monitor).*BandQ.*(?<!Hlt2Filter)(?<!Prescaler)(?<!Postscaler).*",
                "(?i).*(?!Monitor).*BnoC.*(?<!Hlt2Filter)(?<!Prescaler)(?<!Postscaler).*",
                "(?i).*(?!Monitor).*Charm.*(?<!Hlt2Filter)(?<!Prescaler)(?<!Postscaler).*",
                "(?i).*(?!Monitor).*QEE.*(?<!Hlt2Filter)(?<!Prescaler)(?<!Postscaler).*",
                "(?i).*(?!Monitor).*RD.*(?<!Hlt2Filter)(?<!Prescaler)(?<!Postscaler).*",
                "(?i).*(?!Monitor).*SL.*(?<!Hlt2Filter)(?<!Prescaler)(?<!Postscaler).*",
                "(?i).*(?!Monitor).*EW.*(?<!Hlt2Filter)(?<!Prescaler)(?<!Postscaler).*",
                "(?i).*(?!Monitor).*SMOG2.*(?<!Hlt2Filter)(?<!Prescaler)(?<!Postscaler).*",
                "(?i).*(?!Monitor).*IFT.*(?<!Hlt2Filter)(?<!Prescaler)(?<!Postscaler).*",
                ".*[M,m]ajo.*",
                "^Monitoring_Hlt1KsLine_.*",
                "^beauty_to.*",
                "^charmonium_.*",
                "^detached_.*",
                "^dilepton_.*",
                "^make_B0.*",
                "^make_Bc.*",
                "^make_Beauty.*",
                "^make_Bm.*",
                "^make_Bp.*",
                "^make_Bs.*",
                "^make_Jpsi.*",
                "^make_Lb.*",
                "^make_Omb.*",
                "^make_Tbc.*",
                "^make_V0.*",
                "^make_Xib.*",
                "^make_psi.*",
                "^make_bbaryon_4body_.*",
                "^make_phi2kk_.*",
                "^make_bhadron_.*",
                "^prompt_dielectron_.*",
                "make_detached_.*",
                "^std_make_",
                ".*Maker.*",
                "DY2mumu_.*",
                "D02kpi.*",
                "Dpm2kpipi.*",
                "Ds2kkpi.*",
                "dihadron_from_pk",
                "(?i)^jpsi.*",
                "LowDiMuon.*",
                "Ups2mumu.*",
                "cc2kkkk.*",
                "cc2kkpipi.*",
                "cc2pipipipi.*",
                ".*for_b2lllnu.*",
                "Lc2pkpi.*",
                "TightProtons.*",
                "ThOrParticleSelection_.*",
                "^onia_prompt.*",
                "^psi2s_.*",
                "^K.*_topo_2body$",
                "^K.*_topo_2body_SVtag$",
                "SelectionFromWeightedRelationTable_.*",
                "^EvtSize_*",
                "AdvancedCloneKiller",
            ]),
        ("Streaming", ["Streaming_filter"]),
        ("Persistence (cloning)", [
            "CopyLinePersistenceLocations",
            "CopyParticle2PVRelationsFromLinePersistenceLocations",
            "CopyParticlesWithPVRelations",
            "CopyParticles",
            "CopyProtoParticle2MCRelations",
            "CopySignalMCParticles",
            "CopyFlavourTagsWithParticlePointers_.*",
        ]),
        ("Persistence (writing)", [
            "ExecutionReportsWriter",
            "HltDecReportsWriter",
            "HltPackedBufferWriter",
            "Hlt__RoutingBitsWriter",
            "VoidFilter_.*",
            "OutputStream.*",
        ]),
        ("Persistence (packing)", [
            ".*Packer.*",
            "PackMC.*",
            "PackedBufferDecoder_Hlt2",
        ]),
        ("Monitoring", [
            ".*Monitor_.*",
            "SPRUCEDecReportsMonitor",
            "SPRUCEPostscaleDecReportsMonitor",
            "SPRUCEPrescaleDecReportsMonitor",
        ])
    ])
}


def parseTimingTable(log_fn):

    TIMING_TABLE_RE = re.compile(
        r"\n"
        r"(HLTControlFlowMgr|INFO) +(HLTControlFlowMgr|INFO) +(Timing table:)\n"
        r"(HLTControlFlowMgr|INFO) +(HLTControlFlowMgr|INFO) +\n"
        r"(?P<table>(?: \|.*\|\n)+)")

    with open(log_fn, encoding='utf-8') as f:
        m = TIMING_TABLE_RE.search(f.read())
        if not m:
            raise RuntimeError("Timing table not found in {}".format(log_fn))
    table_str = m.group("table")
    df = pandas.read_csv(StringIO(table_str), sep=r"\s*\|\s*", engine="python")
    df = df.drop(columns=["Unnamed: 0", "Unnamed: 5"])
    df.columns = ["name", "count", "total_time", "average_time"]
    df.set_index("name")
    df.drop(index=0, inplace=True)  # drop the "Sum of all Algorithms" line
    df.name = df.name.apply(lambda x: x.strip('"'))
    return df


def parseAllTimingTables(listOfLogs):
    dfs = [parseTimingTable(path) for path in listOfLogs]
    df = pandas.concat(dfs).groupby('name').sum()
    #return a simple map
    vals = {
        name: cols["total_time"]
        for name, cols in df.to_dict('index').items()
    }
    return vals


def checkTimingVals(timeVals, grouping):
    """
    Returns the list of algorithms that are ran and not included in any grouping, and the list of algorithms that are expected but not found.
    """
    #Initialisation
    numberOfRegExpTriggered = {}
    for algName in timeVals:
        numberOfRegExpTriggered[algName] = 0
    numberOfTimesTriggered = {}
    for group, exprs in grouping.items():
        numberOfTimesTriggered[group] = {}
        for expr in exprs:
            numberOfTimesTriggered[group][expr] = 0
    #Scanning the timing values
    for algName in timeVals:
        for group, exprs in grouping.items():
            for expr in exprs:
                if re.match(expr, algName):
                    numberOfTimesTriggered[group][expr] += 1
                    numberOfRegExpTriggered[algName] += 1
    #Checks for algorithm names that match to no expression
    notInGrouping = []
    #Checks for algorithm names that match more than one expression (bad)
    triggersMore = []
    for algName in timeVals:
        if numberOfRegExpTriggered[algName] == 0:
            notInGrouping.append(algName)
        elif numberOfRegExpTriggered[algName] > 1:
            triggersMore.append(algName)
    #Checks for expressions that did not match anything
    notInTiming = []
    for group, exprs in numberOfTimesTriggered.items():
        for expr, val in exprs.items():
            if val == 0:
                notInTiming.append(expr)

    if len(notInGrouping) != 0:
        print(
            "WARNING: Following algorithms are in the timing table but are not associated with any group of algorithms"
        )
        for a in notInGrouping:
            print("\t", a)
    if len(notInTiming) != 0:
        print(
            "WARNING: Following algorithms are expected but are not found in the timing table"
        )
        for a in notInTiming:
            print("\t", a)
    if (len(triggersMore) != 0):
        print(
            "ERROR: Following expressions trigger more than one regular expression"
        )
        for a in triggersMore:
            print("\t", a)


def groupValues(groups, timeVals, produceYAML):
    """
    Gathers timing values in easy-to-understand groups.
    Also checks for algorithms that are not included in any grouping or some expected algorithms that are not present,
    and collects those under a 'Other' category (this category should ideally be empty).

    Optionally prints out a YAML file with the detailed per-category list of algorithms and tools
    that make each group.
    """
    # Checks which values are not in any grouping
    checkTimingVals(timeVals, groups)
    #TOTAL:
    groupedTimings = {}
    groupedTimings["Total"] = sum(timeVals.values())

    #Read them
    def find_group(k):
        for name, exprs in groups.items():
            if any(re.match(expr, k) for expr in exprs):
                return name
        return "Other"

    for name in groups:
        groupedTimings[name] = 0
    groupedTimings["Other"] = 0

    # Dictionary to collect details in the YAML
    content = {}
    for k, v in timeVals.items():
        g = find_group(k)
        groupedTimings[g] += v
        if g in content.keys():
            content[g].append(k)
        else:
            content[g] = [k]

    for k, v in groupedTimings.items():
        if k != "Total":
            groupedTimings[k] /= groupedTimings["Total"] / 100.

    if produceYAML:
        import yaml

        with open('contents_timing_plot.yaml', 'w') as myfile:
            yaml.dump(content, myfile)

    return groupedTimings


def readTimings(groupName, listOfLogs, produceYAML):
    return groupValues(algGroups[groupName], parseAllTimingTables(listOfLogs),
                       produceYAML)
