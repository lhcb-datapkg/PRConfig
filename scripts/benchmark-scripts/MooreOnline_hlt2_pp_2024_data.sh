#!/bin/bash
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

$(dirname $0)/cleanup.sh
workdir=MooreOnline_hlt2_pp_2024_data
rm -rfv $workdir


if [[ $(hostname --fqdn) == "lbhltperf01.cern.ch" ]]; then
  # this test runs on lbhltperf01 where we have more space for HLT2 input at
  # FIXME the online test picks up all files in the directory but some other
  #       tests are using the Hlt2Throughput directory.
  #       => Create a new directory just for this test.
  cache_dirs=(--data-dir "/scratch/z5/data/Hlt2Throughput/MooreOnline")
elif [[ $(hostname --fqdn) == "lbquantaperf01.cern.ch" ]]; then
  cache_dirs=(--data-dir "/localdisk1/scratch/data/Hlt2Throughput")
else
  mkdir -p /tmp/data/Hlt2Throughput
  cache_dirs=(--data-dir "/tmp/data/Hlt2Throughput")
fi

$MOORESCRIPTSROOT/scripts/testbench.py \
    --test-file-db-key=2024_hlt1_mdf_dump_291820 \
    --working-dir=$workdir \
    "${cache_dirs[@]}" \
    --log-file="ThroughputTest.log" \
    --measure-throughput=600 \
    --hlt-type hlt2_pp_2024 \
    --write-encoding-keys \
    $MOORESCRIPTSROOT/tests/options/HLT2/Arch.xml 2>&1

# compatibility with https://gitlab.cern.ch/lhcb-core/LHCbPR2HD/-/blob/83b7b8636ce0f420f7e0898bf68ddb8f62a13df4/handlers/ThroughputProfileHandler.py
head -n-0 $workdir/*.log > ThroughputTest.log

python $PRCONFIGROOT/python/MooreTests/make_profile_plots.py \
      --logs "$workdir/HLT2_0.log" \
      --hltlabel "HLT2" \
      --perf-path "" \
      --output-dir "$workdir"

if ! command -v perf &> /dev/null; then
    echo "Warning: perf not found, will not profile."
else
  $MOORESCRIPTSROOT/scripts/testbench.py \
    --test-file-db-key=2024_hlt1_mdf_dump_291820 \
    --working-dir=$workdir/HLT2Perf \
    "${cache_dirs[@]}" \
    --log-file="ProfileTest.log" \
    --measure-throughput=300 \
    --hlt-type hlt2_pp_2024 \
    --use-perf-control \
    $MOORESCRIPTSROOT/tests/options/HLT2Perf/Arch.xml 2>&1

    cp $workdir/HLT2Perf/*.svg flamy.svg
fi

# copy them to top dir such that they can be picked up on the webpage
cp $workdir/{FlameBars.png,FlameBars.pdf} .


# force 0 return code so the handler runs even for failed jobs
exit 0
