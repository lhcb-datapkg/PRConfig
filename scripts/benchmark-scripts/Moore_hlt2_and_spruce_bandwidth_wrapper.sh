#!/bin/bash
###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

usage=$(cat <<-EOF
Wrapper script to run chained Hlt2 and Sprucing with all lines over a large sample,
and then analyse the results to calculate rates, bandwidths and overlaps etc.

Writes out to tmp/Output/

Usage: Moore/run /path/to/Moore_hlt2_and_spruce_bandwidth_wrapper.sh --input_data <> 2>&1 | tee <path-to-output.log>
       to collect all output as a log file.

       Expected to be called by e.g. Moore_hlt2_and_spruce_bandwidth.sh and Moore_hlt2_and_spruce_bandwidth_SMOG2.sh.

--input-data: "nominal", "SMOG2_pp_pAr"
-h|--help: print this message and exit.

EOF
)


# if no arguments are provided print the usage and exit
if [ $# -eq 0 ]; then
    echo "$usage"
    exit 0
fi

# function to export a variable provided as an argument
function parse_value_and_export() {

    if [ $# -ne 3 ]; then
	echo "ERROR: Must provide the argument as $0 <value>"
	return 1
    fi

    if [[ "$3" =~ "--" ]]; then
	echo 'ERROR: Invalid arguments "'"$2 $3"'"'
	return 1;
    fi

    export $1=$3
}

# parse arguments
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
	-h|--help)
	    echo "$usage"
	    exit 0
	    ;;
	--input-data)
	    parse_value_and_export INPUTDATA $1 $2
	    shift # parse argument
	    shift # parse value
	    ;;
	*)
	    echo "ERROR: Unknown argument \"$1\""
	    exit 1
	    ;;
    esac
done

for subdir in MDF Output Output/Inter to_eos; do
    mkdir -p $BASEDIR/$subdir
done

# this path ends up printed on the BW test page; export so it can be picked up in the child process
export SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/$(basename "$0")"

# TODO pass in the config path - you have it duplicated here and in Moore_bandiwdth_test.sh
$PRCONFIGROOT/scripts/benchmark-scripts/Moore_bandwidth_test.sh --process hlt2 --input-data $INPUTDATA --stream-config production
hlt2_err_code=$?
SPRUCE_BASE_CMD='$PRCONFIGROOT/scripts/benchmark-scripts/Moore_bandwidth_test.sh --process spruce --input-data hlt2-output-locally'
eval "${SPRUCE_BASE_CMD} --stream-config full"
spruce_full_err_code=$?
eval "${SPRUCE_BASE_CMD} --stream-config turbo"
spruce_turbo_err_code=$?
eval "${SPRUCE_BASE_CMD} --stream-config turcal"
spruce_turcal_err_code=$?
eval "${SPRUCE_BASE_CMD} --stream-config no_bias"
spruce_no_bias_err_code=$?
eval "${SPRUCE_BASE_CMD} --stream-config hlt2calib"
spruce_hlt2calib_err_code=$?
eval "${SPRUCE_BASE_CMD} --stream-config lumi"
spruce_lumi_err_code=$?

# Write error codes now to a file for robust error handling
error_codes=$(cat <<EOF
{
    "hlt2": {
        "production": { "code": $hlt2_err_code }
    },
    "spruce": {
        "full": { "code": $spruce_full_err_code },
        "turbo": { "code": $spruce_turbo_err_code },
        "turcal": { "code": $spruce_turcal_err_code },
        "no_bias": { "code": $spruce_no_bias_err_code },
        "hlt2calib": { "code": $spruce_hlt2calib_err_code },
        "lumi": { "code": $spruce_lumi_err_code }
    }
}
EOF
)
echo "$error_codes" > $BASEDIR/Output/message.json
echo "Error codes written to $BASEDIR/Output/message.json"

# Produce plots and HTML pages; add the --building-locally flag to make the links work if you are building the html pages locally
if [ $BUILD_PAGES_LOCALLY = true ];
then
    BUILD_PAGES_LOCALLY_FLAG="--building-locally";
else
    BUILD_PAGES_LOCALLY_FLAG="";
fi
echo 'Making plots and HTML pages'
time python $HLT2CONFROOT/python/Hlt2Conf/tests/bandwidth/make_bandwidth_test_page.py --per-test-info \
    hlt2:production:'$HLT2CONFROOT/tests/options/bandwidth/hlt2_bandwidth_input_2024.yaml' \
    spruce:full:'$BASEDIR/spruce_bandwidth_latest_input__full.yaml' \
    spruce:turbo:'$BASEDIR/spruce_bandwidth_latest_input__turbo.yaml' \
    spruce:turcal:'$BASEDIR/spruce_bandwidth_latest_input__turcal.yaml' \
    spruce:no_bias:'$BASEDIR/spruce_bandwidth_latest_input__no_bias.yaml' \
    spruce:hlt2calib:'$BASEDIR/spruce_bandwidth_latest_input__hlt2calib.yaml' \
    spruce:lumi:'$BASEDIR/spruce_bandwidth_latest_input__lumi.yaml' \
    -s $SCRIPT_PATH \
    $BUILD_PAGES_LOCALLY_FLAG

TEST_PAGE_ERR_CODE=$?
last_msg="{\"make_html_page\": {\"code\": ${TEST_PAGE_ERR_CODE} } }"
jq --argjson last_msg "$last_msg" '. += $last_msg' $BASEDIR/Output/message.json > $BASEDIR/Output/tmp_message.json && mv $BASEDIR/Output/tmp_message.json $BASEDIR/Output/message.json

# force 0 return code so the handler runs even for failed jobs
exit 0
