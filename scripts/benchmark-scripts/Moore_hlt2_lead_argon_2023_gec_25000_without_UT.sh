#!/bin/bash
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

$(dirname $0)/cleanup.sh

if [[ $(hostname --fqdn) == "lbhltperf01.cern.ch" ]]; then
  # this test runs on lbhltperf01 where we have more space for HLT2 input at
  cache_dirs=(--cache-dirs "/scratch/z5/data/Hlt2Throughput")
elif [[ $(hostname --fqdn) == "lbquantaperf02.cern.ch" ]]; then
  cache_dirs=(--cache-dirs "/localdisk1/Hlt2Throughput")
fi

\time python -m MooreTests.run_throughput_jobs -n 300 "${cache_dirs[@]}" \
  --test-file-db-key=upgrade-202305-PbAr-FT-EPOS-b-8_22-VELO-10mm-mdf --avg-event-size=500000 \
  '$MOOREROOT/options/ft_decoding_v6.py' '$MOOREROOT/tests/options/disable-git-metadata-write.py' '$RECOCONFROOT/options/hlt2_lead_lead_fast_reco_pr_kf_without_UT_gec_25000.py'

\time python -m MooreTests.run_throughput_jobs -n 300 -j 1 --profile "${cache_dirs[@]}" \
  --test-file-db-key=upgrade-202305-PbAr-FT-EPOS-b-8_22-VELO-10mm-mdf --avg-event-size=500000 \
  '$MOOREROOT/options/ft_decoding_v6.py' '$MOOREROOT/tests/options/disable-git-metadata-write.py' '$RECOCONFROOT/options/hlt2_lead_lead_fast_reco_pr_kf_without_UT_gec_25000.py'

# force 0 return code so the handler runs even for failed jobs
exit 0
