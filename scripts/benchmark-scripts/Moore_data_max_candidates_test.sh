#!/bin/bash
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
echo "Running Moore For Max Candidates data test"
# Set configuration variables and check configuration makes sense
if [ -z "${OVERRIDE_EVTMAX}" ]; then
    EVTMAX=60000
else
    EVTMAX=${OVERRIDE_EVTMAX}
fi

MOORE_THREADS=${LBN_BUILD_JOBS:-1}

time python $HLT2CONFROOT/python/Hlt2Conf/tests/run_max_candidates_job.py -d -n $EVTMAX -t $MOORE_THREADS --test-file-db-key=2024_hlt1_mdf_dumps_ReverseGEC12K '$HLT2CONFROOT/tests/options/hlt2_max_candidates_pp_2024.py'

time python $HLT2CONFROOT/python/Hlt2Conf/tests/analyze_max_candidates.py
