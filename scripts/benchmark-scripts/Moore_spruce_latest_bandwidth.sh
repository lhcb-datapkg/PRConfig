#!/bin/bash

###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Override variables necessary to easily run qmts in CI
export HLT1_EVTMAX=''
export HLT2_EVTMAX=''
export SPRUCE_EVTMAX=${OVERRIDE_EVTMAX:--1}
export BASEDIR=${OVERRIDE_BASEDIR:-tmp}
export DOWNLOAD_INPUT_LOCALLY=true
export BUILD_PAGES_LOCALLY=false
export HLT2_THREADS=1
export HLT1_INPUT_CONFIG=''
export HLT2_INPUT_CONFIG=''
export SPRUCE_INPUT_CONFIG=''

for subdir in MDF Output Output/Inter to_eos; do
    mkdir -p $BASEDIR/$subdir
done

# this path ends up printed on the BW test page; export so it can be picked up in the child process
export SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )/$(basename "$0")"

BASE_CMD='$PRCONFIGROOT/scripts/benchmark-scripts/Moore_bandwidth_test.sh --process spruce --input-data hlt2-output-from-eos'
eval "${BASE_CMD} --stream-config full"
spruce_full_err_code=$?
eval "${BASE_CMD} --stream-config turbo"
spruce_turbo_err_code=$?
eval "${BASE_CMD} --stream-config turcal"
spruce_turcal_err_code=$?
eval "${BASE_CMD} --stream-config no_bias"
spruce_no_bias_err_code=$?
eval "${BASE_CMD} --stream-config hlt2calib"
spruce_hlt2calib_err_code=$?
eval "${BASE_CMD} --stream-config lumi"
spruce_lumi_err_code=$?

# Write error codes now to a file for robust error handling
error_codes=$(cat <<EOF
{
    "spruce": {
        "full": { "code": $spruce_full_err_code },
        "turbo": { "code": $spruce_turbo_err_code },
        "turcal": { "code": $spruce_turcal_err_code },
        "no_bias": { "code": $spruce_no_bias_err_code },
        "hlt2calib": { "code": $spruce_hlt2calib_err_code },
        "lumi": { "code": $spruce_lumi_err_code }
    }
}
EOF
)
echo "$error_codes" > $BASEDIR/Output/message.json
echo "Error codes written to $BASEDIR/Output/message.json"

# Produce plots and HTML pages; add the --building-locally flag to make the links work if you are building the html pages locally
if [ $BUILD_PAGES_LOCALLY = true ];
then
    BUILD_PAGES_LOCALLY_FLAG="--building-locally";
else
    BUILD_PAGES_LOCALLY_FLAG="";
fi
echo 'Making plots and HTML pages'
time python ${HLT2CONFROOT}/python/Hlt2Conf/tests/bandwidth/make_bandwidth_test_page.py --per-test-info \
    spruce:full:'$BASEDIR/spruce_bandwidth_latest_input__full.yaml' \
    spruce:turbo:'$BASEDIR/spruce_bandwidth_latest_input__turbo.yaml' \
    spruce:turcal:'$BASEDIR/spruce_bandwidth_latest_input__turcal.yaml' \
    spruce:no_bias:'$BASEDIR/spruce_bandwidth_latest_input__no_bias.yaml' \
    spruce:hlt2calib:'$BASEDIR/spruce_bandwidth_latest_input__hlt2calib.yaml' \
    spruce:lumi:'$BASEDIR/spruce_bandwidth_latest_input__lumi.yaml' \
    -s $SCRIPT_PATH \
    $BUILD_PAGES_LOCALLY_FLAG

TEST_PAGE_ERR_CODE=$?
last_msg="{\"make_html_page\": {\"code\": ${TEST_PAGE_ERR_CODE} } }"
jq --argjson last_msg "$last_msg" '. += $last_msg' $BASEDIR/Output/message.json > $BASEDIR/Output/tmp_message.json && mv $BASEDIR/Output/tmp_message.json $BASEDIR/Output/message.json

# force 0 return code so the handler runs even for failed jobs
exit 0