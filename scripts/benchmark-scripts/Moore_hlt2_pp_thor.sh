#!/bin/bash
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

$(dirname $0)/cleanup.sh

if [[ $(hostname --fqdn) == "lbhltperf01.cern.ch" ]]; then
  # this test runs on lbhltperf01 where we have more space for HLT2 input at
  cache_dirs=(--cache-dirs "/scratch/z5/data/Hlt2Throughput")
elif [[ $(hostname --fqdn) == "lbquantaperf01.cern.ch" ]]; then
  cache_dirs=(--cache-dirs "/localdisk1/scratch/data/Hlt2Throughput")
fi

# The lines below overwrite the dddb_tag from the test file database.
# This option was added to allow hlt2 lines that rely on a newer particle table version 
# to work with older simulation used in the thoughput tests. See PRConfig!441 for more details.
# This workaround is temporary and can be removed, once newer simulation samples,
# based on dddb-20240427 directly, are available for these tests.

python -m MooreTests.run_throughput_jobs -n 2e4 --test-file-db-key=expected_2024_min_bias_hlt1_filtered --dddb-tag="upgrade/dddb-20231017-new-particle-table" '$MOOREROOT/tests/options/disable-git-metadata-write.py' '$HLT2CONFROOT/options/hlt2_pp_thor.py' "${cache_dirs[@]}"

python -m MooreTests.run_throughput_jobs -n=-1 -j 1 --profile --test-file-db-key=expected_2024_min_bias_hlt1_filtered --dddb-tag="upgrade/dddb-20231017-new-particle-table" '$MOOREROOT/tests/options/disable-git-metadata-write.py' '$HLT2CONFROOT/options/hlt2_pp_thor.py' "${cache_dirs[@]}"

# force 0 return code so the handler runs even for failed jobs
exit 0
