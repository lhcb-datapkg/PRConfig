#!/bin/bash
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

$(dirname $0)/cleanup.sh
rm -rfv MooreOnline_hlt1_pp_default_data
rm -rfv config.git

cache_dirs=(--data-dir "/tmp/data/Hlt1Throughput")
if [[ $(hostname --fqdn) == "lblab02.cern.ch" ]]; then
  # this test runs on lblab02 where we have more space for HLT1 input at
  # cache_dirs=(--data-dir "/home/localdisk/scratch/data/Hlt1Throughput")
  cache_dirs=(--data-dir "/home/localdisk/scratch/data/Hlt1Throughput")
else
    mkdir -p /tmp/data/Hlt1Throughput
fi

# WORKAROUND for environment variables that are not relocatable
allen_dirs=($(python - <<'CODE'
import inspect
from Allen import tck
for s in ('python', 'InstallArea'):
    print(inspect.getfile(tck).split(f"/{s}/")[0])
CODE
))
export ALLEN_INSTALL_DIR="${allen_dirs[0]}"

# Create the TCK
python "${allen_dirs[1]}/Rec/Allen/scripts/create_hlt1_tck.py" \
	RTA/2050.01.01 \
	hlt1_pp_matching_no_ut_1000KHz \
	config.git \
	0x10000001 2>&1

# Sometimes the system DIM_DNS_NODE seems to be picked up
unset DIM_DNS_NODE

# Run the test
$MOORESCRIPTSROOT/scripts/testbench.py \
    --test-file-db-key=2024_mep_289232 \
    --working-dir=MooreOnline_hlt1_pp_default_data \
    "${cache_dirs[@]}" \
    --log-file="ThroughputTest.log" \
    --measure-throughput=120 \
    --tfdb-nfiles=1 \
	--partition=TESTHLT1THP	\
	--hlt-type=config.git:0x10000001 \
    $MOORESCRIPTSROOT/tests/options/HLT1/Arch.xml 2>&1

# Fixup HLT1 log to remove a spurious Evts/s from the fake event loop
sed -i  's/Evts/Events/g' MooreOnline_hlt1_pp_default_data/HLT1_0.log

# Combine all logs for the throughput handler
head -n-0 MooreOnline_hlt1_pp_default_data/*.log > ThroughputTest.log

# force 0 return code so the handler runs even for failed jobs
exit 0
