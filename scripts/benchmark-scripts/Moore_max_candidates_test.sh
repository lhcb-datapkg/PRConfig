
#!/bin/bash
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
echo "Running Moore For Max Candidates test"
if [ -z "${OVERRIDE_EVTMAX}" ]; then
    EVTMAX=1e5
else
    EVTMAX=${OVERRIDE_EVTMAX}
fi

time python -m MooreTests.run_max_candidates_job -n $EVTMAX --test-file-db-key=expected_2024_min_bias_hlt1_filtered_v2 '$HLT2CONFROOT/tests/options/hlt2_max_candidates_pp_2024.py'

time python -m MooreTests.analyze_max_candidates
