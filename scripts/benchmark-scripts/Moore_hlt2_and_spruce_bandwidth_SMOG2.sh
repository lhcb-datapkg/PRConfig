#!/bin/bash

###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Override variables necessary to easily run qmts in CI
export HLT1_EVTMAX=''
export HLT2_EVTMAX=${OVERRIDE_EVTMAX:-1e5}
export SPRUCE_EVTMAX=-1
export BASEDIR=${OVERRIDE_BASEDIR:-tmp}
export DOWNLOAD_INPUT_LOCALLY=true
export BUILD_PAGES_LOCALLY=false
export HLT2_THREADS=1
export HLT1_INPUT_CONFIG=''
export HLT2_INPUT_CONFIG=''
export SPRUCE_INPUT_CONFIG=''

$PRCONFIGROOT/scripts/benchmark-scripts/Moore_hlt2_and_spruce_bandwidth_wrapper.sh --input-data SMOG2_pp_pAr
