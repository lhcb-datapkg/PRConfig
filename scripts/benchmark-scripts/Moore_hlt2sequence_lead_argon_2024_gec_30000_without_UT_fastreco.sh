#!/bin/bash
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

$(dirname $0)/cleanup.sh

if [[ $(hostname --fqdn) == "lbhltperf01.cern.ch" ]]; then
  # this test runs on lbhltperf01 where we have more space for HLT2 input at
  cache_dirs=(--cache-dirs "/scratch/z5/data/Hlt2Throughput")
elif [[ $(hostname --fqdn) == "lbquantaperf02.cern.ch" ]]; then
  cache_dirs=(--cache-dirs "/localdisk1/Hlt2Throughput")
fi

\time python -m MooreTests.run_throughput_jobs -n 1000 "${cache_dirs[@]}" \
  --test-file-db-key=hlt1-filtered-expected-2024-MB-PbAr --avg-event-size=500000 \
  '$MOOREROOT/tests/options/disable-git-metadata-write.py' '$HLT2CONFROOT/options/hlt2_PbPb_fastreco_without_UT_2024.py'

\time python -m MooreTests.run_throughput_jobs -n 1000 -j 1 --profile "${cache_dirs[@]}" \
  --test-file-db-key=hlt1-filtered-expected-2024-MB-PbAr --avg-event-size=500000 \
  '$MOOREROOT/tests/options/disable-git-metadata-write.py' '$HLT2CONFROOT/options/hlt2_PbPb_fastreco_without_UT_2024.py'

# force 0 return code so the handler runs even for failed jobs
exit 0
