#!/bin/bash
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

$(dirname $0)/cleanup.sh

if [[ $(hostname --fqdn) == "lbhltperf01.cern.ch" ]]; then
  # this test runs on lbhltperf01 where we have more space for HLT2 input at
  cache_dirs=(--cache-dirs "/scratch/z5/data/Hlt2Throughput")
elif [[ $(hostname --fqdn) == "lbquantaperf01.cern.ch" ]]; then
  cache_dirs=(--cache-dirs "/localdisk1/scratch/data/Hlt2Throughput")
else
  mkdir -p /tmp/data/Hlt2Throughput
  cache_dirs=(--cache-dirs "/tmp/data/Hlt2Throughput")
fi

python -m MooreTests.run_throughput_jobs -n 20000 --no-output --test-file-db-key=expected_2024_min_bias_hlt1_filtered_v2 '$MOOREROOT/tests/options/disable-git-metadata-write.py' '$HLT2CONFROOT/options/hlt2_pp_2024.py' "${cache_dirs[@]}"

python -m MooreTests.run_throughput_jobs -n=-1 -j 1 --profile --no-output --test-file-db-key=expected_2024_min_bias_hlt1_filtered_v2 '$MOOREROOT/tests/options/disable-git-metadata-write.py' '$HLT2CONFROOT/options/hlt2_pp_2024.py' "${cache_dirs[@]}"

# force 0 return code so the handler runs even for failed jobs
exit 0
