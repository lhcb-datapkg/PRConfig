#! /usr/bin/bash
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

rm -fv \
  perf.data \
  ThroughputTest*.log \
  RateTest*.log \
  Profile*.log \
  FlameBars.png \
  *.svg \
  ht_flamy* \
  heaptrack.python*.gz \
  FlameBars.pdf \
  result.csv \
  monitoring.json \
  manifest-*.json
