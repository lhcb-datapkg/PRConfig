###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import Boole
from GaudiConf import IOHelper

importOptions("$APPCONFIGOPTS/Boole/Default.py")
importOptions("$APPCONFIGOPTS/Boole/Boole-Upgrade-IntegratedLumi.py")
importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")

testName = "PR-UPG-Integration-noSpillover"

Boole().EvtMax = 1000
Boole().InputDataType = "XDIGI"
Boole().DigiType = "Extended"
#Boole().Histograms = "Expert"
Boole().DatasetName = testName
Boole().Outputs = []

from PRConfig import TestFileDB
TestFileDB.test_file_db['upgrade_Aug2022_BsPhiPhi_md_xdigi'].run()

HistogramPersistencySvc().OutputFile = "Boole-histos.root"
