###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Config for performance testing
from Gaudi.Configuration import *
from Configurables import Brunel
from PRConfig import TestFileDB

TestFileDB.test_file_db['2015_raw_full'].run(configurable=Brunel())
Brunel().EvtMax = 1000
Brunel().DatasetName = "2015magdown"
Brunel().Monitors = ["SC", "FPE"]

from Configurables import EventClockSvc
EventClockSvc(InitialTime=1445527400000000000)
