###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
importOptions("$GAUSSOPTS/General/2016.py")
importOptions("$GAUSSOPTS/General/Threads-8.py")
importOptions("$DECFILESROOT/options/20000000.py")

from Configurables import (
    Gauss,
    GaussGeneration,
)

GaussGeneration(
    ProductionTool="Pythia8ProductionMT",
    GenMonitor=True,
)

Gauss(
    EvtMax=100,
    OutputType='NONE',
)
