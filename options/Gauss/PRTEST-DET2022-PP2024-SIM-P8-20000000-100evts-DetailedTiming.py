###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
importOptions("$GAUSSROOT/options/Gauss-Upgrade.py")
importOptions("$DECFILESROOT/options/20000000.py")
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")

# Disable spill-over
from Configurables import Gauss
Gauss().SpilloverPaths = []

# Events to process
from Configurables import LHCbApp
LHCbApp().EvtMax = 100

# No output file
from Gauss.Configuration import *
Gauss().OutputType = 'NONE'


def use_monitoring():
    importOptions("$GAUSSMONITORROOT/options/MonitorTiming-Upgrade.py")


appendPostConfigAction(use_monitoring)
