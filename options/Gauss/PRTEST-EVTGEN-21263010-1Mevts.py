###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
importOptions("$GAUSSROOT/options/Gauss-2016.py")
importOptions("$LBPGUNSROOT/options/PGuns.py")
importOptions("$GAUSSOPTS/GenStandAlone.py")

# Disable spill-over
from Configurables import Gauss
Gauss().SpilloverPaths = []

# Events to process
from Configurables import LHCbApp
LHCbApp().EvtMax = 1000000

#--Generator phase, set random numbers
from Gauss.Configuration import GenInit
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = 1083
GaussGen.PrintFreq = 10000

# Run only the generator part, no full simulation
importOptions('$APPCONFIGOPTS/Gauss/gen.py')

# Fix the number of interactions to one
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')

# No output file
from Gauss.Configuration import *
Gauss().OutputType = 'NONE'

from Configurables import Generation
Generation().EventType = 21263010

from Configurables import ParticleGun, FixedMomentum, FlatNParticles
from Configurables import ToolSvc, EvtGenDecay
from GaudiKernel import SystemOfUnits

pgun = ParticleGun()
pgun.ParticleGunTool = "FixedMomentum"
pgun.addTool(FixedMomentum, name="FixedMomentum")

pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.addTool(FlatNParticles, name="FlatNParticles")

pgun.FixedMomentum.PdgCodes = [411]
pgun.SignalPdgCode = 411

pgun.FixedMomentum.px = 0.0 * SystemOfUnits.GeV
pgun.FixedMomentum.py = 0.0 * SystemOfUnits.GeV
pgun.FixedMomentum.pz = 0.0 * SystemOfUnits.GeV

tsvc = ToolSvc()
tsvc.addTool(EvtGenDecay, name="EvtGenDecay")
tsvc.EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/D+_K-pi+pi+=res,DecProdCut.dec"

pgun.DecayTool = "EvtGenDecay"

from Configurables import DecayAnalysis
analysis = DecayAnalysis()
analysis.NeutralParticleHistos = True
GenMonitor = GaudiSequencer("GenMonitor")
GenMonitor.Members = [analysis]
