###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
from Configurables import (
    Gauss,
    GaussGeneration,
    Generation,
    FixedNInteractions,
)
importOptions("$GAUSSOPTS/General/2016.py")
importOptions("$GAUSSOPTS/General/Threads-8.py")
importOptions("$DECFILESROOT/options/30000000.py")

GaussGeneration(
    # ensure P8 shared interface is used
    ProductionTool="Pythia8Production",
    GenMonitor=True,
    PileUpTool="FixedNInteractions",
)

Gauss(
    EvtMax=10000,
    Phases=['Generator'],
    OutputType='NONE',
)

# FIXME: this should be passed as PileUpToolOpts
Generation().addTool(FixedNInteractions)
Generation().FixedNInteractions.NInteractions = 1
