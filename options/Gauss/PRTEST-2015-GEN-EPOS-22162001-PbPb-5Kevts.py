###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
importOptions("$APPCONFIGOPTS/Gauss/PbPb-Beam2510GeV-md100-2015-fix1.py")
importOptions("$DECFILESROOT/options/22162001.py")

# DB tags and data type
importOptions("$GAUSSOPTS/DBTags-2015.py")
importOptions("$APPCONFIGOPTS/Gauss/DataType-2015.py")

# Events to process
from Configurables import LHCbApp
LHCbApp().EvtMax = 5000

importOptions("$LBCRMCROOT/options/EPOSBeamBeam.py")
importOptions("$GAUSSOPTS/BeamBeam.py")
importOptions("$GAUSSOPTS/Embedding.py")

# Run only the generator part, no full simulation
importOptions("$GAUSSOPTS/GenStandAlone.py")

# Fix the number of interactions to one
importOptions('$APPCONFIGOPTS/Gauss/OneFixedInteraction.py')

# No output file
from Gauss.Configuration import *
Gauss().OutputType = 'NONE'
