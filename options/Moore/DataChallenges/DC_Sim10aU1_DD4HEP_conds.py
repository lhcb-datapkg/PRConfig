###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options

# To be removed once data is updated to use a tag including
# https://gitlab.cern.ch/lhcb-conddb/DDDB/-/merge_requests/107
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    from Configurables import DDDBConf
    DDDBConf().GeometryVersion = 'run3/before-rich1-geom-update-26052022'

from RecoConf.calorimeter_reconstruction import make_digits
make_digits.global_bind(calo_raw_bank=False)

options.evt_max = 1000
