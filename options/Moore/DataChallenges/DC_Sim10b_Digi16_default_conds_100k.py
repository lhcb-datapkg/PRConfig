###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options

options.n_threads = 1
options.event_store = 'EvtStoreSvc'

options.input_raw_format = 0.5
options.first_evt = 0
options.print_freq = 1000
options.dddb_tag = "dddb-20221004"
options.conddb_tag = "sim-20221220-vc-md100"
options.data_type = "Upgrade"

#options.output_level = 0

options.evt_max = 100000
