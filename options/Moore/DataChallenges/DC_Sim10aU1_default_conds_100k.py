###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from RecoConf.hlt1_muonid import make_muon_hits

make_muon_hits.global_bind(geometry_version=2)

options.input_type = 'ROOT'
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = "dddb-20210617"
options.conddb_tag = "sim-20210617-vc-md100"

options.evt_max = 100000
