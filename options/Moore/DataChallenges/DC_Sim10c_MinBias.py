###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options

from PRConfig.FilesFromDirac import get_access_urls_mc

options.input_files = get_access_urls_mc(
    "/MC/Dev/Beam6800GeV-expected-2024-MagDown-Nu7.6-25ns-Pythia8/Sim10c",
    "30000000", ["DIGI"])
options.input_type = "ROOT"
