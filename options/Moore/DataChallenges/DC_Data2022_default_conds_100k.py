###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc

options.n_threads = 1
options.event_store = 'EvtStoreSvc'

# Alignment tags available at https://lhcb-alignment.docs.cern.ch/constants/constants.html

options.input_raw_format = 0.5
options.first_evt = 0
options.print_freq = 1000
options.dddb_tag = "master"
options.conditions_version = "AlignmentV10_2023_05_09_LHCP"  #specific DD4hep tag
#options.conditions_version = "master" #-- general settings for DD4hep builds
options.data_type = "Upgrade"
options.geometry_version = "run3/trunk"
options.conditions_version = "master"

#options.output_level = 0

LHCb__Det__LbDD4hep__DD4hepSvc(DetectorList=[
    '/world', 'VP', 'FT', 'Magnet', 'Rich1', 'Rich2', 'Ecal', 'Hcal', 'Muon'
])

options.evt_max = 100000
