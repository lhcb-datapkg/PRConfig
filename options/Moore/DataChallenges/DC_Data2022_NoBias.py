###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options

options.input_files = [
    "/eos/lhcb/wg/rta/samples/data/256139-LHCb-RAW/256139_00090007_0041.raw",
    "/eos/lhcb/wg/rta/samples/data/256139-LHCb-RAW/256139_00090024_0039.raw",
    "/eos/lhcb/wg/rta/samples/data/256139-LHCb-RAW/256139_00090027_0040.raw",
]
options.use_iosvc = True
options.input_type = "RAW"
options.simulation = False
