###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Moore option file for Callgrind profile
#
#
from Gaudi.Configuration import *
from Configurables import CallgrindProfile, Moore


def addProfile():
    p = CallgrindProfile()
    p.StartFromEventN = 100
    p.StopAtEventN = 300
    p.DumpAtEventN = 300
    p.DumpName = "PRTEST"
    GaudiSequencer("Hlt").Members.insert(0, p)


from Configurables import Hlt__Service
Hlt__Service().Pedantic = False
Moore().OutputLevel = 1
Moore().ThresholdSettings = 'Physics_September2012'
Moore().EvtMax = 301
Moore().RemoveInputHltRawBanks = True

# taking list of data files from the TestFileDB
from PRConfig.TestFileDB import test_file_db
input = test_file_db['2012_raw_default']
input.run(configurable=Moore())
Moore().ForceSingleL0Configuration = False

# Now add the profiling algorithm to the sequence
appendPostConfigAction(addProfile)
