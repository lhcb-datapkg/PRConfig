#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#
# Minimal file for running Moore from python prompt
# Syntax is:
#   gaudirun.py ../options/Moore.py
# or just
#   ../options/Moore.py
#
import Gaudi.Configuration
from Configurables import Moore

# if you want to generate a configuration, uncomment the following lines:
#Moore().generateConfig = True
#Moore().configLabel = 'Default'
#Moore().ThresholdSettings = 'Commissioning_PassThrough'
#Moore().configLabel = 'ODINRandom acc=0, TELL1Error acc=1'

Moore().ThresholdSettings = 'Physics_draft2012'

Moore().Verbose = True
Moore().EvtMax = 30000

Moore().UseDBSnapshot = False
Moore().DDDBtag = 'head-20120413'
Moore().CondDBtag = 'head-20120420'
Moore().Simulation = False
Moore().DataType = '2012'
Moore().inputFiles = ['/opt/dirac/profile/slohn/114280_0000000007.raw']
#Moore().inputFiles = [ '/opt/dirac/profile/slohn/114280_0000000007.raw' ]
#from Configurables import HltRoutingBitsWriter
Moore().ForceSingleL0Configuration = False

from Configurables import EventSelector
EventSelector().PrintFreq = 100

#from Configurables import Hlt__Service
#Hlt__Service().Pedantic = False
